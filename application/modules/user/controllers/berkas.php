<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class berkas extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/

	function index($uri=0)
	{
		if($this->session->userdata('logged_in')!="")
		{
				
			$d['surat_keterangan_indexs'] = $this->app_global_web_model->generate_indexs_surat_keterangan_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_pindah_indexs'] = $this->app_global_web_model->generate_indexs_surat_pindah_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_nikah_indexs'] = $this->app_global_web_model->generate_indexs_surat_nikah_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_kelahiran_indexs'] = $this->app_global_web_model->generate_indexs_surat_kelahiran_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_kematian_indexs'] = $this->app_global_web_model->generate_indexs_surat_kematian_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_kkb_indexs'] = $this->app_global_web_model->generate_indexs_surat_kkb_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_keterangan_usaha_indexs'] = $this->app_global_web_model->generate_indexs_surat_keterangan_usaha_edit($this->session->userdata("id_user"),'4',$uri);
			$d['ijin_keramaian_indexs'] = $this->app_global_web_model->generate_indexs_ijin_keramaian_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_bepergian_indexs'] = $this->app_global_web_model->generate_indexs_surat_bepergian_edit($this->session->userdata("id_user"),'4',$uri);
			$d['surat_kuasa_indexs'] = $this->app_global_web_model->generate_indexs_surat_kuasa_edit($this->session->userdata("id_user"),'4',$uri);
			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('MANAJEMEN BERKAS', '/');
			
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/berkas/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}	
	}
}
