<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cektiket extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
function __construct()
{
          // this is your constructor
	parent::__construct();
		//$this->load->model('app_global_superadmin_model');

	$where2		= array(
		'id_user' => $this->session->userdata("id_user"),
		'readStatus' => 1,
		);
	
	$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
	$this->session->set_userdata('notifUser', $jumlah);
}

public function index($uri=0)
{
	if($this->session->userdata('logged_in')!="")
	{
		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
		$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
		$this->breadcrumb->append_crumb("TIKET SAMBATAN ANDA", '/');
		
		$d['title'] = "Tiket Sambatan Anda";
			//$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_kategori($this->config->item("limit_item"),$uri);
		$id_param = $this->session->userdata("id_user");
		$d['data_retrieve'] = $this->app_global_web_model->generate_index_tiket_user($this->session->userdata("id_user"),'4',$uri);
		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
		$this->load->view($_SESSION['site_theme'].'/user/cektiket/bg_home');
		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}
	else
	{
		redirect(base_url());
	}
}


public function edit($id_param)
{
	if($this->session->userdata('logged_in')!="")
	{
		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
		$this->breadcrumb->append_crumb("KATEGORI TIKET SAMBATAN", base_url().'superadmin/kategori');
		$this->breadcrumb->append_crumb("EDIT KATEGORI", '/');
		$d['title'] = "Edit Kategori Tiket";
		$where['id_kategori'] = $id_param;
		$get = $this->db->get_where("sam_kategori",$where)->row();
		$d['kategori'] = $get->kategori;
		$d['id_param'] = $get->id_kategori;
		$d['tipe'] = "edit";
		
		$this->load->view('bg_header',$d);
		$this->load->view('kategori/bg_input');
		$this->load->view('bg_footer');
	}
	else
	{
		redirect(base_url());
	}
}

public function simpan()
{
	if($this->session->userdata('logged_in')!="")
	{
		$tipe = $this->input->post("tipe");
		$id['id_kategori'] = $this->input->post("id_param");
		if($tipe=="tambah")
		{
			$in['kategori'] = $this->input->post("kategori");
			$this->db->insert("sam_kategori",$in);
		}
		else if($tipe=="edit")
		{
			$in['kategori'] = $this->input->post("kategori");
			$in['id_kategori'] = $this->input->post("id_kategori");
			$this->db->update("sam_kategori",$in,$id);
		}
		
		redirect("superadmin/kategori");
	}
	else
	{
		redirect(base_url());
	}
}

public function hapus($id_param)
{
	if($this->session->userdata('logged_in')!="")
	{
		$where['id_kategori'] = $id_param;
		$this->db->delete("sam_kategori",$where);
		redirect("superadmin/kategori");
	}
	else
	{
		redirect(base_url());
	}
}
}

/* End of file superadmin.php */
