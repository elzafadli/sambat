<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class surat_keterangan extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('image_lib');

        $where2		= array(
			'id_user' => $this->session->userdata("id_user"),
			'readStatus' => 1,
			);
		
		$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
		$this->session->set_userdata('notifUser', $jumlah);
    }

	function index($uri=0)
	{
		if($this->session->userdata('logged_in')!="")
		{
			
				
			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('SURAT KETERANGAN', '/');
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_lokasi();
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/surat_keterangan/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
			
	}

	function edit($id_param)
	{
		if($this->session->userdata('logged_in')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$q = $this->db->get_where("kel_surat_keterangan",$where)->row();
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_lokasi_select($id_param);
			$d['id_surat_ket'] = $id_param;
			$d['id_lokasi'] = $this->session->userdata("id_lokasi");
			$d['id_user'] = $this->session->userdata("id_user");
			$d['keterangan'] = $q->keterangan;
			
			if($this->session->userdata("id_user")!=$q->id_user)
			{
				redirect("user/dashboard");
			}
			
			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('SURAT KETERANGAN', base_url().'user/surat_keterangan');
			$this->breadcrumb->append_crumb('EDIT', '/');
			
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/surat_keterangan/bg_edit');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
			
	}

	function set()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$config['upload_path'] = './asset/file';
				$config['allowed_types'] = 'zip|pdf';
				$config['encrypt_name']	= TRUE;
				$config['remove_spaces']= TRUE;	
				$config['max_size']     = '40000';
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('file'))
				{
					$data = $this->upload->data(); 
					$in['id_lokasi'] = $this->session->userdata("id_lokasi");
					$in['id_user'] = $this->session->userdata("id_user");
					$in['keterangan'] = $this->input->post("keterangan");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$in['file'] = $data['file_name'];
					$this->db->insert("kel_surat_keterangan",$in);
					redirect("user/surat_keterangan/sukses/".$in['id_user']."");
				}
				else
			{
				redirect("user/surat_keterangan");
			}
		}
	}

	function update()
	{
				
				$config['upload_path'] = './asset/file';
				$config['allowed_types'] = 'zip|pdf';
				$config['encrypt_name']	= TRUE;
				$config['remove_spaces']= TRUE;	
				$config['max_size']     = '40000';
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('file'))
				{
					$data = $this->upload->data();
					$id['id_surat_ket'] = $this->input->post("id_surat_ket"); 
					$in['id_lokasi'] = $this->session->userdata("id_lokasi");
					$in['id_user'] = $this->session->userdata("id_user");
					$in['keterangan'] = $this->input->post("keterangan");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$in['file'] = $data['file_name'];
					$this->db->update("kel_surat_keterangan",$in,$id);
					redirect("user/surat_keterangan/sukses/".$in['id_user']."");
				}
				else
			{ 
					$id['id_surat_ket'] = $this->input->post("id_surat_ket");
					$in['id_lokasi'] = $this->session->userdata("id_lokasi");
					$in['id_user'] = $this->session->userdata("id_user");
					$in['keterangan'] = $this->input->post("keterangan");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$this->db->update("kel_surat_keterangan",$in,$id);
					redirect("user/surat_keterangan/sukses/".$in['id_user']."");
			}
	}

	function hapus($id_param)
	{
		if($this->session->userdata('logged_in')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$this->db->delete("kel_surat_keterangan",$where);
			redirect("user/dashboard");
		}
	}
	function sukses($id_param)
	{
		if ($this->session->userdata('logged_in')!="") {

			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('SURAT KETERANGAN', base_url().'user/surat_keterangan');
			$this->breadcrumb->append_crumb('SUKSES', '/');
			$d['dt_retrieve'] = $this->app_user_web_model->generate_detail_surat_keterangan($id_param);
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/surat_keterangan/sukses');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');

		}
	}
}
