<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profile extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	function __construct()
	{
          // this is your constructor
		parent::__construct();
		//$this->load->model('app_global_superadmin_model');

		$where2		= array(
			'id_user' => $this->session->userdata("id_user"),
			'readStatus' => 1,
			);
		
		$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
		$this->session->set_userdata('notifUser', $jumlah);
	}
	
	function index()
	{
		if($this->session->userdata('logged_in')!="")
		{
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('EDIT PROFILE MEMBER', '/');
			$d['title'] = "Edit Profile";
			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/user/profile/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
			
	}

	function set()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$id['id_user'] = $this->session->userdata("id_user");
			$in['nama'] = $this->input->post("nama");
			$in['email'] = $this->input->post("email");
			$in['alamat'] = $this->input->post("alamat");
			$in['profesi'] = $this->input->post("profesi");
			$in['no_telpon'] = $this->input->post("no_telpon");
			
			$cek_email = $this->db->get_where("sam_user",array("email"=>$in['email']))->num_rows();
			if($cek_email>0 && $this->input->post("email_temp")!=$this->input->post("email"))
			{
				$this->session->set_flashdata('result', 'Email telah terpakai');
				redirect("user/profile");
			}
			else
			{
				
				$this->db->update("sam_user",$in,$id);
				$this->session->set_userdata($in);
				$this->session->set_flashdata('result', 'Berhasil memperbaharui data profil');
				redirect("user/profile");
			
			}
		}
		else
		{
			redirect(base_url());
		}
	}
}
