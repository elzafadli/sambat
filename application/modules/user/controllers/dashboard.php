<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	function __construct()
	{
          // this is your constructor
		parent::__construct();
		//$this->load->model('app_global_superadmin_model');

		$where2		= array(
			'id_user' => $this->session->userdata("id_user"),
			'readStatus' => 1,
			);
		
		$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
		$this->session->set_userdata('notifUser', $jumlah);
	}

	function sambatAPI(){
		echo "Hello World";
	}
	 
	function index($title="",$uri=0)
	{
		if($this->session->userdata('logged_in')!="")
		{
			
			$id_param = $this->session->userdata("id_user");
			$where['id_user'] = $id_param;
			$gen_menu = $this->db->get_where("sam_user",$where);
			if($gen_menu->num_rows()==0)
			{
				redirect(base_url());
			}
			$menu_crumb = $gen_menu->row();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb(strtoupper($menu_crumb->nama), base_url().'web/kategori/get/'.$id_param.'/'.url_title($menu_crumb->nama,'-',TRUE));
			$d['title'] = "Dashboard";
			
			
			$d['dt_retrieve'] = $this->app_user_web_model->generate_detail_member($id_param,$uri);
			$d['title'] = "Dashboard";
			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/user/dashboard/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
	}
}
