<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiket extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('image_lib');
    }

	function index($uri=0)
	{
		if($this->session->userdata('logged_in')!="")
		{
			
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('TIKET BARU', '/');
			$d['title'] = "Tiket Sambat Baru";
			$d['jenis'] = $this->app_global_web_model->generate_jenis_tiket();
			$d['kategori'] = $this->app_global_web_model->generate_kategori_tiket();

			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/user/tiket/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');

			/*$spam = $this->app_user_web_model->getLastSend()->result();
			//print_r($d['lastSend']);
			$i = 0;
			foreach ($spam as $item) {
				$tanggal[$i] = $item->tanggal;
				$menit[$i] = $item->waktu;
				$i++;
			}
			echo $tanggal[1];

			$test = new DateTime($tanggal[0]);
			$test1 = new DateTime($tanggal[1]); 
	 
			$diff = $test->diff($test1);

			print_r($diff);
			echo "<br>";
			echo $menit[0];
			echo $menit[1];*/
			// $spam = $this->app_user_web_model->getLastSend()->row_array();
			// //echo $spam['tanggal'];
			
			// $now = date('m/d/Y h:i:sa');

			// $test = new DateTime($spam['tanggal']);
			// $test1 = new DateTime($now); 
	 
			// $diff = $test->diff($test1);

			// print_r($diff);
			// echo $diff->days;
			// echo "<br>";
			// echo $spam['waktu'];
			// echo date('i');
			// echo "<br>";
			// print_r($test);

		}
		else
		{
			redirect(base_url());
		}
			
	}

	function edit($id_param)
	{
		if($this->session->userdata('logged_in')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$q = $this->db->get_where("kel_surat_keterangan",$where)->row();
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_lokasi_select($id_param);
			$d['id_surat_ket'] = $id_param;
			$d['id_lokasi'] = $this->session->userdata("id_lokasi");
			$d['id_user'] = $this->session->userdata("id_user");
			$d['keterangan'] = $q->keterangan;
			
			if($this->session->userdata("id_user")!=$q->id_user)
			{
				redirect("user/dashboard");
			}
			
			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('SURAT KETERANGAN', base_url().'user/surat_keterangan');
			$this->breadcrumb->append_crumb('EDIT', '/');
			
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/surat_keterangan/bg_edit');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
			
	}

	function set()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$id_user = $this->session->userdata("id_user");
			$tiket = $this->input->post("tiket", TRUE);
			
			if(empty($_FILES['userfile']['name']))
				{
					

					$in['id_user'] = $this->session->userdata("id_user");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$in['tiket'] = $this->input->post("tiket", TRUE);
					$in['id_jenis'] = $this->input->post("id_jenis", TRUE);
					$in['id_kategori'] = $this->input->post("id_kategori", TRUE);
					$in['file'] = '0';

					$data = $this->app_user_web_model->getLastSend(1,$id_user)->row_array();

					if($data['tiket'] ==  $tiket){
						$this->session->set_flashdata('result', 'Isi Tiket Sama');
					}else{
						$this->db->insert("sam_tiket",$in);

						$spam = $this->app_user_web_model->getLastSend(2,$id_user)->result();

						$i = 0;
						foreach ($spam as $item) {
							$tanggal[$i] = $item->tanggal;
							$menit[$i] = $item->waktu;
							$i++;
						}
						//echo $spam['tanggal'];
						
						$now = date('m/d/Y h:i:sa');

						$test = new DateTime($tanggal[0]);
						$test1 = new DateTime($tanggal[1]); 
				 
						$diff = $test->diff($test1);

						if ($diff->d == 0 && $diff->i <= 5) {
							echo "spam";
							$tiket = $this->app_user_web_model->getLastSend(1,$id_user)->row_array();

							if ($this->app_user_web_model->deleteSpam($tiket['id_tiket'])) {
							}

							$this->session->set_flashdata('result', 'Silahkan tunggu 5 Menit lagi untuk mengirim tiket!!!');
						}else{
							$this->session->set_flashdata('result', 'Berhasil Memasukkan Tiket');
						}
					}

					//$this->session->set_userdata($in);		

					redirect("user/tiket");
				}
				else
				{
					$config['upload_path'] = './asset/images/member/';
					$config['allowed_types']= 'gif|jpg|png|jpeg';
					$config['encrypt_name']	= TRUE;
					$config['remove_spaces']	= TRUE;	
					$config['max_size']     = '2000';
					$config['max_width']  	= '2000';
					$config['max_height']  	= '2000';
			 
					$this->load->library('upload', $config);
	 
					if ($this->upload->do_upload("userfile")) {
						$data	 	= $this->upload->data();
			 
						/* PATH */
						$source             = "./asset/images/member/".$data['file_name'] ;
						$destination_thumb	= "./asset/images/member/thumb/" ;
			 
						// Permission Configuration
						chmod($source, 0777) ;
			 
						/* Resizing Processing */
						// Configuration Of Image Manipulation :: Static
						$this->load->library('image_lib') ;
						$img['image_library'] = 'GD2';
						$img['create_thumb']  = TRUE;
						$img['maintain_ratio']= TRUE;
			 
						/// Limit Width Resize
						$limit_thumb    = 400 ;
			 
						// Size Image Limit was using (LIMIT TOP)
						$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
			 
						// Percentase Resize
						if ($limit_use > $limit_thumb) {
							$percent_thumb  = $limit_thumb/$limit_use ;
						}
			 
						//// Making THUMBNAIL ///////
						$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
						$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;
			 
						// Configuration Of Image Manipulation :: Dynamic
						$img['thumb_marker'] = '';
						$img['quality']      = '90%' ;
						$img['source_image'] = $source ;
						$img['new_image']    = $destination_thumb ;
			 
						// Do Resizing
						$this->image_lib->initialize($img);
						$this->image_lib->resize();
						$this->image_lib->clear() ;
						
						if($this->input->post("userfile")!="")
						{
							$old_thumb	= "./asset/images/member/thumb/".$this->input->post("userfile")."" ;
							unlink($old_thumb);
						}
						unlink($source);
						
						$in['file'] = $data['file_name'];
						$in['id_user'] = $this->session->userdata("id_user");
						$in['tanggal'] = time()+3600*7;
						$in['st'] = "0";
						$in['tiket'] = $this->input->post("tiket", TRUE);
						$in['id_jenis'] = $this->input->post("id_jenis", TRUE);
						$in['id_kategori'] = $this->input->post("id_kategori", TRUE);
						$data = $this->app_user_web_model->getLastSend(1,$id_user)->row_array();

					if($data['tiket'] ==  $tiket){
						$this->session->set_flashdata('result', 'Isi Tiket Sama');
					}else{
						$this->db->insert("sam_tiket",$in);

						$spam = $this->app_user_web_model->getLastSend(2,$id_user)->result();

						$i = 0;
						foreach ($spam as $item) {
							$tanggal[$i] = $item->tanggal;
							$menit[$i] = $item->waktu;
							$i++;
						}
						//echo $spam['tanggal'];
						
						$now = date('m/d/Y h:i:sa');

						$test = new DateTime($tanggal[0]);
						$test1 = new DateTime($tanggal[1]); 
				 
						$diff = $test->diff($test1);

						if ($diff->d == 0 && $diff->i <= 5) {
							echo "spam";
							$tiket = $this->app_user_web_model->getLastSend(1,$id_user)->row_array();

							if ($this->app_user_web_model->deleteSpam($tiket['id_tiket'])) {
							}

							$this->session->set_flashdata('result', 'Silahkan tunggu 5 Menit lagi untuk mengirim tiket!!!');
						}else{
							$this->session->set_flashdata('result', 'Berhasil Memasukkan Tiket');
						}
					}

						//$this->session->set_userdata($in);
						redirect("user/tiket");
					}
					else 
					{
						echo $this->upload->display_errors('<p>','</p>');
					}
				}
		}
	}

	function update()
	{
				
				$config['upload_path'] = './asset/file';
				$config['allowed_types'] = 'zip|pdf';
				$config['encrypt_name']	= TRUE;
				$config['remove_spaces']= TRUE;	
				$config['max_size']     = '40000';
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('file'))
				{
					$data = $this->upload->data();
					$id['id_surat_ket'] = $this->input->post("id_surat_ket"); 
					$in['id_lokasi'] = $this->session->userdata("id_lokasi");
					$in['id_user'] = $this->session->userdata("id_user");
					$in['keterangan'] = $this->input->post("keterangan");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$in['file'] = $data['file_name'];
					$this->db->update("kel_surat_keterangan",$in,$id);
					redirect("user/surat_keterangan/sukses/".$in['id_user']."");
				}
				else
			{ 
					$id['id_surat_ket'] = $this->input->post("id_surat_ket");
					$in['id_lokasi'] = $this->session->userdata("id_lokasi");
					$in['id_user'] = $this->session->userdata("id_user");
					$in['keterangan'] = $this->input->post("keterangan");
					$in['tanggal'] = time()+3600*7;
					$in['st'] = "0";
					$this->db->update("kel_surat_keterangan",$in,$id);
					redirect("user/surat_keterangan/sukses/".$in['id_user']."");
			}
	}

	function hapus($id_param)
	{
		if($this->session->userdata('logged_in')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$this->db->delete("kel_surat_keterangan",$where);
			redirect("user/dashboard");
		}
	}
	function sukses($id_param)
	{
		if ($this->session->userdata('logged_in')!="") {

			$this->breadcrumb->append_crumb('BERANDA', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('SURAT KETERANGAN', base_url().'user/surat_keterangan');
			$this->breadcrumb->append_crumb('SUKSES', '/');
			$d['dt_retrieve'] = $this->app_user_web_model->generate_detail_surat_keterangan($id_param);
			$this->load->view($_SESSION['site_theme'].'/bg_header_user',$d);
			$this->load->view($_SESSION['site_theme'].'/user/surat_keterangan/sukses');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');

		}
	}
}
