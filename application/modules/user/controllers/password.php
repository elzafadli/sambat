<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class password extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	
	function __construct()
	{
          // this is your constructor
		parent::__construct();
		//$this->load->model('app_global_superadmin_model');

		$where2		= array(
			'id_user' => $this->session->userdata("id_user"),
			'readStatus' => 1,
			);
		
		$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
		$this->session->set_userdata('notifUser', $jumlah);
	}
	 
	
	function index()
	{
		if($this->session->userdata('logged_in')!="")
		{
			
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb('EDIT PASSWORD USER', '/');
			$d['title'] = "Edit Password";
			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/user/password/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect(base_url());
		}
			
	}

	function set()
	{
		if($this->session->userdata('logged_in')!="")
		{
			$id['id_user'] = $this->session->userdata("id_user");
			$id['email'] = $this->session->userdata("email");

			if($this->session->userdata('password')){
				$id['password'] = $this->session->userdata('password');
			}else{
				$id['password'] = md5($this->input->post("pass_lama").$this->config->item('key_login'));
			}
			
			$in['pass_lama'] = md5($this->input->post("pass_lama").$this->config->item('key_login'));
			$in['pass_baru'] = $this->input->post("pass_baru");
			$in['ulangi_pass'] = $this->input->post("ulangi_pass");
			
			$cek_pass = $this->db->get_where("sam_user",$id)->num_rows();
			if($cek_pass>0)
			{
				if($in['pass_baru']==$in['ulangi_pass'])
				{
					$up['password'] = md5($this->input->post("ulangi_pass").$this->config->item('key_login'));
					$id_up['id_user'] = $this->session->userdata("id_user");
					$id_up['email'] = $this->session->userdata("email");
					$this->db->update("sam_user",$up,$id_up);
					$this->session->set_flashdata('result', 'Password Berhasil Diperbaharui');

					//unset($_SESSION['password']);
					$this->session->unset_userdata('password');
					//$this->session->sess_destroy('password');
					redirect("user/password");
				}
				else
				{
					$this->session->set_flashdata('result', 'Password Baru Tidak Sama');
					redirect("user/password");
				}
			}
			else
			{
				$this->session->set_flashdata('result', 'Password Lama Salah');
				redirect("user/password");
			}
		}
		else
		{
			redirect(base_url());
		}
	}
}
