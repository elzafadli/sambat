<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pesan extends MX_Controller {

	/**
		 * @author : Wewaits
		 * @twitter : @wewaits
		 **/
	function __construct()
	{
	          // this is your constructor
		parent::__construct();
			//$this->load->model('app_global_superadmin_model');
		$where		= array(
				'id_user' => $this->session->userdata("id_user"),
				);

		$this->app_global_web_model->update_pesan($where);

		$where2		= array(
			'id_user' => $this->session->userdata("id_user"),
			'readStatus' => 1,
			);

		$jumlah = $this->app_global_web_model->get_pesan($where2)->num_rows();
		$this->session->set_userdata('notifUser', $jumlah);
	}

	public function delete($id){
		$this->db->where('id_notifikasi', $id);

		if($this->db->delete('notifikasi')){
			$this->session->set_flashdata('result', 'Pesan berhasil di Hapus !!!');
			redirect('user/pesan','refresh');
		}

	}

	public function index($uri=0)
	{
		if($this->session->userdata('logged_in')!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb('DASHBOARD', base_url().'user/dashboard');
			$this->breadcrumb->append_crumb("PESAN DARI KOMINFO", '/');

			$d['title'] = "Tiket Sambatan Anda";
				//$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_kategori($this->config->item("limit_item"),$uri);
			
			$id_param = $this->session->userdata("id_user");
			$where		= array(
					'id_user' => $id_param,
					);
			
			$d['pesan'] = $this->app_global_web_model->get_pesan($where)->result();


			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/user/pesan/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');


		}
		else
		{
			redirect(base_url());
		}
	}
	}

	?>