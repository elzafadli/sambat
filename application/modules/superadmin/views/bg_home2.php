<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>

  $( document ).ready(function(){
       $("#cari").click(function(){
                   //var nama = $("#nama").val();
                   $('#bar-chart').hide();
                   $('#top').hide();
                   $('#bar-chart2').hide();
                   $('#top2').hide();
                   $('#list').hide();
                   $('#list2').hide();
                   var parent = $("#country_details").val();
                   var child = $("#old_state").val();
                   $.ajax({
                       type:"post",
                       url:"<?php echo base_url(); ?>superadmin/dashboard/skpdGrafik",
                       data:{tanggal1: parent, tanggal2: child},
                       success: function (data) {
                         $('#grafik1').html(data);
                     },
                     error: function (ErrorResponse) {
                       alert("Error : Silahkan pilih OPD dahulu !!!");
                   }
               })
                   return false;
               });
   });

  function getchild(id)
  {
        //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('superadmin/user/get_skpd').'/';?>'+id,
            data: id='parent_id',
            success: function(data){
            //alert(data);
            $('#old_state').html(data);
        },
    });
    }

  

</script>


<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard/grafik">
                        <span class="mif-chart-bars icon"></span>
                        <span class="title">Grafik</span>
                        <span class="counter">Pengaduan per OPD</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <h1 class="text-light">OPD</h1>
                <hr class="thin bg-grayLighter">
                <?php //echo form_open("superadmin/arsip/cari"); ?>
                <div class="example" data-text="CC Tiket">
                    <div class="row flex-just-sb">
                        <div class="cell colspan5">
                            <dl>
                                <dt>Pilih OPD :</dt>
                                <div class="input-control select full-size">
                                    <select name="id_skpd0" id="country_details" onChange="getchild(this.value)">
                                        <option value="" selected="selected" >Pilih OPD</option>
                                        <?php foreach($parent as $k): ?>
                                            <option value="<?php echo $k->id_skpd; ?>"><?php echo $k->nama_skpd; ?></option>
                                        <?php endforeach; ?> 
                                    </select>
                                </div>
                            </dl>
                        </div>
                        <div class="cell colspan5">
                            <dl>
                                <dt>Pilih Sub OPD :</dt>
                                <div class="input-control select full-size">
                                    <select name="id_skpd" id="old_state">
                                        <option selected="selected" value="0">Pilih Sub OPD</option> 
                                    </select>
                                </div>
                            </dl>
                        </div>
                    </div>
                    <button id="cari" class="button loading-pulse lighten primary">Cari Data</button>
                </div>
                <?php   //echo $tanggal2; ?>
                <div id="grafik1"></div>
                <center><h3 id="top">Top 5 OPD penerima pengaduan Web Tiket Terbanyak</h3></center>
                <canvas id="bar-chart" width="800" height="450"  ></canvas>
                <div id="list">
                    <ul>
                    <?php
                        $i = 0;
                        foreach (range('A', 'E') as $char) {
                            echo "<li>".$char." : ".$skpd[$i]->nama."</li>". "<br>";
                            $i++;
                        }
                    ?>
                    </ul>
                </div>
                <center><h3 id="top2">Top 5 OPD penerima pengaduan Web SMS Terbanyak</h3></center>
                <canvas id="bar-chart2" width="800" height="450" ></canvas>
                <div id="list2">
                    <ul>
                    <?php
                        $i = 0;
                        foreach (range('A', 'E') as $char) {
                            echo "<li>".$char." : ".$skpd2[$i]->nama."</li>". "<br>";
                            $i++;
                        }
                    ?>
                    </ul>
                </div>
                <script type="text/javascript">
  // Bar chart
                  new Chart(document.getElementById("bar-chart"), {
                    type: 'bar',
                    data: {
                    labels: [
                      <?php
                            foreach (range('A', 'E') as $char) {
                                echo '"'.$char.'",' ;
                            }
                        ?>
                    ],   
                      datasets: [
                      {
                        label: "Tiket",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [
                        <?php foreach ($skpd as $item) { ?>
                        <?php echo $item->jumlah; ?>,
                        <?php } ?>
                        ]
                    }
                    ]
                },
                
                 options: {
                            
                          legend: { display: false },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
                });


                  new Chart(document.getElementById("bar-chart2"), {
                    type: 'bar',
                    data: {
                    labels: [
                    <?php
                            foreach (range('A', 'E') as $char) {
                                echo '"'.$char.'",' ;
                            }
                        ?>
                    ],   
                      datasets: [
                      {
                        label: "Tiket",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [
                        <?php foreach ($skpd2 as $item) { ?>
                        <?php echo $item->jumlah; ?>,
                        <?php } ?>
                        ]
                    }
                    ]
                },
                 options: {

                  legend: { display: false },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
                });

                </script>


            