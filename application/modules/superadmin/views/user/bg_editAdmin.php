<script>
    function getchild(id)
    {
        //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('superadmin/user/get_skpd').'/';?>'+id,
            data: id='parent_id',
            success: function(data){
            //alert(data);
            $('#old_state').html(data);
        },
    });
    }
</script>
<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <hr class="thin bg-grayLighter">
                <?php if($this->session->flashdata("Update")){ ?>
                <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("Update"); ?>
                        </div>
                <?php } ?>
                <?php echo form_open("superadmin/user/setAdmin/"); ?>
                <div class="example" data-text="Form Isian User">
                    <div class="row flex-just-sb">
                        <div class="cell colspan5">
                            <label>Username</label>
                            <div class="input-control text full-size">
                                <span class="mif-user prepend-icon"></span>
                                <input type="hidden" id="id" name="id"  value="<?php echo $data['id_admin']; ?>">
                                <input id="username" name="username" placeholder="Username Admin" value="<?php echo $data['username']; ?>" disabled>
                            </div>
                        </div>
                        <div class="cell colspan5">
                            <label>Nama Admin</label>
                            <div class="input-control text full-size">
                                <span class="mif-spell-check prepend-icon"></span>
                                <input type="text" id="nama_user" name="nama_user" placeholder="Nama Admin" value="<?php echo $data['nama']; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="row flex-just-sb">
                        <div class="cell colspan5">
                            <label>Masukkan Password Baru</label>
                            <div class="input-control password full-size">
                                <span class="mif-key prepend-icon"></span>
                                <input type="hidden" id="password" name="passwordTemp" value="<?php echo $data['password']?>">
                                <input type="password" id="password" name="password" placeholder="Password Admin" value="">
                            </div>
                        </div>

                        <div class="cell colspan5">
                            <label>No Handphone</label>
                            <div class="input-control text full-size">
                                <span class="mif-mobile prepend-icon"></span>
                                <input type="number" id="no_hp" name="no_hp" placeholder="No Handphone" value="<?php echo $data['no_hp']; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="row flex-just-sb">
                        <div class="cell colspan5">
                            <label>Email</label>
                            <div class="input-control text full-size">
                                <span class="mif-mail prepend-icon"></span>
                                <input id="email" name="email" placeholder="Email Admin" value="<?php echo $data['email']; ?>" disabled>
                            </div>
                        </div>
                        <div class="cell colspan5">
                            <label>Group SMS</label>
                            <div class="input-control select full-size">
                                <select name="GroupID" id="old_state">
                                <?php foreach ($group as $item) { 
                                    if ($item->ID == $data['GroupID']) { 
                                    ?><option value="<?php echo $item->ID; ?>" selected><?php echo $item->Name; ?> </option>
                                    <?php }else{ ?>
                                    <option value="<?php echo $item->ID; ?>"><?php echo $item->Name; ?> </option>
                                    <?php } ?>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="button loading-pulse lighten primary">Simpan Data</button>
                </div>
                <?php echo form_close(); ?>