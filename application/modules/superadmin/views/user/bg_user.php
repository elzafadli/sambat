<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <h1 class="text-light">User Management</h1>
                <hr class="thin bg-grayLighter">
                <table id="example">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>ID User</td>

                            <td>Username</td>
                            <td>Email</td>
                            <td>No.Telp</td>
                            <td>Status</td>
                            <td>Kode Aktivasi</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <?php 
                    $i = 1;
                    foreach ($list as $item) { 
                        if($item->stts == 1){
                            $status = "Aktif";
                        }else{
                            $status = "Belum di Aktifasi";
                        }
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $item->id_user; ?></td>
                            <td><?php echo $item->username; ?></td>
                            <td><?php echo $item->email; ?></td>
                            <td><?php echo $item->no_telpon; ?></td>
                            <td><?php echo $status; ?></td>
                            <td><?php echo $item->kode_aktivasi; ?></td>
                            <td>

                                <a href='<?php echo base_url();?>superadmin/user/editMasyarakat/<?php echo $item->id_user; ?>' class='button warning'><span class='mif-pencil'></span></a>
                                <a href='<?php echo base_url();?>superadmin/user/hapusUser/<?php echo $item->id_user; ?>' onClick="return confirm('Anda Yakin Ingin Menghapus?')" class='button alert'><span class='mif-bin'></span></a>
                            </td>
                        </tr>
                        <?php $i++;} ?>
                    </thead>
                </table>
                <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
                <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

                <script>
                  $(function(){
                    $("#example").dataTable();
                })
            </script>