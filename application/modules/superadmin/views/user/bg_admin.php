<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <h1 class="text-light">User Management</h1>
                <hr class="thin bg-grayLighter">
                <table class='dataTable border bordered' data-role='datatable' data-auto-width='false'>
                    <thead>
                        <tr>
                        <td class='sortable-column sort-asc' style='width: 60px'>No.</td>
                            <td class='sortable-column'>Username</td>
                            <td class='sortable-column'>Email</td>
                            <td class='sortable-column'>Nama</td>
                            <td class='sortable-column'>No.Telp</td>
                            <td class='sortable-column'><a href='<?php echo base_url(); ?>superadmin/user/addAdmin' class='image-button primary small-button'>Tambah User<span class='icon mif-user-plus bg-darkCobalt'></span></a></td>
                        </tr>
                    </thead>
                    <?php 
                    $i = 1;
                    foreach ($list as $item) { ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $item->username;?></td><td><?php echo $item->email;?></td><td><?php echo $item->nama;?></td><td><?php echo $item->no_hp;?></td>
                        <td>
                            <a href='<?php echo base_url();?>superadmin/user/editAdmin/<?php echo $item->id_admin; ?>' class='button warning'><span class='mif-pencil'></span></a> 
                            <a href='<?php echo base_url();?>superadmin/user/hapusAdmin/<?php echo $item->id_admin; ?>' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
                        </tr>
                        <?php $i++;} ?>
                    </thead>
                </table>