<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
    <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

    <link rel='shortcut icon' type='image/x-icon' href='../favicon.ico' />

    <title><?php echo $title; ?> - <?php echo $_SESSION['site_title'].' - '.$_SESSION['site_quotes']; ?></title>
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-responsive.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery-2.1.3.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/metro.js"></script>


   


    <style>
        html, body {
            height: 100%;
        }
        body {
        }
        .page-content {
            padding-top: 3.125rem;
            min-height: 100%;
            height: 100%;
        }
        .table .input-control.checkbox {
            line-height: 1;
            min-height: 0;
            height: auto;
        }

        @media screen and (max-width: 800px){
            #cell-sidebar {
                flex-basis: 52px;
            }
            #cell-content {
                flex-basis: calc(100% - 52px);
            }
        }
    </style>

    <script>

        $(function(){
            $('.sidebar').on('click', 'li', function(){
                if (!$(this).hasClass('active')) {
                    $('.sidebar li').removeClass('active');
                    $(this).addClass('active');
                }
            })
        })
    </script>
</head>
<body>
	<!-- Menu atas -->
	<div class="app-bar fixed-top darcula" data-role="appbar">
        <a class="app-bar-element branding">Sambat Online V1.0</a>
        <span class="app-bar-divider"></span>
        <?php if ($this->session->userdata("id_skpd")=='0') { ?>
        <ul class="app-bar-menu">
            <li>
                <a  class="dropdown-toggle">Konfigurasi</a>
                <ul class="d-menu" data-role="dropdown">
                    <li><a href="<?php echo base_url(); ?>superadmin/user/admin">User Management Admin</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/user">User Management OPD</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/user/masyarakat">User Management Masyarakat</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/sistem">System</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/skpd">OPD</a></li>
                </ul>
            </li>
            <li>
                <a href="" class="dropdown-toggle">SMS Tools</a>
                <ul class="d-menu" data-role="dropdown">
                    <li><a href="<?php echo base_url(); ?>superadmin/phonebook">Tambah Phonebook (Personal)</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/sendsms">Kirim SMS Personal</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/group">Group SMS</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/broadcast">Broadcast SMS</a></li>
                </ul>
            </li>
            <!--
            <li style="margin-left: 550px;">

            <a href="<?php echo base_url(); ?>superadmin/pesan"><span class="mif-envelop icon"></span> Pesan <span class="badge" style="background-color: #2ecc71;font-size: 14px;border-radius: 10px;"> &nbsp;&nbsp;<?php echo $jumlah; ?> &nbsp;</a>
            </li> -->
        </ul>
        <?php } ?>
        <div class="app-bar-element place-right">

            <span>
               <a style="color: white;margin-right: 5px" href="<?php echo base_url(); ?>superadmin/pesan"><span class="mif-envelop icon <?php if($this->session->userdata('notifAdmin') != 0){?>mif-ani-bounce<?php } ?>"></span> Pesan <span class="badge mif-ani-heartbeat" style="background-color: #2ecc71;font-size: 14px;border-radius: 10px;"> &nbsp;&nbsp;<?php echo $this->session->userdata('notifAdmin'); ?> &nbsp;</a>
            </span>
            
            <span class="dropdown-toggle"><span class="mif-cog"></span> <?php echo $this->session->userdata("nama"); ?></span>
            <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" data-no-close="true" style="width: 220px">
                <h2 class="text-light">Menu Anda</h2>
                <ul class="unstyled-list fg-dark">
                    <li><a href="<?php echo base_url(); ?>superadmin/profil" class="fg-white1 fg-hover-yellow">User Profile</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/password" class="fg-white2 fg-hover-yellow">User Password</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/logout" class="fg-white3 fg-hover-yellow">Log Out</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Menu atas -->
