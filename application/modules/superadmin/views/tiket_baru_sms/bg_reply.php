<script>
    function getchild(id)
        {
        //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('superadmin/user/get_skpd').'/';?>'+id,
            data: id='parent_id',
           success: function(data){
            //alert(data);
            $('#old_state').html(data);
            },
            });
        }
</script>
<div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">
            <div class="row" style="height: 100%">
                <!-- Sidebar -->
                <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                    <ul class="sidebar">
                        <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                            <span class="mif-list-numbered icon"></span>
                            <span class="title">Jenis</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                            <span class="mif-flow-tree icon"></span>
                            <span class="title">Kategori</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket SMS</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                            <span class="mif-contacts-mail icon"></span>
                            <span class="title">SMS</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket SMS</span>
                        </a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                            <span class="mif-mobile icon"></span>
                            <span class="title">Tiket SMS</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                            <span class="mif-contacts-mail icon"></span>
                            <span class="title">SMS</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /Sidebar -->
                <!-- Konten kanan -->
                <div class="cell auto-size padding20 bg-white" id="cell-content">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    <h1 class="text-light">Tanggapan SMS Sambatan</h1>
                    <hr class="thin bg-grayLighter">
                        <div class="example" data-text="Detail Tiket">
                            <?php foreach ($sms_cc as $t): ?>
                            <div class="row flex-just-sb">
                                <div class="cell colspan5">
                                    <dl>
                                        <dt>ID SMS :</dt>
                                        <dd><span class="tag info">#<?php echo $t->ID; ?></span></dd>
                                    </dl>
                                </div>
                                <div class="cell colspan5">
                                    <dl>
                                        <dt>Tanggal/Waktu :</dt>
                                        <dd><span class="tag info"><?php echo $t->ReceivingDateTime; ?></span></dd>
                                    </dl>
                                </div>
                            </div>
                             <div class="row flex-just-sb">
                                <div class="cell colspan5">
                                    <dl>
                                        <dt>Isi SMS :</dt>
                                        <dd>
                                            <?php echo $t->TextDecoded; ?>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            
                        </div>
                    <?php echo form_open("superadmin/tiket_baru_sms/jawabtiketKominfo"); ?>
                        <?php if($this->session->flashdata("result")){ ?>
                        <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                        </div>
                        <?php } ?>
                        <div class="example" data-text="Tanggapan Tiket">
                            <div class="row flex-just-sb">
                                <div class="cell colspan12">
                                    <dl>
                                        <dt>Tanggapan Anda :</dt>
                                        <div class="input-control textarea full-size">
                                            <textarea name="tanggapan" id="tanggapan"></textarea>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan12">
                                    <dl>
                                        <dt>Tanggapan Anda :</dt>
                                        <?php //echo $skpd_cc; ?>
                                    </dl>
                                </div>
                            </div>
                             <input type="hidden" name="no_hp" value="<?php echo $t->SenderNumber; ?>" />
                        <?php endforeach; ?>
                            <input type="hidden" name="id_param" value="<?php echo $id_param; ?>" />
                            <button class="button loading-pulse lighten primary">Jawab SMS</button>
                            <a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms/<?php echo $this->session->userdata('page');?>" class="button loading-pulse lighten primary">Kembali</a>
                        </div>
                    <?php echo form_close(); ?>