<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>

  $( document ).ready(function(){
       $("#cari").click(function(){
                   //var nama = $("#nama").val();
                   $('#bar-chart').hide();
                   $('#top1').hide();
                   $('#bar-chart2').hide();
                   $('#top2').hide();
                   var tgl1 = $("#bulan").val();
                   var tgl2 = $("#tahun").val();
                   $.ajax({
                       type:"post",
                       url:"<?php echo base_url(); ?>superadmin/dashboard/jsonGrafik",
                       data:{bulan: tgl1, tahun: tgl2},
                       success: function (data) {
                         $('#grafik1').html(data);
                     },
                     error: function (ErrorResponse) {
                       alert("error");
                   }
               })
                   return false;
               });
   })

</script>


<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard/grafik">
                        <span class="mif-chart-bars icon"></span>
                        <span class="title">Grafik</span>
                        <span class="counter">Pengaduan per OPD</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard/grafik">
                        <span class="mif-chart-bars icon"></span>
                        <span class="title">Grafik</span>
                        <span class="counter">Pengaduan per OPD</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                <h1 class="text-light">Tiket Pengaduan</h1>
                <hr class="thin bg-grayLighter">
                <?php //echo form_open("superadmin/arsip/cari"); ?>
                <div class="example" data-text="Cari Tiket">    
                    
                    <div class="row flex-just-sb">
                        <div class="cell colspan5">
                            <label>Bulan</label>
                            <div class="input-control text full-size">
                                <select name="bulan" form="carform" id="bulan">
                                  <option value="-">-</option>
                                  <option value="1">Januari</option>
                                  <option value="2">Ferbuari</option>
                                  <option value="3">Maret</option>
                                  <option value="4">April</option>
                                  <option value="5">Mei</option>
                                  <option value="6">Juni</option>
                                  <option value="7">Juli</option>
                                  <option value="8">Agustus</option>
                                  <option value="9">September</option>
                                  <option value="10">Oktober</option>
                                  <option value="11">November</option>
                                  <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="cell colspan5">
                            <label>Tahun</label>
                            <div class="input-control text full-size">
                                <select name="tahun" form="carform" id="tahun">
                                <option value="-">-</option>
                                <?php
                                foreach ($tahun as $value) { ?>
                                <option value="<?php echo $value->tahun; ?>"><?php echo $value->tahun; ?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <button id="cari" class="button loading-pulse lighten primary">Cari Data</button>
                </div>
                <?php } ?>
                <div id="grafik1"></div>
                <center><h3 id="top1">Pengaduan WEB Tiket Tahun <?php echo date("Y"); ?></h3></center>
                <canvas id="bar-chart" width="800" height="250"></canvas>

                <center><h3 id="top2">Pengaduan SMS Tiket Tahun <?php echo date("Y"); ?></h3></center>
                <canvas id="bar-chart2" width="800" height="250"></canvas>

                <script type="text/javascript">
  // Bar chart
                  new Chart(document.getElementById("bar-chart"), {
                    type: 'bar',
                    data: {
                    labels: [
                    <?php foreach ($bar as $item) { 
                    if($item->bulan == "01"){
                        $data = "Januari";
                    }elseif($item->bulan == "02"){
                        $data = "Februari";
                    }elseif($item->bulan == "03"){
                        $data = "Maret";
                    }elseif($item->bulan == "04"){
                        $data = "April";
                    }elseif($item->bulan == "05"){
                        $data = "Mei";
                    }elseif($item->bulan == "06"){
                        $data = "Juni";
                    }elseif($item->bulan == "07"){
                        $data = "July";
                    }elseif($item->bulan == "08"){
                        $data = "Agustus";
                    }elseif($item->bulan == "09"){
                        $data = "September";
                    }elseif($item->bulan == "10"){
                        $data = "Oktober";
                    }elseif($item->bulan == "11"){
                        $data = "November";
                    }else{
                        $data = "Desember";
                    }
                        ?>
                        "<?php 
                        echo $data; ?> ",
                    <?php } ?>  
                    ],   
                      datasets: [
                      {
                        label: "Tiket",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [
                        <?php foreach ($bar as $item) { ?>
                        <?php echo $item->jumlah; ?>,
                        <?php } ?>
                        ]
                    }
                    ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: false,
                    text: ''
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
                }
                });

                  new Chart(document.getElementById("bar-chart2"), {
                    type: 'bar',
                    data: {
                    labels: [
                    <?php foreach ($bar2 as $item) { 
                    if($item->bulan == "01"){
                        $data = "Januari";
                    }elseif($item->bulan == "02"){
                        $data = "Februari";
                    }elseif($item->bulan == "03"){
                        $data = "Maret";
                    }elseif($item->bulan == "04"){
                        $data = "April";
                    }elseif($item->bulan == "05"){
                        $data = "Mei";
                    }elseif($item->bulan == "06"){
                        $data = "Juni";
                    }elseif($item->bulan == "07"){
                        $data = "July";
                    }elseif($item->bulan == "08"){
                        $data = "Agustus";
                    }elseif($item->bulan == "09"){
                        $data = "September";
                    }elseif($item->bulan == "10"){
                        $data = "Oktober";
                    }elseif($item->bulan == "11"){
                        $data = "November";
                    }else{
                        $data = "Desember";
                    }
                        ?>
                        "<?php 
                        echo $data; ?> ",
                    <?php } ?>
                    ],   
                      datasets: [
                      {
                        label: "Tiket",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [
                        <?php foreach ($bar2 as $item) { ?>
                        <?php echo $item->jumlah; ?>,
                        <?php } ?>
                        ]
                    }
                    ]
                },
                 options: {
                    legend: { display: false },
                  title: {
                    display: false,
                    text: ''
                
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
                });


                </script>


            