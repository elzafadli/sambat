 <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/select2.min.js"></script>
 <script>
        $(function(){
            $(".js-select").select2({
                placeholder: "Klik untuk Pilih",
                allowClear: true
            });
        });

    </script>
<div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">
            <div class="row" style="height: 100%">
                <!-- Sidebar -->
                <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                    <ul class="sidebar">
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                            <span class="mif-list-numbered icon"></span>
                            <span class="title">Jenis</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                            <span class="mif-flow-tree icon"></span>
                            <span class="title">Kategori</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                            <span class="mif-mobile icon"></span>
                            <span class="title">Tiket SMS</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                            <span class="mif-contacts-mail icon"></span>
                            <span class="title">SMS</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket SMS</span>
                        </a></li>
                    </ul>
                </div>
                <!-- /Sidebar -->
                <!-- Konten kanan -->
                <div class="cell auto-size padding20 bg-white" id="cell-content">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
					<h1 class="text-light"></i> Kirim SMS</h1>
                    <hr class="thin bg-grayLighter">
                    <?php if($this->session->flashdata("result")){ ?>
                        <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                        </div>
                        <?php } ?>
					<?php echo form_open("superadmin/sendsms/simpan"); ?>
					<div class="example" data-text="Form Isian SMS">
                         <div class="row flex-just-sb">
                            <div class="cell colspan10">
                                <dl>
                                    <dt>Pilih dari Phonebook</dt>
                                    <select name="Number" class="js-select full-size">
                                        <option></option>
                                        <?php foreach($phonebook as $g): ?>
                                        <option value="<?php echo $g->Number; ?>"><?php echo $g->Name; ?> (<?php echo $g->Number; ?>)</option>
                                        <?php endforeach; ?> 
                                    </select>
                                </dl>         
                            </div>
                         </div>
                        <div class="row flex-just-sb">
                            <div class="cell colspan12">
                                    <dl>
                                        <dt>ISI SMS :</dt>
                                        <div class="input-control textarea full-size">
                                            <textarea name="isi" id="isi"></textarea>
                                        </div>
                                    </dl>
                                </div>
                        </div>
                        <button class="button loading-pulse lighten primary">Kirim SMS</button>
                     </div>
                     <?php echo form_close(); ?>