<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>

  $( document ).ready(function(){
       $("#cari").click(function(){
                   //var nama = $("#nama").val();
                   $('#bar-chart').hide();
                   $('#top').hide();
                   $('#bar-chart2').hide();
                   $('#top2').hide();
                   $('#list').hide();
                   $('#list2').hide();
                   var parent = $("#country_details").val();
                   var child = $("#old_state").val();
                   $.ajax({
                       type:"post",
                       url:"<?php echo base_url(); ?>superadmin/dashboard/skpdGrafik",
                       data:{tanggal1: parent, tanggal2: child},
                       success: function (data) {
                         $('#grafik1').html(data);
                     },
                     error: function (ErrorResponse) {
                       alert("Error : Silahkan pilih OPD dahulu !!!");
                   }
               })
                   return false;
               });
   });

  function getchild(id)
  {
        //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('superadmin/user/get_skpd').'/';?>'+id,
            data: id='parent_id',
            success: function(data){
            //alert(data);
            $('#old_state').html(data);
        },
    });
    }

  

</script>


<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard/grafik">
                        <span class="mif-chart-bars icon"></span>
                        <span class="title">Grafik</span>
                        <span class="counter">Pengaduan per OPD</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li class="active"><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard/grafik">
                        <span class="mif-chart-bars icon"></span>
                        <span class="title">Grafik</span>
                        <span class="counter">Pengaduan per OPD</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <h1 class="text-light">OPD</h1>
                <hr class="thin bg-grayLighter">
                
                <center><h3 id="top">Pengaduan Tiket OPD <?php echo $skpd['nama']; ?></h3></center>
                <canvas id="bar-chart" width="800" height="250"></canvas>

                <center><h3 id="top">Pengaduan WEB Diterima vs Dijawab</h3></center>
                <canvas id="pie-chart" width="1200" height="350"></canvas>
                <br>
                <center><h3 id="top">Pengaduan SMS Diterima vs Dijawab </h3></center>
                <canvas id="pie-chart2" width="1200" height="350"></canvas>

                <script type="text/javascript">
                  // Bar chart
                  new Chart(document.getElementById("bar-chart"), {
                    type: 'bar',
                    data: {
                      labels: ["Web Tiket", "Web SMS"],
                      datasets: [
                        {
                          label: ["Web Tiket"],
                          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                          data: [<?php echo $tiket; ?>,<?php echo $tiketSMS; ?>]
                        }
                      ]
                    },
                    options: {
                        legend: { display: false },
                                  title: {
                                    display: false,
                                    text: ''
                                
                                },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });

                  <?php $tiketTidakCC = $tiket - $tiketCC; ?>
                  new Chart(document.getElementById("pie-chart"), {
                    type: 'pie',
                    data: {
                      labels: ["Tiket Dijawab", "Tiket Diterima","Tiket Tidak Dijawab"],
                      datasets: [{
                        label: "Population (millions)",
                        backgroundColor: ["#2ecc71","#3e95cd","#e74c3c"],
                        data: [<?php echo $tiketCC; ?>,<?php echo $tiket; ?>,<?php echo $tiketTidakCC; ?>]
                    }]
                },
                options: {
                  title: {
                    display: true,Number: 20,
                    text: ''
                }
                }
                });
                  <?php $tiketSMSTidakCC = $tiketSMS - $tiketCCSMS; ?>
                  new Chart(document.getElementById("pie-chart2"), {
                    type: 'pie',
                    data: {
                      labels: ["Tiket Dijawab", "Tiket Diterima","Tiket Tidak Dijawab"],
                      datasets: [{
                        label: "Population (millions)",
                        backgroundColor: [ "#2ecc71","#3e95cd","#e74c3c"],
                        data: [<?php echo $tiketCCSMS; ?>,<?php echo $tiketSMS; ?>,<?php echo $tiketSMSTidakCC; ?>]
                    }]
                },
                options: {
                  title: {
                      display: true,Number: 20,
                      text: ''
                  }
                  
                }
                });

                </script>




            