<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Wewaits OB">

    <link rel='shortcut icon' type='image/x-icon' href='../favicon.ico' />

    <title>Masuk Log - <?php echo $_SESSION['site_title'].' - '.$_SESSION['site_quotes']; ?></title>

    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-responsive.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery-2.1.3.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/metro.js"></script>
 
    <style>
        .login-form {
            width: 25rem;
            height: auto;
            margin:30px auto 0;
            opacity: 0;
            color: #777;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
    </style>

    <script>

        $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>
</head>
<body style="background:#f1f1f1;">
    <div class="login-form">
         <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/logo-malangkota.png" style="height: 120px; display: block; margin: 0 auto 10px;">
        <?php echo form_open("superadmin/superadmin/login", 'class="padding20 block-shadow bg-white"'); ?>
            <h1 class="text-light">Login Sambat Online</h1>
            <hr class="thin"/>
            <?php if($this->session->flashdata("result")){ ?>
          <div class="align-center padding10 text-secondary fg-red">
            <span class="mif-warning mif-ani-flash mif-ani-fast"></span> <?php echo $this->session->flashdata("result"); ?>
          </div>
         <?php } ?>
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="user_login">Nama Pengguna</label>
                <input type="text" name="username" placeholder="Isikan Username Anda" required>
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="user_password">Password</label>
                <input type="password" id="controls" name="password" placeholder="Isikan Password" required>
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <?php echo $captcha; ?>
            <div class="input-control text full-size" data-role="input">
                <input type="text" name="captcha" placeholder="Isikan Captcha">
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary">Masuk</button>
                <a href="<?php echo base_url(); ?>" class="button link"><span class="mif-keyboard-return"></span> Kembali ke home</a>
            </div>
        <?php echo form_close(); ?>
    </div>
    <div class="align-center padding20 text-secondary fg-grayLight">
            &copy; 2016 Bidang Informasi Publik Diskominfo Kota Malang
        </div>
</body>
</html>