<div class="page-content">
    <div class="flex-grid no-responsive-future" style="height: 100%;">
        <div class="row" style="height: 100%">
            <!-- Sidebar -->
            <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                <ul class="sidebar">
                    <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                        <span class="mif-list-numbered icon"></span>
                        <span class="title">Jenis</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                        <span class="mif-flow-tree icon"></span>
                        <span class="title">Kategori</span>
                        <span class="counter">tiket sambatan</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                        <span class="mif-clipboard icon"></span>
                        <span class="title">Arsip</span>
                        <span class="counter">Tiket SMS</span>
                    </a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                        <span class="mif-home icon"></span>
                        <span class="title">Dashboard</span>
                        <span class="counter">admin</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                        <span class="mif-mail icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                        <span class="mif-mobile icon"></span>
                        <span class="title">Tiket SMS</span>
                        <span class="counter">baru</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                        <span class="mif-mail-read icon"></span>
                        <span class="title">Tiket</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                        <span class="mif-contacts-mail icon"></span>
                        <span class="title">SMS</span>
                        <span class="counter">terjawab</span>
                    </a></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Sidebar -->
            <!-- Konten kanan -->
            <div class="cell auto-size padding20 bg-white" id="cell-content">
                <ul class="breadcrumbs2">
                    <?php echo $this->breadcrumb->output(); ?>
                </ul>
                <h1 class="text-light">Pesan</h1>
                <?php if($this->session->flashdata("result")){ ?>
                <div class="notice marker-on-bottom fg-blue">
                  <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
              </div>
              <?php } ?>
              <hr class="thin bg-grayLighter">
              <?php if($this->session->userdata('id_skpd') != 0){ ?>
              <div>
                  <a href="<?php echo base_url(); ?>superadmin/pesan/sendPesan" class="button loading-pulse lighten primary">Kirim Pesan </a>
              </div>
              <?php } ?>

              <div class="table-responsive">          
                  <table class="table">
                    <thead>
                      <tr>
                          <th>No</th>
                          <th>Pesan</th>
                          <?php if($this->session->userdata('id_skpd') != 0){ ?>
                          <th>Dari</th>
                          <?php } ?>
                          <th>Hapus</th>
                      </tr>
                  </thead>
                  <tbody>

                    <?php
                    $i=1; 
                    foreach ($pesan as $item) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <?php if($this->session->userdata('id_skpd') == 0){ ?>
 
                                <?php if($item->tipe == 3){ ?>
                                <td><?php echo $item->pesan; ?><br> Pesan dari OPD : <?php echo $item->nama; ?></a></td>
                                <?php }else{ ?>
                                <td><?php echo $item->pesan; ?></td>
                                <?php } ?>
                            <?php }else{ 
                                if($item->tipe == 1){
                                ?>

                            <td><a href="<?php echo base_url();?>superadmin/tiket_baru/jawab/<?php echo $item->id_tiket;?>";"><?php echo $item->pesan; ?></a></td>
                                <?php }
                                    else{?>
                             <td><a href="<?php echo base_url();?>superadmin/tiket_baru_sms/jawab/<?php echo $item->id_tiket;?>";"><?php echo $item->pesan; ?></a></td> 
                                <?php } ?>

                            <?php } ?>
                            <?php if($this->session->userdata('id_skpd') != 0){ ?>
                            <td><?php echo $item->nama; ?></td>
                            <?php } ?>
                            
                            <td><a href='<?php echo base_url(); ?>superadmin/pesan/delete/<?php echo $item->id_notifikasi; ?>' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
                        </tr>

                        <?php $i++;}  ?>
                    </tbody>
                </table>
            </div>
