
<center><h3 id="top">Pengaduan Tiket OPD <?php echo $skpd['nama']; ?></h3></center>
<canvas id="bar-chart" width="800" height="250"></canvas>

<center><h3 id="top">Pengaduan WEB Diterima vs Dijawab</h3></center>
<canvas id="pie-chart" width="1200" height="350"></canvas>
<br>
<center><h3 id="top">Pengaduan SMS Diterima vs Dijawab </h3></center>
<canvas id="pie-chart2" width="1200" height="350"></canvas>

<script type="text/javascript">
  // Bar chart
  new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: ["Web Tiket", "Web SMS"],
      datasets: [
        {
          label: ["Web Tiket"],
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [<?php echo $tiket; ?>,<?php echo $tiketSMS; ?>]
        }
      ]
    },
    options: {
        legend: { display: false },
                  title: {
                    display: false,
                    text: ''
                
                },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

  <?php $tiketTidakCC = $tiket - $tiketCC; ?>
  new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: ["Tiket Dijawab", "Tiket Diterima","Tiket Tidak Dijawab"],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#2ecc71","#3e95cd","#e74c3c"],
        data: [<?php echo $tiketCC; ?>,<?php echo $tiket; ?>,<?php echo $tiketTidakCC; ?>]
    }]
},
options: {
  title: {
    display: true,Number: 20,
    text: ''
}
}
});
  <?php $tiketSMSTidakCC = $tiketSMS - $tiketCCSMS; ?>
  new Chart(document.getElementById("pie-chart2"), {
    type: 'pie',
    data: {
      labels: ["Tiket Dijawab", "Tiket Diterima","Tiket Tidak Dijawab"],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: [ "#2ecc71","#3e95cd","#e74c3c"],
        data: [<?php echo $tiketCCSMS; ?>,<?php echo $tiketSMS; ?>,<?php echo $tiketSMSTidakCC; ?>]
    }]
},
options: {
  title: {
      display: true,Number: 20,
      text: ''
  }
  
}
});

</script>

