<?php foreach ($bar as $q){
    $month = $q->bulan;
    $tahun = $q->tahun;

    switch ($month) {
        case "1":
        $bulan = "Januari";
        break;

        case "2":
        $bulan = "Februari";
        break;

        case "3":
        $bulan = "Maret";
        break;

        case "4":
        $bulan = "April";
        break;

        case "5":
        $bulan = "Mei";
        break;

        case "6":
        $bulan = "Juni";
        break;


        case "7":
        $bulan = "Juli";
        break;


        case "8":
        $bulan = "Agustus";
        break;

        case "9":
        $bulan = "September";
        break;

        case "10":
        $bulan = "Oktober";
        break;
        case "11":
        break;

        default:
        $bulan = "Desember";
    }
} 
if(!empty($bulan)){
    ?>
    <center><h3 id="top1">Pengaduan WEB Tiket <?php echo $bulan." ".$tahun; ?></h3></center>
    <canvas id="bar-chart" width="800" height="250"></canvas>

    <?php }else{
        echo '<center><h3 id="top1">Tidak Ada Pengaduan</h3><center>';
    } ?>
    <script type="text/javascript">
  // Bar chart
  new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
        labels: [
        <?php foreach ($bar as $item) { 
            ?>
            "<?php 
            echo $item->kategori; ?> ",
            <?php } ?>
            ],   
            datasets: [
            {
                label: "Tiket",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#3e95cd","#8e5ea2"],
                data: [
                <?php foreach ($bar as $item) { ?>
                    <?php echo $item->jumlah; ?>,
                    <?php } ?>
                    ]
                }
                ]
            },
          options: {
            legend: { display: false },
                  title: {
                    display: false,
                    text: ''
                
                },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

</script>

