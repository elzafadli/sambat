
<script>
    
    $(document).ready(function(){
        $("#export").click(function(){
            var value = $('.dataTables_filter input').val();
            $("#search").val(value);
            $("#Etgl1").val(<?php echo $tanggal1; ?>);
            $("#Etgl2").val(<?php echo $tanggal2; ?>);
            $("#Ekategori").val(<?php echo $kategori; ?>);
            $("#Eskpd").val(<?php echo $skpd; ?>);
        });
    });

   

    function getchild(id)
    {
        //alert('this id value :'+id);
        $.ajax({
            type: "POST",
            url: '<?php echo site_url('superadmin/user/get_skpd').'/';?>'+id,
            data: id='parent_id',
            success: function(data){
            //alert(data);
            $('#old_state').html(data);
        },
    });
    }

</script>
<div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">
            <div class="row" style="height: 100%">
                <!-- Sidebar -->
                <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                    <ul class="sidebar">
                        <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                            <span class="mif-list-numbered icon"></span>
                            <span class="title">Jenis</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                            <span class="mif-flow-tree icon"></span>
                            <span class="title">Kategori</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                            <span class="mif-mobile icon"></span>
                            <span class="title">Tiket SMS</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                            <span class="mif-contacts-mail icon"></span>
                            <span class="title">SMS</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>superadmin/arsip">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket SMS</span>
                        </a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /Sidebar -->
                <!-- Konten kanan -->
                <div class="cell auto-size padding20 bg-white" id="cell-content">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    <h1 class="text-light">Arsip Tiket</h1>
                    <hr class="thin bg-grayLighter">
                    <?php echo form_open("superadmin/arsip/cari"); ?>
                    <div class="example" data-text="Arsip Tiket">    
                        <div class="row flex-just-sb">
                            <div class="cell colspan5">
                                <label>Dari Tanggal</label>
                                <div class="input-control text full-size" data-role="datepicker" data-format="dd/mm/yyyy 00:00:00">
                                    <input type="text" name="tanggal1">
                                </div>
                            </div>
                            <div class="cell colspan5">
                                <label>Sampai Tanggal</label>
                                <div class="input-control text full-size" data-role="datepicker" data-format="dd/mm/yyyy 00:00:00">
                                    <input type="text" name="tanggal2">
                                    <input type="hidden" id="test3" name="search"value=""></p>
                                </div>
                            </div>
                        </div>
                        <div class="row flex-just-sb">
                            <div class="cell colspan5">
                                    <dl>
                                        <dt>Pilih OPD : (Optional)</dt>
                                        <div class="input-control select full-size">
                                            <select name="id_skpd0" id="country_details" onChange="getchild(this.value)">
                                                <option value="" selected="selected" >Pilih OPD</option>
                                                <?php foreach($parent as $k): ?>
                                                    <option value="<?php echo $k->id_skpd; ?>"><?php echo $k->nama_skpd; ?></option>
                                                <?php endforeach; ?> 
                                            </select>
                                        </div>
                                    </dl>
                                </div>
                                <div class="cell colspan5">
                                    <dl>
                                        <dt>Pilih Sub OPD :</dt>
                                        <div class="input-control select full-size">
                                            <select name="id_skpd" id="old_state">
                                                <option selected="selected" value="0">Pilih Sub OPD</option> 
                                            </select>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan5">
                                    <label>Kategori : (Optional)</label>
                                    <div class="input-control text full-size">
                                        <select name="kategori">
                                        <option selected="selected" value=""></option>
                                        <?php foreach ($kategorils as $item) { ?>
                                                <option value="<?php echo $item->id_kategori; ?>"><?php echo $item->kategori; ?></option>
                                        <?php } ?>
                                            </select>
                                    </div>
                                </div>
                            </div>
                        <br>
                        <button class="button loading-pulse lighten primary">Cari Data</button>
                        <div id="grafik1"></div>
                    </div>
                    <?php echo form_close(); ?>
                    <?php //echo $tanggal1; ?>
                    <?php echo $data_retrieve; ?>

                    <?php echo form_open("superadmin/arsip/xls"); ?>
                    <p class='v-align-top'>

                        <input type="hidden" id="search" name="search"value=""></p>
                        <input id="Etgl1" type="hidden" name="Etanggal1">
                        <input id="Etgl2" type="hidden" name="Etanggal2">
                        <input id="Eskpd" type="hidden" name="skpd">
                        <input id="Ekategori" type="hidden" name="kategori">

                        <span data-role='hint' data-hint-background='bg-green' data-hint-color='fg-white' data-hint-mode='2' data-hint-position='center'>  
                            <span class='button bg-darkEmerald fg-white'>
                            <span class='mif-file-excel'>
                                
                                <button id="export" style="background-color: #003e00;border: none; ">Export excel</button>
                            </span>
                         </span>
                    </p>
                    <?php echo form_close(); ?>
                    
	