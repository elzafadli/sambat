<div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">
            <div class="row" style="height: 100%">
                <!-- Sidebar -->
                <div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1; height: 100%">
                    <ul class="sidebar">
                        <?php if ($this->session->userdata("id_skpd")=='0') { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/jenis">
                            <span class="mif-list-numbered icon"></span>
                            <span class="title">Jenis</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/kategori">
                            <span class="mif-flow-tree icon"></span>
                            <span class="title">Kategori</span>
                            <span class="counter">tiket sambatan</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru_sms">
                            <span class="mif-mobile icon"></span>
                            <span class="title">Tiket SMS</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab_sms">
                            <span class="mif-contacts-mail icon"></span>
                            <span class="title">SMS</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsipsms">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket SMS</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/arsip">
                            <span class="mif-clipboard icon"></span>
                            <span class="title">Arsip</span>
                            <span class="counter">Tiket</span>
                        </a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo base_url(); ?>superadmin/dashboard">
                            <span class="mif-home icon"></span>
                            <span class="title">Dashboard</span>
                            <span class="counter">admin</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_baru">
                            <span class="mif-mail icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">baru</span>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>superadmin/tiket_terjawab">
                            <span class="mif-mail-read icon"></span>
                            <span class="title">Tiket</span>
                            <span class="counter">terjawab</span>
                        </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /Sidebar -->
                <!-- Konten kanan -->
                <div class="cell auto-size padding20 bg-white" id="cell-content">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    <h1 class="text-light">Profile <small><?php echo $this->session->userdata("nama"); ?></small></h1>
                    <hr class="thin bg-grayLighter">
                    <?php if($this->session->flashdata("simpan_akun")){ ?>
                    <div class="notify success">
                        <span class="notify-title">Informasi</span>
                        <span class="notify-text">
                        <?php echo $this->session->flashdata("simpan_akun"); ?>
            			<?php echo validation_errors(); ?>
            			</span>
                    </div>
                    <?php } ?>
                    <?php echo form_open("superadmin/password/simpan"); ?>
                    <div class="example" data-text="Password Anda">
                    	<div class="row flex-just-sb">
                    		<div class="cell colspan5">
                    			<label>Username <small>(tidak bisa diganti)</small></label>
                    			<div class="input-control text full-size">
                    				<input type="text" id="username" name="username" value="<?php echo $username; ?>" placeholder="input text" disabled/>
                    			</div>
                    		</div>
                    		<div class="cell colspan5">
                    			<label>Password lama</label>
                    			<div class="input-control password full-size">
                    				<input type="password" name="password_lama" id="password_lama" placeholder="Password Lama"/>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="row flex-just-sb">
                    		<div class="cell colspan5">
                    			<label>Password baru</label>
                    			<div class="input-control password full-size">
                    				<input type="password" name="password_baru" id="password_baru" placeholder="Password Baru"/>
                    			</div>
                    		</div>
                    		<div class="cell colspan5">
                    			<label>Ulangi Password baru</small></label>
                    			<div class="input-control password full-size">
                    				<input type="password" name="ulangi_password" id="ulangi_password" placeholder="Ulangi Password Baru"/>
                    			</div>
                    		</div>
                    	</div>
                    	<input type="hidden" name="id_param" value="<?php echo $id_param; ?>" />
						<input type="hidden" name="username_temp" value="<?php echo $username; ?>" />
						<button class="button loading-pulse lighten primary">Simpan Data</button>
                    </div>
                    <?php echo form_close(); ?>