<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class surat_keterangan extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Surat Keterangan", '/');
			
			$id_lokasi = $this->session->userdata("id_lokasi");
			$filter = $this->session->userdata("filter_iklan");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_indexs_surat_keterangan(4,$uri,$filter,$id_lokasi);
			
			$this->load->view('bg_header',$d);
			$this->load->view('surat_keterangan/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

	function hapus($id_param)
	{
		if($this->session->userdata('logged_in_admin')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$get = $this->db->get_where("kel_surat_keterangan",$where);
			foreach($get->result() as $del)
			{
				$url = "./file/".$del->file."";
				unlink($url);
			}
			$this->db->delete("kel_surat_keterangan",$where);
			redirect("superadmin/surat_keterangan");
		}
	}

	function approve($id_param,$upd)
	{
		if($this->session->userdata('logged_in_admin')!="")
		{
			$where['id_surat_ket'] = $id_param;
			$updt['st'] = $upd;
			$this->db->update("kel_surat_keterangan",$updt,$where);
			redirect("superadmin/surat_keterangan");
		}
	}

	function filter()
	{
		if($this->session->userdata('logged_in_admin')!="")
		{
			$set['filter_iklan'] = $_POST['filter'];
			$this->session->set_userdata($set);
			redirect("superadmin/surat_keterangan");
		}
	}

	function komentar($id_param)
	{
		if($this->session->userdata('logged_in_admin')!="")
		{
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Surat Keterangan", base_url().'superadmin/surat_keterangan');
			$this->breadcrumb->append_crumb("Komentar", '/');

			$where['id_surat_ket'] = $id_param;
			$q = $this->db->get_where("kel_surat_keterangan",$where)->row();
			$d['komentar'] = $q->komentar;
			$d['id_surat_ket'] = $q->id_surat_ket;
			
			$this->load->view('bg_header',$d);
			$this->load->view('surat_keterangan/komentar');
			$this->load->view('bg_footer');
			// redirect("superadmin/surat_keterangan");
		}
	}

	function simpan()
   	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id['id_surat_ket'] = $this->input->post("id_surat_ket");
			$in['komentar'] = $this->input->post("komentar");

			$this->db->update("kel_surat_keterangan",$in,$id);
			redirect("superadmin/surat_keterangan");
		}
		else
		{
			redirect("superadmin");
		}
	}
}
 
/* End of file superadmin.php */
