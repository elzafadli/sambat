<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class arsip extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{	

			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("ARSIP TIKET", '/');
			$d['title'] = "Arsip Tiket";

			$d['parent'] = $this->app_global_web_model->generate_skpd();
			$d['kategori'] = $this->app_global_superadmin_model->getKategori()->result();
			
			$this->load->view('bg_header',$d);
			$this->load->view('arsip/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function cari($uri=0)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$d['kategorils'] = $this->app_global_superadmin_model->getKategori()->result();

			$data = $this->input->post("search");

			if($this->input->post("id_skpd") == 0){
				$skpd = $this->input->post("id_skpd0");
			}else{
				$skpd = $this->input->post("id_skpd");	
			}

			$kategori = $this->input->post("kategori");
			

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("CARI ARSIP TIKET", '/');

			if($this->input->post("tanggal1") =="" or $this->input->post("tanggal2") == ""){
				redirect('superadmin/arsip/','refresh');
			}

			$input1 = $this->input->post("tanggal1");		

			$tanggal1asli = DateTime::createFromFormat('d/m/Y H:i:s', $input1);
			$tanggal1 = $tanggal1asli->getTimestamp();

			$input2 = $this->input->post("tanggal2");
			$tanggal2asli = DateTime::createFromFormat('d/m/Y H:i:s', $input2);
			$tanggal2 = $tanggal2asli->getTimestamp();

			$d['title'] = "Cari Arsip Tiket " .generate_tanggal_query($tanggal1)." - ".generate_tanggal_query($tanggal2)."";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_arsip($tanggal1,$tanggal2,$this->config->item("limit_item"),$uri,$data,$skpd,$kategori);

			$d['tanggal1'] =$tanggal1;
			$d['tanggal2'] =$tanggal2;
			$d['skpd'] =$skpd;
			$d['kategori'] =$kategori;

			$d['parent'] = $this->app_global_web_model->generate_skpd();


			$this->load->view('bg_header',$d);
			$this->load->view('arsip/bg_cari');

			// //echo $tanggal1; 
			$this->load->view('bg_footer');
			
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function xls()
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tanggal1 = $this->input->post("Etanggal1");
			$tanggal2 = $this->input->post("Etanggal2");
			$s = $this->input->post("search");

 			$skpd = $this->input->post("skpd");
 			$kategori = $this->input->post("kategori");

 			//echo $skpd.$kategori;
			
			$d['title'] = "Arsip Tiket " .generate_tanggal_query($tanggal1)." - ".generate_tanggal_query($tanggal2).".xls ";

			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_arsip_xls($tanggal1,$tanggal2,$s,$skpd,$kategori);
			
			$this->load->view('arsip/bg_xls',$d);
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
