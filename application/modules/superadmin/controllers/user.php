<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	 
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", '/');
			
			$d['title'] = "User Management";
			$filter['nama_user'] = $this->session->userdata("search_nama_user");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_user($this->config->item("limit_item"),$uri,$filter);
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function masyarakat($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", '/');
			
			$d['title'] = "User Management";
			$filter['nama_user'] = $this->session->userdata("search_nama_user");
			$d['list'] = $this->app_global_superadmin_model->generate_index_user_masyarakat()->result();

			$this->load->view('user/bg_header',$d);
			$this->load->view('user/bg_user',$d);
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function admin($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", '/');
			
			$d['title'] = "User Management";
			$d['list'] = $this->app_global_superadmin_model->getSuperAdmin()->result();
			//$filter['nama_user'] = $this->session->userdata("search_nama_user");
			//$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_user_masyarakat($this->config->item("limit_item"),$uri,$filter);
			//print_r($d['list']);
		
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_admin');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			$d['title'] = "Tambah User";
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", base_url().'superadmin/user');
			$this->breadcrumb->append_crumb("TAMBAH", '/');
			$d['parent'] = $this->app_global_web_model->generate_skpd();
			$d['group'] = $this->app_global_web_model->generate_group();
			$d['nama_user'] = "";
			$d['username'] = "";
			$d['email'] = "";
			$d['no_hp'] = "";
			$d['GroupID'] = "";
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function addAdmin()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{


			
			$d['title'] = "Tambah User";
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", base_url().'superadmin/user/admin');
			$this->breadcrumb->append_crumb("TAMBAH", '/');
		
			$d['group'] = $this->app_global_superadmin_model->getSMSGroup()->result();
			
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_addAdmin');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function addAdminData()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$in['nama'] = $this->input->post("nama_user");
			$in['username'] = $this->input->post("username");
			$in['password'] = md5($this->input->post("password").$this->config->item("key_login"));
			$in['id_skpd'] = 0;
			$in['email'] = $this->input->post("email");
			$in['no_hp'] = $this->input->post("no_hp");
			$in['GroupID'] = $this->input->post("GroupID");

			$this->db->insert("sam_admin",$in);
			
			$this->session->set_flashdata('result', 'Akun berhasil di Tambahkan !!!');
			redirect("superadmin/user/addAdmin");
		}
		else
		{
			redirect("superadmin");
		}
   }

 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{

			

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", base_url().'superadmin/user');
			$this->breadcrumb->append_crumb("EDIT USER", '/');
			$d['title'] = "Edit User";

			$where['id_admin'] = $id_param;
			$get = $this->db->get_where("sam_admin",$where)->row();
			
			$d['nama_user'] = $get->nama;
			$d['username'] = $get->username;
			$d['email'] = $get->email;
			$d['no_hp'] = $get->no_hp;
			$d['GroupID'] = $get->GroupID;
			$id_skpd = $get->id_skpd;
			$d['group'] = $this->app_global_web_model->generate_group();
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_skpd_select($id_skpd);
			$d['id_param'] = $get->id_admin;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['id_admin'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$cek = $this->db->get_where("sam_admin",array("username"=>$this->input->post("username")))->num_rows();
				if($cek>0)
				{
					$this->session->set_flashdata("simpan_akun","Username telah terpakai, gunakan username lainnya");
					redirect("superadmin/user/tambah");
				}
				else
				{
					$in['nama'] = $this->input->post("nama_user");
					$in['username'] = $this->input->post("username");
					$in['password'] = md5($this->input->post("password").$this->config->item("key_login"));
					if ($this->input->post("id_skpd")==0){
						$in['id_skpd'] = $this->input->post("id_skpd0");
					} else {
						$in['id_skpd'] = $this->input->post("id_skpd");
					}
					$in['email'] = $this->input->post("email");
					$in['no_hp'] = $this->input->post("no_hp");
					$in['GroupID'] = $this->input->post("GroupID");
					$this->db->insert("sam_admin",$in);
					redirect("superadmin/user");
				}
			}
			else if($tipe=="edit")
			{	
				if($_POST['password']!="")
				{
					$cek = $this->db->get_where("sam_admin",array("username"=>$this->input->post("username")))->num_rows();
					if($cek>0 && $this->input->post("username_temp")!=$this->input->post("username"))
					{
						$this->session->set_flashdata("simpan_akun","Username telah terpakai, gunakan username lainnya atau tetap gunakan username ini");
						redirect("superadmin/user/edit/".$id['id_admin']."");
					}
					else
					{
						$in['nama'] = $this->input->post("nama_user");
						$in['username'] = $this->input->post("username");
						$in['email'] = $this->input->post("email");
						$in['no_hp'] = $this->input->post("no_hp");
						$in['GroupID'] = $this->input->post("GroupID");
						$in['password'] = md5($this->input->post("password").$this->config->item("key_login"));
						$in['id_skpd'] = $this->input->post("id_skpd");
						$this->db->update("sam_admin",$in,$id);
						redirect("superadmin/user");
					}
				}
				else
				{
					$cek = $this->db->get_where("sam_admin",array("username"=>$this->input->post("username")))->num_rows();
					if($cek>0 && $this->input->post("username_temp")!=$this->input->post("username"))
					{
						$this->session->set_flashdata("simpan_akun","Username telah terpakai, gunakan username lainnya atau tetap gunakan username ini");
						redirect("superadmin/user/edit/".$id['id_admin']."");
					}
					else
					{
						$in['nama'] = $this->input->post("nama_user");
						$in['username'] = $this->input->post("username");
						$in['email'] = $this->input->post("email");
						$in['no_hp'] = $this->input->post("no_hp");
						$in['GroupID'] = $this->input->post("GroupID");
						$in['id_skpd'] = $this->input->post("id_skpd");
						$this->db->update("sam_admin",$in,$id);
						redirect("superadmin/user");
					}
				}
			}
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function set()
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$sess['search_nama_user'] = $this->input->post("by_nama");
			$this->session->set_userdata($sess['search_nama_user']);
			redirect("superadmin/user");
		}
		else
		{
			redirect("web");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_admin'] = $id_param;
			$this->db->delete("sam_admin",$where);
			redirect("superadmin/user");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function hapusUser($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_user'] = $id_param;
			$this->db->delete("sam_user",$where);
			redirect("superadmin/user/masyarakat");
		}
		else
		{
			redirect("superadmin");
		}
   }
   public function hapusAdmin($id)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$data = array(
				'view' => '0',
				'password' => 'kosong'
				);

			//$where['id_user'] = $id_param;
			//$this->db->delete("sam_user",$where);
			$this->db->where('id_admin', $id);
			$this->db->update('sam_admin', $data);

			redirect("superadmin/user/admin");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function editMasyarakat($id)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", base_url().'superadmin/user/masyarakat');
			$this->breadcrumb->append_crumb("EDIT USER", '/');
			$d['title'] = "Edit User";

			$d['data'] = $this->app_global_superadmin_model->getIDMasyarakat($id)->row_array();

			$d['group'] = $this->app_global_superadmin_model->getSMSGroup()->result();

			/*$where['id_admin'] = $id_param;
			$get = $this->db->get_where("sam_admin",$where)->row();
			
			$d['nama_user'] = $get->nama;
			$d['username'] = $get->username;
			$d['email'] = $get->email;
			$d['no_hp'] = $get->no_hp;
			$d['GroupID'] = $get->GroupID;
			$id_skpd = $get->id_skpd;
			$d['group'] = $this->app_global_web_model->generate_group();
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_skpd_select($id_skpd);
			$d['id_param'] = $get->id_admin;
			$d['tipe'] = "edit";*/
			//print_r($d['data']);
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_editUser',$d);
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

    public function editAdmin($id)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
	
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("USER MANAGEMENT", base_url().'superadmin/user/admin');
			$this->breadcrumb->append_crumb("EDIT USER", '/');
			$d['title'] = "Edit User";

			$d['data'] = $this->app_global_superadmin_model->getAdmin($id)->row_array();
			$d['group'] = $this->app_global_superadmin_model->getSMSGroup()->result();

			/*$where['id_admin'] = $id_param;
			$get = $this->db->get_where("sam_admin",$where)->row();
			
			$d['nama_user'] = $get->nama;
			$d['username'] = $get->username;
			$d['email'] = $get->email;
			$d['no_hp'] = $get->no_hp;
			$d['GroupID'] = $get->GroupID;
			$id_skpd = $get->id_skpd;
			$d['group'] = $this->app_global_web_model->generate_group();
			$d['generate_child_lokasi'] = $this->app_global_web_model->generate_child_skpd_select($id_skpd);
			$d['id_param'] = $get->id_admin;
			$d['tipe'] = "edit";*/
			//print_r($d['data']);
			$this->load->view('bg_header',$d);
			$this->load->view('user/bg_editAdmin',$d);
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

    public function setUser()
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			//$this->load->library('session');
			$id = $this->input->post("id");
			//echo "58c690ff66f1a7861af67795961fe070";

			if($this->input->post("password") == ""){
				$password = $this->input->post("passwordTemp");
			}else{
				$pass = $this->input->post("password");
				$password = $this->encrypt->encode($pass);
				//$password = md5($this->input->post("password").$this->config->item("key_login"));
			}
			$no_hp = $this->input->post("no_hp");
			$nama = $this->input->post("nama_user");
			$status = $this->input->post("status");
			//echo $temp1;

			$data = array(
				'nama' => $nama,
				'password' => $password,
				'no_telpon' => $no_hp,
				'stts' => $status,
				);

			//print_r($data);
			//$where['id_user'] = $id_param;
			//$this->db->delete("sam_user",$where);
			$this->db->where('id_user', $id);
			$this->db->update('sam_user', $data);

			//$this->session->set_flashdata('result', 'Akun sudah berhasil diperbarui!!!');
			$this->session->set_flashdata('Update', 'Akun berhasil di Update!!!');
			//echo $this->session->set_flashdata('result');

			redirect("superadmin/user/editMasyarakat/".$id);
		}
		else
		{
			redirect("superadmin");
		}
   }


   public function setAdmin()
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->load->library('session');
			$id = $this->input->post("id");
			//echo "58c690ff66f1a7861af67795961fe070";

			if($this->input->post("password") == ""){
				$password = $this->input->post("passwordTemp");
			}else{
				$password = md5($this->input->post("password").$this->config->item("key_login"));
			}
			$no_hp = $this->input->post("no_hp");
			$nama = $this->input->post("nama_user");
			$group = $this->input->post("GroupID");
			//echo $temp1;

			$data = array(
				'nama' => $nama,
				'password' => $password,
				'no_hp' => $no_hp,
				'GroupID' => $group,
				);

			//print_r($data);
			//$where['id_user'] = $id_param;
			//$this->db->delete("sam_user",$where);
			$this->db->where('id_admin', $id);
			$this->db->update('sam_admin', $data);

			//$this->session->set_flashdata('result', 'Akun sudah berhasil diperbarui!!!');
			$this->session->set_flashdata('Update', 'Akun berhasil di Update!!!');
			//echo $this->session->set_flashdata('result');

			redirect("superadmin/user/editAdmin/".$id);
		}
		else
		{
			redirect("superadmin");
		}
   }


   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
