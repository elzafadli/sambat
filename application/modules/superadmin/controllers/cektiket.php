<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cektiket extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("KATEGORI TIKET SAMBATAN", '/');
			
			$d['title'] = "Kategori Tiket Sambatan";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_kategori($this->config->item("limit_item"),$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('kategori/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("KATEGORI TIKET SAMBATAN", base_url().'superadmin/kategori');
			$this->breadcrumb->append_crumb("TAMBAH KATEGORI", '/');

			$d['kategori'] = "";
			
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			$d['title'] = "Tambah Kategori Tiket";
			$this->load->view('bg_header',$d);
			$this->load->view('kategori/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("KATEGORI TIKET SAMBATAN", base_url().'superadmin/kategori');
			$this->breadcrumb->append_crumb("EDIT KATEGORI", '/');
			$d['title'] = "Edit Kategori Tiket";
			$where['id_kategori'] = $id_param;
			$get = $this->db->get_where("sam_kategori",$where)->row();
			$d['kategori'] = $get->kategori;
			$d['id_param'] = $get->id_kategori;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('kategori/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['id_kategori'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$in['kategori'] = $this->input->post("kategori");
				$this->db->insert("sam_kategori",$in);
			}
			else if($tipe=="edit")
			{
				$in['kategori'] = $this->input->post("kategori");
				$in['id_kategori'] = $this->input->post("id_kategori");
				$this->db->update("sam_kategori",$in,$id);
			}
			
			redirect("superadmin/kategori");
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_kategori'] = $id_param;
			$this->db->delete("sam_kategori",$where);
			redirect("superadmin/kategori");
		}
		else
		{
			redirect("superadmin");
		}
   }
}
 
/* End of file superadmin.php */
