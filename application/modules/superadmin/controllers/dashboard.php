<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	 
	function __construct()
	{
          // this is your constructor
		parent::__construct();
		$this->load->model('app_global_superadmin_model');

		$jumlah = $this->app_global_superadmin_model->countNotifAdmin($this->session->userdata('id_admin'))->num_rows();

		$this->session->set_userdata('notifAdmin', $jumlah);

		if($this->session->userdata("logged_in_admin") == ""){
			redirect("superadmin");
		}
	}

	function test(){
		echo "Hello World<br>";
		$d['check'] = $this->app_global_superadmin_model->checkNotif("c.id_skpd")->result();
		foreach ($d['check'] as $item) {
			$tanggal = $item->tanggal;
		}

		$now = date('m/d/Y h:i:sa');

		$test = new DateTime($tanggal);
		$test1 = new DateTime($now); 
 
		$diff = $test->diff($test1);

		echo $diff->days;

	}

	function jsonGrafik(){

		$bulan = $this->input->post("bulan");
		$tahun = $this->input->post("tahun");

		if ($bulan== "-" or $tahun == "-") {
			echo "<font color='red'>Note : Masukkan Tanggal Terlebih Dahulu !!!</font>";
		}else{
			
		
			$data['bar'] = $this->app_global_superadmin_model->getDetailKategori($bulan,$tahun)->result();
			//echo $tanggal2;
			//$data['bar'] = $this->app_global_superadmin_model->countTikets($tanggal1,$tanggal2);
			//echo $tgl1;echo $tgl2;

			//$data['bar2'] = $this->app_global_superadmin_model->countTiketsSMS($tgl1,$tgl2);
			//print_r($data['bar']);
			$this->load->view('grafik/pengaduan',$data);
			//echo "[{x: 1, y: 10}, {x: 2, y: 10}, {x: 3, y: 10}, {x: 4, y: 10}, {x: 5, y: 10}]";
		}
	}


	function skpdGrafik(){
		//$input1 = $this->input->post("tanggal1");
		//$input1 = $this->input->post("tanggal2");
	
		if ($this->input->post("tanggal2")==0){
			//$in['id_skpd'] = $this->input->post("id_skpd0");
			$id_skpd = $this->input->post("tanggal1");	
		} else {
			//$in['id_skpd'] = $this->input->post("id_skpd");
			$id_skpd = $this->input->post("tanggal2");
		}

		$data['skpd'] = $this->app_global_superadmin_model->countSkpds($id_skpd)->row_array();
		$data['skpd2'] = $this->app_global_superadmin_model->countSkpdsSMS($id_skpd)->row_array();

		$data['tiket'] = $this->app_global_superadmin_model->countAllTiket($id_skpd)->num_rows();

		$data['tiketCC'] = $this->app_global_superadmin_model->countAllTiketCC($id_skpd)->num_rows();

		$data['tiketSMS'] = $this->app_global_superadmin_model->countAllTiketSMS($id_skpd)->num_rows();
		$data['tiketCCSMS'] = $this->app_global_superadmin_model->countAllTiketSMSCC($id_skpd)->num_rows();



		//echo $data['tiket'];echo $data['tiketCC'];
		//print_r($data['skpd']);
		$this->load->view('grafik/skpd',$data);
		//echo "[{x: 1, y: 10}, {x: 2, y: 10}, {x: 3, y: 10}, {x: 4, y: 10}, {x: 5, y: 10}]";
	}

	function grafik(){
		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
		$this->breadcrumb->append_crumb('DASHBOARD', '/');
		$d['title'] = "Dashboard Admin";

		//$d['count_tiket'] = $this->app_global_superadmin_model->count_tiket_superadmin();
		
		if ($this->session->userdata("id_skpd")=='0') {
			$d['skpd'] = $this->app_global_superadmin_model->countSkpd();

			$d['skpd2'] = $this->app_global_superadmin_model->countSkpdSMS();
			$d['parent'] = $this->app_global_web_model->generate_skpd();


			$this->load->view('bg_header',$d);
			$this->load->view('bg_home2',$d);
			$this->load->view('bg_footer');
		}else{
			$id_skpd = $this->session->userdata("id_skpd");

			$data['skpd'] = $this->app_global_superadmin_model->countSkpds($id_skpd)->row_array();
			$data['skpd2'] = $this->app_global_superadmin_model->countSkpdsSMS($id_skpd)->row_array();

			$data['tiket'] = $this->app_global_superadmin_model->countAllTiket($id_skpd)->num_rows();

			$data['tiketCC'] = $this->app_global_superadmin_model->countAllTiketCC($id_skpd)->num_rows();

			$data['tiketSMS'] = $this->app_global_superadmin_model->countAllTiketSMS($id_skpd)->num_rows();
			$data['tiketCCSMS'] = $this->app_global_superadmin_model->countAllTiketSMSCC($id_skpd)->num_rows();

			$this->load->view('bg_header',$d);
			$this->load->view('bg_homeSkpd',$data);
			$this->load->view('bg_footer');
		}

	}


	function sendNotif($number,$isi){

		$data0 = array(
   			'DestinationNumber' => $number,
   			'TextDecoded' => $isi,
   			'CreatorID' => 'Gammu'
  		);
  		
  		if($this->db->insert('outbox', $data0)){
  			return "OK";
  		}else{
  			return "Gagal";
  		}
	}


	function index()
	{
		if($this->session->userdata('logged_in_admin')!="")
		{
			$d['tahun'] =$this->app_global_superadmin_model->getDateKategori()->result();
			
			if($this->session->userdata('id_skpd') == 0){
				$d['check'] = $this->app_global_superadmin_model->checkNotif("c.id_skpd")->result();
				$d['checkSMS'] = $this->app_global_superadmin_model->checkNotifSMS("c.id_skpd")->result();

				
				$temp = "";
				$now = date('m/d/Y h:i:sa');
				foreach ($d['check'] as $item) {
					$id = $item->id_skpd; 
					$idtiket = $item->id_tiket;
					$tanggal = $item->tanggal;

					$test = new DateTime($tanggal);
					$test1 = new DateTime($now); 
			 
					$diff = $test->diff($test1);
					//echo $diff->days;
					if($diff->days >= 4){

						$nama = $this->app_global_superadmin_model->getSkpd($id)->row_array();

						$pesan = "Pesan dengan ID Tiket ".$item->id_tiket." belum di Jawab OPD ".$nama['nama_skpd'];

						$where	= array(
			     				'id_tiket'	=> $item->id_tiket,
			     				'id_admin'	=> $item->id_admin,
			     				'id_skpd' => $id,
			     				'pesan'	=> $pesan,
			     				'tipe' => 1,
			     				'id_sender' => $this->session->userdata("id_admin")
			     			);

						$admin = $this->app_global_superadmin_model->getSuperAdmin("")->result();

						if($idtiket != $temp){
							foreach ($admin as $s) {
								$where2	= array(
				     				'id_tiket'	=> $item->id_tiket,
				     				'id_admin'	=> $s->id_admin,
				     				'id_sender' => $this->session->userdata("id_admin"),
				     				'id_skpd' => $s->id_skpd,
				     				'tipe' => 1,
				     				'pesan'	=> $pesan,
				     			);

								if($this->db->insert('notifikasi_skpd',$where2)){
									//echo "OK";
								}
							}
						}

						if($this->db->insert('notifikasi_skpd',$where)){
							//echo "OK";
						}
						$temp= $idtiket;
						$this->app_global_superadmin_model->updateNewNotif(0,$idtiket);
					}
				}

				//print_r($d['checkSMS']);
				$temp2="";
				foreach ($d['checkSMS'] as $item) {
					$id = $item->id_skpd; 
					$idsms = $item->id_sms;
					$tanggal = $item->tanggal;

					$test = new DateTime($tanggal);
					$test1 = new DateTime($now); 
			 
					$diff = $test->diff($test1);

					if($diff->days >= 4){


						$nama = $this->app_global_superadmin_model->getSkpd($id)->row_array();

						$pesan = "Pesan dengan ID Tiket SMS ".$item->id_sms." belum di Jawab OPD ".$nama['nama_skpd'];

						$where	= array(
			     				'id_tiket'	=> $item->id_sms,
			     				'id_admin'	=> $item->id_admin,
			     				'id_skpd' => $id,
			     				'pesan'	=> $pesan,
			     				'tipe' => 2,
			     				'id_sender' => $this->session->userdata("id_admin")
			     			);

						$admin = $this->app_global_superadmin_model->getSuperAdmin("")->result();

						if($idsms != $temp2){
							foreach ($admin as $s) {
								$where2	= array(
				     				'id_tiket'	=> $item->id_sms,
				     				'id_admin'	=> $s->id_admin,
				     				'id_sender' => $this->session->userdata("id_admin"),
				     				'id_skpd' => $s->id_skpd,
				     				'tipe' => 2,
				     				'pesan'	=> $pesan,
				     			);

								if($this->db->insert('notifikasi_skpd',$where2)){
																	}
							}
						}

						if($this->db->insert('notifikasi_skpd',$where)){
							//echo "OK";
						}
						$temp2= $idsms;
						$this->app_global_superadmin_model->updateNewNotifSMS(0,$idsms);
					}
				}


				$d['pesan'] = $this->app_global_superadmin_model->notifSkpd()->result();
				$d['admin'] = $this->app_global_superadmin_model->getSuperAdmin()->result();
				//print_r($d['admin']);
				//print_r($d['pesan']);
				$nama = $this->app_global_superadmin_model->getAdmin($this->session->userdata('id_admin'))->row_array();

				foreach ($d['pesan'] as $item) {

					if($item->id_skpd == 0){
						$pesan= $item->pesan;
					}else{
						$pesan = $item->pesan." - Pesan dari: ".$nama['nama'];
					}

					$id = $item->id_admin;
					$dataAdmin = $this->app_global_superadmin_model->getAdmin($id)->row_array();

					//echo $this->sendNotif();
					if(empty($dataAdmin['no_hp'])){
						$no = 0;
					}else{
						$no =  $dataAdmin['no_hp'];
					}
						
				
					$this->sendNotif($no,$pesan);
				}
			}
			
			$this->app_global_superadmin_model->updateSend(0);

			
			$d['skpd'] = $this->app_global_superadmin_model->countSkpd();
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb('DASHBOARD', '/');
			$d['title'] = "Dashboard Admin";

			if ($this->session->userdata("id_skpd")=='0') { 
				$d['count_tiket'] = $this->app_global_superadmin_model->count_tiket_superadmin();
				$d['bar'] = $this->app_global_superadmin_model->countTiket();
				$d['bar2'] = $this->app_global_superadmin_model->countTiketSMS();

			} else {
				$id_skpd = $this->session->userdata("id_skpd");
				$d['count_tiket'] = $this->app_global_superadmin_model->count_tiket_skpd($id_skpd);	
				$d['bar'] = $this->app_global_superadmin_model->countTiketSkpd($id_skpd);
				$d['bar2'] = $this->app_global_superadmin_model->countTiketSMSSkpd($id_skpd);
			}
		
			$this->load->view('bg_header',$d);
			$this->load->view('bg_home',$d);
			$this->load->view('bg_footer');  
		}
		else
		{
			redirect("superadmin");
		}
	}
}
