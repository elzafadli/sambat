<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class jenis extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("JENIS TIKET SAMBATAN", '/');
			
			$d['title'] = "Jenis Tiket Sambatan";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_jenis($this->config->item("limit_item"),$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('jenis/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("JENIS TIKET SAMBATAN", base_url().'superadmin/jenis');
			$this->breadcrumb->append_crumb("TAMBAH JENIS", '/');

			$d['jenis'] = "";
			
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			$d['title'] = "Tambah Jenis Tiket";
			$this->load->view('bg_header',$d);
			$this->load->view('jenis/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$d['generate_child_skpd'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("JENIS TIKET SAMBATAN", base_url().'superadmin/jenis');
			$this->breadcrumb->append_crumb("EDIT JENIS", '/');
			$d['title'] = "Edit Jenis Tiket";
			$where['id_jenis'] = $id_param;
			$get = $this->db->get_where("sam_jenis",$where)->row();
			$d['jenis'] = $get->jenis;
			$d['id_param'] = $get->id_jenis;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('jenis/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['id_jenis'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$in['jenis'] = $this->input->post("jenis");
				$this->db->insert("sam_jenis",$in);
			}
			else if($tipe=="edit")
			{
				$in['jenis'] = $this->input->post("jenis");
				$in['id_jenis'] = $this->input->post("id_jenis");
				$this->db->update("sam_jenis",$in,$id);
			}
			
			redirect("superadmin/jenis");
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_jenis'] = $id_param;
			$this->db->delete("sam_jenis",$where);
			redirect("superadmin/jenis");
		}
		else
		{
			redirect("superadmin");
		}
   }
}
 
/* End of file superadmin.php */
