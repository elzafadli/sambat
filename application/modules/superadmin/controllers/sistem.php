<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sistem extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SYSTEM MANAGEMENT", '/');
			$d['title'] = "System Management";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_sistem(10,$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('sistem/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
   
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SYSTEM MANAGEMENT", base_url().'superadmin/sistem');
			$this->breadcrumb->append_crumb("EDIT", '/');
			
			$d['aktif_slide_banner'] = "";
			$d['aktif_category'] = "";
			$d['aktif_product'] = "";
			
			$where['id_setting'] = $id_param;
			$get = $this->db->get_where("sam_setting",$where)->row();
			
			$d['tipe'] = $get->tipe;
			$d['title'] = $get->title;
			$d['content_setting'] = $get->content_setting;
			
			$d['id_param'] = $get->id_setting;
			$d['password'] = $get->password;
			
			//var_dump($d);
			$this->load->view('bg_header',$d);
			if($id_param == 13){
				$this->load->view('sistem/bg_email');
			}else{
				$this->load->view('sistem/bg_input');
			}
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			if($this->input->post("email") != null){
				if($this->input->post("pass1") != $this->input->post("pass2")){
					$this->session->set_flashdata('result', 'Password yang Anda masukkan tidak sama !!!');
					redirect("superadmin/sistem/edit/13");
				}else{
					$id['id_setting'] = $this->input->post("id_param");
					$in['tipe'] = $this->input->post("tipe");
					$in['content_setting'] = $this->input->post("email");
					$in['password'] = $this->input->post("pass1");
					
					$this->db->update("sam_setting",$in,$id);

					$this->session->set_flashdata('result', 'Email berhasil di Update!!!');
					redirect("superadmin/sistem/edit/13");
				}
				
			}else{
				$tipe = $this->input->post("tipe");
				$id['id_setting'] = $this->input->post("id_param");
				
				$in['tipe'] = $this->input->post("tipe");
				$in['title'] = $this->input->post("title");
				$in['content_setting'] = $this->input->post("content_setting");
				
				$this->db->update("sam_setting",$in,$id);
			}
			
			redirect("superadmin/sistem");
		}
		else
		{
			redirect("superadmin");
		}
   }
}
 
/* End of file superadmin.php */
