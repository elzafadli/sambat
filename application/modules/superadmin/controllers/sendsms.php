<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sendsms extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("KIRIM SMS", '/');
			
			$d['phonebook'] = $this->app_global_web_model->generate_phonebook();
			$d['title'] = "Kirim SMS";
			$this->load->view('bg_header',$d);
			$this->load->view('sendsms/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function test(){
      echo "Hello World";
   }
 
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$no_hp = $this->input->post("Number");
			$isi_pesan=$this->input->post("isi");
			// hitung berapa jumlah sms dengan membaginya dengan 160
			$jml_karakter=strlen($isi_pesan);
				if($jml_karakter <= 160) {
					$data0 = array(
   						'DestinationNumber' => $no_hp,
   						'TextDecoded' => $isi_pesan,
   						'CreatorID' => 'Gammu'
  						);
  					$this->db->insert('outbox', $data0);
				}
				else{
					$jml_potongan_SMS = ceil(strlen($isi_pesan)/153);
					$potongan_SMS = str_split($isi_pesan, 153);
					$masuk = "SHOW TABLE STATUS LIKE 'outbox'";
					$hasil = mysql_query($masuk);
					$data  = mysql_fetch_array($hasil);
    				$newID = $data['Auto_increment'];
    				// proses penyimpanan ke tabel mysql untuk setiap pecahan
    					for ($i=1; $i<=$jml_potongan_SMS; $i++)
    					{
    						// membuat UDH untuk setiap pecahan, sesuai urutannya
    						$udh = "050003A7".sprintf("%02s", $jml_potongan_SMS).sprintf("%02s", $i);
    						$msg = $potongan_SMS[$i-1];
    						if ($i == 1)
    						{
    							$data1 = array(
   									'DestinationNumber' => $no_hp,
   									'UDH' => $udh,
   									'TextDecoded' => $msg,
   									'ID' => $newID,
   									'Multipart' => 'true',
   									'CreatorID' => 'Gammu'
  								);
  								$this->db->insert('outbox', $data1);
    						}
    						else
    						{
    							$data2 = array(
   									'UDH' => $udh,
   									'TextDecoded' => $msg,
   									'ID' => $newID,
   									'SequencePosition' => $i
  								);
  								$this->db->insert('outbox_multipart', $data2);
    						}
    					}
    				}
          $this->session->set_flashdata('result', 'SMS telah terkirim......');
					redirect("superadmin/sendsms");
		}
		else
		{
			redirect("superadmin");
		}
   }
}
 
/* End of file superadmin.php */
