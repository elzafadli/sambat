<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class group extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("GROUP SMS", '/');
			
			$d['title'] = "Group SMS";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_indexs_group($this->config->item("limit_item"),$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('group/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("GROUP SMS", base_url().'superadmin/group');
			$this->breadcrumb->append_crumb("TAMBAH GROUP", '/');
			$d['Name'] = "";	
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			$d['title'] = "Tambah Group";
			$this->load->view('bg_header',$d);
			$this->load->view('group/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			//$d['generate_child_skpd'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("GROUP SMS", base_url().'superadmin/group');
			$this->breadcrumb->append_crumb("EDIT GROUP", '/');
			$d['title'] = "Edit Group";
			$where['ID'] = $id_param;
			$get = $this->db->get_where("pbk_groups",$where)->row();
			//$d['parent'] = $this->app_global_web_model->generate_parent();
			$d['Name'] = $get->Name;
			$d['id_param'] = $get->ID;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('group/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['ID'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$in['Name'] = $this->input->post("Name");
				$this->db->insert("pbk_groups",$in);
			}
			else if($tipe=="edit")
			{
				$in['Name'] = $this->input->post("Name");
				$this->db->update("pbk_groups",$in,$id);
			}
			
			redirect("superadmin/group");
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['ID'] = $id_param;
			$this->db->delete("pbk_groups",$where);
			redirect("superadmin/group");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_child($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_child($parent_id);
 		$this->load->view('skpd/child',$d);
	}
}
 
/* End of file superadmin.php */
