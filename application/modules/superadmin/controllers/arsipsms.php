<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class arsipsms extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("ARSIP TIKET SMS", '/');
			$d['title'] = "Arsip Tiket SMS";
			$d['parent'] = $this->app_global_web_model->generate_skpd();			
			$this->load->view('bg_header',$d);
			$this->load->view('arsipsms/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function cari($uri=0)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{

			if($this->input->post("id_skpd") == 0){
				$skpd = $this->input->post("id_skpd0");
			}else{
				$skpd = $this->input->post("id_skpd");	
			}
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("CARI ARSIP TIKET SMS", '/');

			if($this->input->post("tanggal1") =="" or $this->input->post("tanggal2") == ""){
				redirect('superadmin/arsipsms/','refresh');
			}

			$input1 = $this->input->post("tanggal1");
			$tanggal1 = DateTime::createFromFormat('Y/m/d H:i:s', $input1);
			// $tanggal1 = $tanggal1asli->getTimestamp();

			$input2 = $this->input->post("tanggal2");
			$tanggal2 = DateTime::createFromFormat('Y/m/d H:i:s', $input2);
			// $tanggal2 = $tanggal2asli->getTimestamp();

			$d['parent'] = $this->app_global_web_model->generate_skpd();
			$d['title'] = "Cari Arsip Tiket SMS " .$input1." - ".$input2."";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_arsip_sms($input1,$input2,$this->
				config->item("limit_item"),$uri,$skpd);

			$d['tanggal1'] =$input1;
			$d['tanggal2'] =$input2;
			$d['skpd'] =$skpd;

			$this->load->view('bg_header',$d);
			$this->load->view('arsipsms/bg_cari');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function xls()
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tanggal1 = $this->input->post("Etanggal1");
			$tanggal2 = $this->input->post("Etanggal2");
			$s = $this->input->post("search");
			$tanggal11 = urldecode($tanggal1);
			$skpd = $this->input->post("skpd");

			//echo $tanggal1;echo $tanggal2;echo $tanggal11;
			$d['title'] = "Arsip Tiket SMS " .$tanggal1." - ".$tanggal2.".xls";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_arsip_sms_xls($tanggal1,$tanggal2,$s,$skpd);
			//echo $s;
			//print_r($d['data_retrieve']);
			$this->load->view('arsipsms/bg_xls',$d);
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
