<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiket_terjawab extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif("c.id_skpd",2)->num_rows();
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET TERJAWAB", '/');
			$d['title'] = "Tiket Terjawab";
			if ($this->session->userdata("id_skpd")=='0') { 

			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_terjawab();
			
			} else {
			$id_skpd = $this->session->userdata("id_skpd");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_terjawab_skpd($id_skpd,$this->config->item("limit_item"),$uri);
			}
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_terjawab/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function closed($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id['id_tiket'] = $id_param;
			$in['st'] = 1;
			$this->db->update("sam_tiket",$in,$id);
			redirect("superadmin/tiket_terjawab/");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
