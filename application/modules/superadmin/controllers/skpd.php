<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class skpd extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SKPD MANAGEMENT", '/');
			
			$d['title'] = "SKPD Management";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_indexs_skpd($this->config->item("limit_item"),$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('skpd/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SKPD MANAGEMENT", base_url().'superadmin/skpd');
			$this->breadcrumb->append_crumb("TAMBAH SKPD", '/');
			$d['parent'] = $this->app_global_web_model->generate_parent();
			$d['nama_skpd'] = "";
			$d['id_parent'] = "";
			
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			$d['title'] = "Tambah SKPD";
			$this->load->view('bg_header',$d);
			$this->load->view('skpd/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$d['generate_child_skpd'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SKPD MANAGEMENT", base_url().'superadmin/skpd');
			$this->breadcrumb->append_crumb("EDIT SKPD", '/');
			$d['title'] = "Edit SKPD";
			$where['id_skpd'] = $id_param;
			$get = $this->db->get_where("sam_skpd",$where)->row();
			$d['parent'] = $this->app_global_web_model->generate_parent();
			$d['nama_skpd'] = $get->nama_skpd;
			$d['id_parent'] =$get->id_parent;
			$d['id_param'] = $get->id_skpd;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('skpd/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['id_skpd'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$in['nama_skpd'] = $this->input->post("nama_skpd");
				$in['id_parent'] = $this->input->post("id_parent");
				
				$this->db->insert("sam_skpd",$in);
			}
			else if($tipe=="edit")
			{
				$in['nama_skpd'] = $this->input->post("nama_skpd");
				$in['id_parent'] = $this->input->post("id_parent");
				$this->db->update("sam_skpd",$in,$id);
			}
			
			redirect("superadmin/skpd");
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_skpd'] = $id_param;
			$this->db->delete("sam_skpd",$where);
			redirect("superadmin/skpd");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_child($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_child($parent_id);
 		$this->load->view('skpd/child',$d);
	}
}
 
/* End of file superadmin.php */
