<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profil extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("PROFILE", '/');
			$d['title'] = "Profile";
			$where['id_admin'] = $this->session->userdata("id_admin");
			$get = $this->db->get_where("sam_admin",$where)->row();
			
			$d['nama_user'] = $get->nama;
			$d['username'] = $get->username;
			
			$d['id_param'] = $get->id_admin;
			
			$this->load->view('bg_header',$d);
			$this->load->view('profil/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id['id_admin'] = $this->input->post("id_param");
			$in['nama'] = $this->input->post("nama_user");
			$sess_data['nama'] = $in['nama'];
			$this->session->set_userdata($sess_data);	
			$this->db->update("sam_admin",$in,$id);
			$this->session->set_flashdata('result', 'Anda berhasil memperbarui data');
			redirect("superadmin/profil");
		}
		else
		{
			redirect("superadmin");
		}
   }
}
 
/* End of file superadmin.php */
