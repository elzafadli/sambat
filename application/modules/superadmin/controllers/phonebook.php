<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class phonebook extends MX_Controller {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="" && $this->session->userdata("id_skpd")=='0')
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("PHONEBOOK PERSONAL", '/');
			
			$d['title'] = "Phonebook Personal";
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_indexs_phonebook($this->config->item("limit_item"),$uri);
			
			$this->load->view('bg_header',$d);
			$this->load->view('phonebook/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function tambah()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("PHONEBOOK PERSONAL", base_url().'superadmin/phonebook');
			$this->breadcrumb->append_crumb("TAMBAH PHONEBOOK", '/');
			$d['group'] = $this->app_global_web_model->generate_group();
			$d['Name'] = "";
			$d['GroupID'] = "";
			$d['Number'] = "";	
			$d['id_param'] = "";
			$d['tipe'] = "tambah";
			$d['title'] = "Tambah Phonebook";
			$this->load->view('bg_header',$d);
			$this->load->view('phonebook/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			//$d['generate_child_skpd'] = $this->app_global_web_model->generate_child_skpd();
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("PHONEBOOK PERSONAL", base_url().'superadmin/phonebook');
			$this->breadcrumb->append_crumb("EDIT PHONEBOOK", '/');
			$d['title'] = "Edit Phonebook";
			$d['group'] = $this->app_global_web_model->generate_group();
			$where['ID'] = $id_param;
			$get = $this->db->get_where("pbk",$where)->row();
			//$d['parent'] = $this->app_global_web_model->generate_parent();
			$d['Name'] = $get->Name;
			$d['Number'] = $get->Number;
			$d['GroupID'] = $get->GroupID;
			$d['id_param'] = $get->ID;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('phonebook/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpan()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$tipe = $this->input->post("tipe");
			$id['ID'] = $this->input->post("id_param");
			if($tipe=="tambah")
			{
				$in['Name'] = $this->input->post("Name");
				$in['Number'] = $this->input->post("Number");
				$in['GroupID'] = $this->input->post("GroupID");
				$this->db->insert("pbk",$in);
			}
			else if($tipe=="edit")
			{
				$in['Name'] = $this->input->post("Name");
				$in['Name'] = $this->input->post("Name");
				$in['Number'] = $this->input->post("Number");
				$in['GroupID'] = $this->input->post("GroupID");
				$this->db->update("pbk",$in,$id);
			}
			
			redirect("superadmin/phonebook");
		}
		else
		{
			redirect("superadmin");
		}
   }
 
	public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['ID'] = $id_param;
			$this->db->delete("pbk",$where);
			redirect("superadmin/phonebook");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_child($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_child($parent_id);
 		$this->load->view('skpd/child',$d);
	}
}
 
/* End of file superadmin.php */
