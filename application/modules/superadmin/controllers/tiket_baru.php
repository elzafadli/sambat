<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiket_baru extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->session->set_userdata('page', 'index/'.$uri);
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", '/');
			$d['title'] = "Tiket Baru";

			if ($this->session->userdata("id_skpd")=='0') { 
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_baru($this->config->item("limit_item"),$uri);

			} else {
			$id_skpd = $this->session->userdata("id_skpd");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_baru_skpd($id_skpd,$this->config->item("limit_item"),$uri);

			}
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }


   	public function deleteForwardSKPD($id_cc,$id_tiket){
		$this->db->where('id_tiket_cc', $id_cc);
		
		$this->db->delete('sam_tiket_cc');

		//$page = $this->session->userdata("page");

		redirect("superadmin/tiket_baru/cc/".$id_tiket);
	}

	public function deleteForwardSMSSKPD($id_cc,$id_tiket){
		$this->db->where('id_sms_cc', $id_cc);
		
		$this->db->delete('sam_tiket_cc_sms');

		//$page = $this->session->userdata("page");

		redirect("superadmin/tiket_baru_sms/ccsms/".$id_tiket);
	}
   
   public function cc($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", base_url().'superadmin/tiket_baru');
			$this->breadcrumb->append_crumb("FORWARD ke OPD", '/');
			$d['title'] = "CC Tiket #$id_param";
			$d['id_param'] = $id_param;
			$d['tiket_cc'] = $this->app_global_superadmin_model->generate_tiket_cc($id_param);

			$where['id_tiket'] = $id_param;
			$get = $this->db->get_where("sam_tiket",$where)->row();
			$id_jenis = $get->id_jenis;
			$id_kategori = $get->id_kategori;
			$d['jenis'] = $this->app_global_superadmin_model->generate_child_jenis_select($id_jenis);
			$d['kategori'] = $this->app_global_superadmin_model->generate_child_kategori_select($id_kategori);
			$d['parent'] = $this->app_global_web_model->generate_skpd();
			$d['skpd_cc'] = $this->app_global_superadmin_model->generate_skpd_cc($id_param);
			//$get = $this->db->get_where("sam_tiket",$where)->row();
			
			//$d['tipe'] = $get->tipe;
			//$d['title'] = $get->title;
			//$d['content_setting'] = $get->content_setting;
			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru/bg_cc');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function reply($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", base_url().'superadmin/tiket_baru');
			$this->breadcrumb->append_crumb("CC SKPD", '/');
			$d['title'] = "Reply Tiket #$id_param";
			$d['id_param'] = $id_param;
			$d['tiket_cc'] = $this->app_global_superadmin_model->generate_tiket_cc($id_param);

			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru/bg_reply');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function jawab($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", base_url().'superadmin/tiket_baru');
			$this->breadcrumb->append_crumb("JAWAB TIKET", '/');
			$d['title'] = "Jawab Tiket #$id_param";
			$d['id_param'] = $id_param;
			$d['tiket_cc'] = $this->app_global_superadmin_model->generate_tiket_cc($id_param);
			//$get = $this->db->get_where("sam_tiket",$where)->row();
			
			//$d['tipe'] = $get->tipe;
			//$d['title'] = $get->title;
			//$d['content_setting'] = $get->content_setting;
			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru/bg_jawab');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpantiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id_tiket = $this->input->post("id_param");
			$id['id_tiket'] = $this->input->post("id_param");
			
			$in['id_jenis'] = $this->input->post("id_jenis");
			$in['id_kategori'] = $this->input->post("id_kategori");

			//echo "Hello";
			$this->db->update("sam_tiket",$in,$id);
			
			redirect("superadmin/tiket_baru/cc/$id_tiket");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function cctiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id_tiket = $this->input->post("id_param");
			
			$in['id_tiket'] = $this->input->post("id_param");
			if ($this->input->post("id_skpd")==0){
				$in['id_skpd'] = $this->input->post("id_skpd0");
				$id_skpd = $this->input->post("id_skpd0");	
			} else {
				$in['id_skpd'] = $this->input->post("id_skpd");
				$id_skpd = $this->input->post("id_skpd");
			}

			//$pesan = $test['nama_skpd'];
			//$id_skpd = $this->input->post("id_skpd0");
			$in['tanggal_pengiriman'] = date("Y-m-d H:i:s"); 
			$this->db->insert("sam_tiket_cc",$in);
			$temp1 = $this->app_global_superadmin_model->getSkpd($id_skpd)->row_array();
			$userid = $this->app_global_superadmin_model->getUser($id_tiket)->row_array();
			
			//notifikasi User
			$in2['id_tiket'] = $id_tiket;
			$in2['id_user'] = $userid['id_user'];
			$in2['pesan'] = "Pesan Anda dengan ID Tiket ".$id_tiket ." sudah terkirim ke ".$temp1['nama_skpd'];  
			$in2['id_admin'] = $this->session->userdata("id_admin");
			$data = $this->app_global_superadmin_model->getDetailUser($userid['id_user'])->row_array();

			//notifikasi Admin
			$pesan2 = "Pesan baru dengan ID Tiket: ".$id_tiket;

			$Admin = $this->app_global_superadmin_model->getIdAdmin($id_skpd)->result();

			require 'PHPMailer/PHPMailerAutoload.php';
			$email = $this->app_user_web_model->getEmail()->row_array();


			foreach ($Admin as $k) {

				$where2	= array(
					'id_tiket'	=> $id_tiket,
					'id_admin'	=> $k->id_admin,
					'id_skpd'	=> $k->id_skpd,
					'pesan'	=> $pesan2,
					'tipe' => 1,
					'id_sender' => $this->session->userdata("id_admin")
					);

				$this->db->insert('notifikasi_skpd',$where2);

				

				$mail = new PHPMailer;

				$mail->isSMTP();                                   // Set mailer to use SMTP
				$mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                            // Enable SMTP authentication
				$mail->Username = $email['content_setting'];          // SMTP username
				$mail->Password = $email['password']; // SMTP password
				$mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                 // TCP port to connect to

				$mail->setFrom($email['content_setting'], 'Admin Sambat');
				$mail->addReplyTo($email['content_setting'], 'Admin Sambat');

				$mail->addAddress($k->email);   // Add a recipient
				//$mail->addCC('cc@example.com');
				//$mail->addBCC('bcc@example.com');

				$mail->isHTML(true);  // Set email format to HTML

				$bodyContent = '<h1>'.$pesan2.'</h1>';

				$mail->Subject = 'Tiket Baru dari SAMBAT';
				$mail->Body    = $bodyContent;

				//echo $jwt;
				//redirect("web/login/pemulihan/sukses","refresh");
				
				if(!$mail->send()) {
					$this->session->set_flashdata('result', 'Email tidak dapat Terkirim. Coba check internet Anda!!!');
				} else {
					$this->session->set_flashdata('result', 'Email pemberitahuan telah dikirim ke SKPD');
				} 
			}

			$this->db->insert("notifikasi",$in2);
			$this->sendNotif($data['no_telpon'],$in2['pesan']);

			redirect("superadmin/tiket_baru/cc/".$id_tiket);
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function jawabtiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			$id_tiket = $this->input->post("id_param");
			$in['id_tiket'] = $this->input->post("id_param");
			$in['id_skpd'] = $this->session->userdata("id_skpd");
			$in['tanggapan'] = $this->input->post("tanggapan");
			$this->db->insert("sam_tanggapan",$in);

			$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah masuk.');
			redirect("superadmin/tiket_terjawab");
		}
		else
		{
			redirect("superadmin");
		}
   }


   function sendNotif($number,$isi){

	   	$data0 = array(
	   		'DestinationNumber' => $number,
	   		'TextDecoded' => $isi,
	   		'CreatorID' => 'Gammu'
	   		);

	   	if($this->db->insert('outbox', $data0)){
	   		return "OK";
	   	}else{
	   		return "Gagal";
	   	}
   }

   public function jawabtiketKominfo()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id = $this->session->userdata("id_admin");
			$id_tiket = $this->input->post("id_param");

			$data = $this->app_global_superadmin_model->getSuperAdmin($id)->row_array();

			//print_r($data);
			$pesan = $this->input->post("tanggapan")." (ID Tiket: ".$id_tiket.")"; 
			$pesan1 = $this->input->post("tanggapan")." - Pesan dari: ".$data['nama'];
			
			
			$in['id_tiket'] = $this->input->post("id_param");
			//$in['id_skpd'] = $this->session->userdata("id_skpd");
			$in['pesan'] = $pesan;
			//$this->db->insert("sam_tanggapan",$in);
			$userid = $this->app_global_superadmin_model->getUser($id_tiket)->row_array();
			$in['id_user'] = $userid['id_user'];
			$in['id_admin'] = $this->session->userdata("id_admin");
			//echo $userid['id_user'];

			$detail = $this->app_global_superadmin_model->getDetailUser($userid['id_user'])->row_array();

			//print_r($detail);
			//print_r($userid);
			echo $this->sendNotif($detail['no_telpon'],$pesan1);

			$this->db->insert("notifikasi",$in);


			$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah terkirim');
			//print_r($in);
			//$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah masuk.');
			redirect("superadmin/tiket_baru/reply/".$id_tiket); 
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['id_tiket'] = $id_param;
			$this->db->delete("sam_tiket",$where);
			redirect("superadmin/tiket_baru");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
