<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiket_terjawab_sms extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("SMS TERJAWAB", '/');
			$d['title'] = "SMS Terjawab";
			if ($this->session->userdata("id_skpd")=='0') { 
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_terjawab_sms();
			
			} else {
			$id_skpd = $this->session->userdata("id_skpd");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_terjawab_skpd_sms($id_skpd,$this->config->item("limit_item"),$uri);
			$d['jumlah'] = $this->app_global_superadmin_model->countNotif($id_skpd,1)->num_rows();
			}
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_terjawab_sms/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function closed($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id['ID'] = $id_param;
			$in['Class'] = 2;
			$this->db->update("tempinbox",$in,$id);
			redirect("superadmin/tiket_terjawab_sms");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
