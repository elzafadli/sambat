<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pesan extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/

	function __construct()
	{
          // this is your constructor
		parent::__construct();
		$this->load->model('app_global_superadmin_model');

		$this->app_global_superadmin_model->updateNotifAdmin($this->session->userdata('id_admin'));
		$jumlah = $this->app_global_superadmin_model->countNotifAdmin($this->session->userdata('id_admin'))->num_rows();

		$this->session->set_userdata('notifAdmin', $jumlah);

		if($this->session->userdata("logged_in_admin") == ""){
			redirect("superadmin");
		}
	}

	function getAdmin(){
		
	}

	function delete($id){
		//$this->app_global_superadmin_model->deletePesan($set,$id);
		$this->db->where('id_notifikasi', $id);
		$this->db->delete('notifikasi_skpd');

		$this->session->set_flashdata('result','Pesan berhasil di hapus !!!');

		redirect("superadmin/pesan");
	}

	function sendNotif($number,$isi){

		$data0 = array(
			'DestinationNumber' => $number,
			'TextDecoded' => $isi,
			'CreatorID' => 'Gammu'
			);

		if($this->db->insert('outbox', $data0)){
			return "OK";
		}else{
			return "Gagal";
		}
	}

	function sendPesan(){
		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
		$this->breadcrumb->append_crumb('Kirim Pesan', '/');
		$d['title'] = "Pesan";

		$this->load->view('bg_header',$d);
		$this->load->view("pesan/kirimpesan");
		$this->load->view('bg_footer');
	}

	function insertPesan(){
		$pesan = $this->input->post("tanggapan");
		$admin = $this->app_global_superadmin_model->getSuperAdmin()->result();
		;

		foreach ($admin as $item) {
			$data = array(
				'id_sender' => $this->session->userdata('id_admin'),
				'pesan' => $pesan,
				'id_admin' => $item->id_admin,
				'id_skpd' => $item->id_skpd,
				'id_tiket' => 0,
				'tipe' => 3,
				);



			$this->session->set_flashdata('result','Pesan berhasil di Kirim !!!');

			$this->db->insert('notifikasi_skpd', $data);
		}

		$d['pesan'] = $this->app_global_superadmin_model->notifSkpd()->result();

		foreach ($d['pesan'] as $item) {

			$id = $item->id_admin;
			$pesan= $item->pesan;

			$dataAdmin = $this->app_global_superadmin_model->getAdmin($id)->row_array();
			
			$no = $dataAdmin['no_hp'];
			$this->sendNotif($no,$pesan);
		}

		$this->app_global_superadmin_model->updateSend(0);

		redirect("superadmin/pesan/sendPesan");
		
	}

	function index()
	{
		if($this->session->userdata('logged_in_admin')!="")
		{

			//$this->app_global_superadmin_model->updateNotif();
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();
			//$this->app_global_superadmin_model->updateNotif();
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb('DASHBOARD', '/');
			$d['title'] = "Pesan";

			if ($this->session->userdata("id_skpd")=='0') { 
				$set	= array(
					'readStatus' => 0,
					);

				//$this->app_global_superadmin_model->updatePesanR($set,'id_notifikasi');

				$id = $this->session->userdata("id_admin");
				$d['pesan'] = $this->app_global_superadmin_model->getPesan($id)->result();

			} else {
				$set	= array(
					'readStatus' => 1,
					);

				//$this->app_global_superadmin_model->updatePesanR($set);

				$id = $this->session->userdata("id_admin");
				$d['pesan'] = $this->app_global_superadmin_model->getPesan($id)->result();
			}

			$this->load->view('bg_header',$d);
			$this->load->view('pesan/pesan');
			$this->load->view('bg_footer'); 
		}
		else
		{
			redirect("superadmin");
		}
	}
}
