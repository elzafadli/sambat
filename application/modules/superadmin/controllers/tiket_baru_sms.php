<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiket_baru_sms extends MX_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
 
   public function index($uri=0)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->session->set_userdata('page', 'index/'.$uri);
			
			//$d['jumlah'] = $this->app_global_superadmin_model->countNotif()->num_rows();

			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU SMS", '/');
			$d['title'] = "Tiket Baru SMS";

			if ($this->session->userdata("id_skpd")=='0') {
			 
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_baru_sms($this->config->item("limit_item"),$uri);

			} else {
			$id_skpd = $this->session->userdata("id_skpd");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_tiket_baru_sms_skpd($id_skpd,$this->config->item("limit_item"),$uri);
			}

			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru_sms/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
   
   public function ccsms($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET SMS", base_url().'superadmin/tiket_baru_sms');
			$this->breadcrumb->append_crumb("FORWARD SMS ke OPD", '/');
			$d['title'] = "CC Tiket #$id_param";
			$d['id_param'] = $id_param;
			$d['tiket_cc'] = $this->app_global_superadmin_model->generate_tiket_cc_sms($id_param);
			$d['parent'] = $this->app_global_web_model->generate_skpd();
			$d['skpd_cc'] = $this->app_global_superadmin_model->generate_skpd_cc_sms($id_param);
			//$get = $this->db->get_where("sam_tiket",$where)->row();
			
			//$d['tipe'] = $get->tipe;
			//$d['title'] = $get->title;
			//$d['content_setting'] = $get->content_setting;
			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru_sms/bg_cc');
			$this->load->view('bg_footer');


		}
		else
		{
			redirect("superadmin");
		}
   }

   public function editsms($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET SMS", base_url().'superadmin/tiket_baru_sms');
			$this->breadcrumb->append_crumb("EDIT SMS", '/');
			$d['title'] = "Edit Tiket #$id_param";
			$d['id_param'] = $id_param;
			$d['tiket_cc'] = $this->app_global_superadmin_model->generate_tiket_cc_sms($id_param);
			$d['parent'] = $this->app_global_web_model->generate_skpd();
			
			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru_sms/bg_edit');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function jawab($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", base_url().'superadmin/tiket_baru_sms');
			$this->breadcrumb->append_crumb("JAWAB SMS", '/');

			

			$d['title'] = "Jawab SMS #$id_param";
			$d['id_param'] = $id_param;
			$d['sms_cc'] = $this->app_global_superadmin_model->generate_tiket_cc_sms($id_param);
			//$get = $this->db->get_where("sam_tiket",$where)->row();
			
			//$d['tipe'] = $get->tipe;
			//$d['title'] = $get->title;
			//$d['content_setting'] = $get->content_setting;
			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru_sms/bg_jawab');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function reply($id_param)
   {
		if($this->session->userdata("logged_in_admin")!="")
		{


			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url().'superadmin');
			$this->breadcrumb->append_crumb("TIKET BARU", base_url().'superadmin/tiket_baru');
			$this->breadcrumb->append_crumb("CC SKPD", '/');
			
			$d['title'] = "Jawab SMS #$id_param";
			$d['id_param'] = $id_param;
			$d['sms_cc'] = $this->app_global_superadmin_model->generate_tiket_cc_sms($id_param);
			//$get = $this->db->get_where("sam_tike

			
			$this->load->view('bg_header',$d);
			$this->load->view('tiket_baru_sms/bg_reply');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("superadmin");
		}
   }
 
   public function simpantiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id_tiket = $this->input->post("id_param");
			$id['id_tiket'] = $this->input->post("id_param");
			
			$in['id_jenis'] = $this->input->post("id_jenis");
			$in['id_kategori'] = $this->input->post("id_kategori");

			$this->db->update("sam_tiket",$in,$id);
			
			redirect("superadmin/tiket_baru/cc/$id_tiket");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function simpansms()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id_sms = $this->input->post("id_param");
			$id['ID'] = $this->input->post("id_param");
			$in['TextDecoded'] = $this->input->post("tiketsms");
			$this->db->update("tempinbox",$in,$id);
			$this->session->set_flashdata('resultsms', 'SMS berhasil diperbahurui');
			redirect("superadmin/tiket_baru_sms/editsms/$id_sms");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function cctiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id_tiket = $this->input->post("id_param");

			//echo $id_tiket;
			
			$in['id_sms'] = $this->input->post("id_param");
			if ($this->input->post("id_skpd")==0){
				$in['id_skpd'] = $this->input->post("id_skpd0");
				$id_skpd = $this->input->post("id_skpd0");	
			} else {
				$in['id_skpd'] = $this->input->post("id_skpd");
				$id_skpd = $this->input->post("id_skpd");
			}
			$in['tanggal_pengiriman'] = date("Y-m-d H:i:s");
			if($this->db->insert("sam_tiket_cc_sms",$in)){
				echo "OK";
			}else{
				redirect("superadmin/tiket_baru_sms/ccsms/".$id_tiket);
			}

			$pesan2 = "Pesan baru dengan ID Tiket SMS: ".$id_tiket;

			$Admin = $this->app_global_superadmin_model->getIdAdmin($id_skpd)->result();

			require 'PHPMailer/PHPMailerAutoload.php';
			$email = $this->app_user_web_model->getEmail()->row_array();

			foreach ($Admin as $k) {

				$where2	= array(
					'id_tiket'	=> $id_tiket,
					'id_admin'	=> $k->id_admin,
					'id_skpd'	=> $k->id_skpd,
					'pesan'	=> $pesan2,
					'tipe' => 2,
					'id_sender' => $this->session->userdata("id_admin")
					);

				if($this->db->insert('notifikasi_skpd',$where2)){
					echo "OK";
				}


				$mail = new PHPMailer;

				$mail->isSMTP();                                   // Set mailer to use SMTP
				$mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                            // Enable SMTP authentication
				$mail->Username = $email['content_setting'];          // SMTP username
				$mail->Password = $email['password']; // SMTP password
				$mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                 // TCP port to connect to

				$mail->setFrom($email['content_setting'], 'Admin Sambat');
				$mail->addReplyTo($email['content_setting'], 'Admin Sambat');

				$mail->addAddress($k->email);   // Add a recipient
				//$mail->addCC('cc@example.com');
				//$mail->addBCC('bcc@example.com');

				$mail->isHTML(true);  // Set email format to HTML

				$bodyContent = '<h1>'.$pesan2.'</h1>';

				$mail->Subject = 'Tiket Baru dari SAMBAT';
				$mail->Body    = $bodyContent;

				if(!$mail->send()) {
					$this->session->set_flashdata('result', 'Email tidak dapat Terkirim. Coba check internet Anda!!!');
				} else {
					$this->session->set_flashdata('result', 'Email pemberitahuan telah dikirim ke SKPD');
				} 
			}

			$data = $this->app_global_superadmin_model->getDetailSender($id_tiket)->row_array();

			$id_skpd = $this->input->post("id_skpd0");
			
			$temp1 = $this->app_global_superadmin_model->getSkpd($id_skpd)->row_array();
			$pesan = "Pesan Anda dengan ID Tiket ".$id_tiket ." sudah terkirim ke ".$temp1['nama_skpd'];  

			$this->sendNotif($data['SenderNumber'],$pesan);
			
			/*
			$where['id_skpd'] = $id_skpd;
			$query = $this->db->get_where("sam_admin",$where);
			if ($query->num_rows() > 0)
			{
				$row = $query->row();
				$no_hp_skpd = $row->no_hp;
				$nama_skpd = $row->nama;
			}
			$data1 = array(
   					'DestinationNumber' => $no_hp_skpd,
   					'TextDecoded' => 'Halo admin SKPD '.$nama_skpd.', Anda memiliki sambatan terbaru. Silakan balas melalui aplikasi sambat online',
   					'CreatorID' => 'Gammu'
  				);
  			$this->db->insert('outbox', $data1);
  			*/
		
			redirect("superadmin/tiket_baru_sms/ccsms/".$id_tiket);
		}
		else
		{
			redirect("superadmin");
		}
   }

     function sendNotif($number,$isi){

	   	$data0 = array(
	   		'DestinationNumber' => $number,
	   		'TextDecoded' => $isi,
	   		'CreatorID' => 'Gammu'
	   		);

	   	if($this->db->insert('outbox', $data0)){
	   		return "OK";
	   	}else{
	   		return "Gagal";
	   	}
   }

     public function jawabtiketKominfo()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			$id = $this->session->userdata("id_admin");
			$data = $this->app_global_superadmin_model->getSuperAdmin($id)->row_array();

			$id_tiket = $this->input->post("id_param");
			$in['id_sms'] = $this->input->post("id_param");
			$in['id_skpd'] = $this->session->userdata("id_skpd");
			$in['tanggapan'] = $this->input->post("tanggapan");
			$tanggapansms = $this->input->post("tanggapan");
			$no_hp = $this->input->post("no_hp");
			$this->db->insert("sam_tanggapan_sms",$in);

			$isi_pesan=$this->input->post("tanggapan")." - Pesan dari: ".$data['nama'];
			// hitung berapa jumlah sms dengan membaginya dengan 160
			$jml_karakter=strlen($isi_pesan);

			if($jml_karakter <= 160) {
				$data0 = array(
   					'DestinationNumber' => $no_hp,
   					'TextDecoded' => $isi_pesan,
   					'CreatorID' => 'Gammu'
  				);
  				$this->db->insert('outbox', $data0);
			}
			else{
				$jml_potongan_SMS = ceil(strlen($isi_pesan)/153);
				$potongan_SMS = str_split($isi_pesan, 153);
				$masuk = "SHOW TABLE STATUS LIKE 'outbox'";
				$hasil = mysql_query($masuk);
				$data  = mysql_fetch_array($hasil);
    			$newID = $data['Auto_increment'];
    			// proses penyimpanan ke tabel mysql untuk setiap pecahan
    			for ($i=1; $i<=$jml_potongan_SMS; $i++)
    			{
    				// membuat UDH untuk setiap pecahan, sesuai urutannya
    				$udh = "050003A7".sprintf("%02s", $jml_potongan_SMS).sprintf("%02s", $i);
    				$msg = $potongan_SMS[$i-1];
    				if ($i == 1)
    				{
    					$data1 = array(
   							'DestinationNumber' => $no_hp,
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'Multipart' => 'true',
   							'CreatorID' => 'Gammu'
  						);
  						$this->db->insert('outbox', $data1);
    				}
    				else
    				{
    					$data2 = array(
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'SequencePosition' => $i
  						);
  						$this->db->insert('outbox_multipart', $data2);
    				}
    			}
			}
			
			$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah masuk.');
			redirect("superadmin/tiket_baru_sms/reply/".$id_tiket);
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function jawabtiket()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			$id_tiket = $this->input->post("id_param");
			$in['id_sms'] = $this->input->post("id_param");
			$in['id_skpd'] = $this->session->userdata("id_skpd");
			$in['tanggapan'] = $this->input->post("tanggapan");
			$tanggapansms = $this->input->post("tanggapan");
			$no_hp = $this->input->post("no_hp");
			$this->db->insert("sam_tanggapan_sms",$in);
			$isi_pesan=$this->input->post("tanggapan");
			// hitung berapa jumlah sms dengan membaginya dengan 160
			$jml_karakter=strlen($isi_pesan);
			if($jml_karakter <= 160) {
				$data0 = array(
   					'DestinationNumber' => $no_hp,
   					'TextDecoded' => $isi_pesan,
   					'CreatorID' => 'Gammu'
  				);
  				$this->db->insert('outbox', $data0);
			}
			else{
				$jml_potongan_SMS = ceil(strlen($isi_pesan)/153);
				$potongan_SMS = str_split($isi_pesan, 153);
				$masuk = "SHOW TABLE STATUS LIKE 'outbox'";
				$hasil = mysql_query($masuk);
				$data  = mysql_fetch_array($hasil);
    			$newID = $data['Auto_increment'];
    			// proses penyimpanan ke tabel mysql untuk setiap pecahan
    			for ($i=1; $i<=$jml_potongan_SMS; $i++)
    			{
    				// membuat UDH untuk setiap pecahan, sesuai urutannya
    				$udh = "050003A7".sprintf("%02s", $jml_potongan_SMS).sprintf("%02s", $i);
    				$msg = $potongan_SMS[$i-1];
    				if ($i == 1)
    				{
    					$data1 = array(
   							'DestinationNumber' => $no_hp,
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'Multipart' => 'true',
   							'CreatorID' => 'Gammu'
  						);
  						$this->db->insert('outbox', $data1);
    				}
    				else
    				{
    					$data2 = array(
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'SequencePosition' => $i
  						);
  						$this->db->insert('outbox_multipart', $data2);
    				}
    			}
			}
			
			$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah masuk.');
			redirect("superadmin/tiket_terjawab_sms");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function jawabtiketcepat()
   {
		if($this->session->userdata("logged_in_admin")!="")
		{
			
			$id_tiket = $this->input->post("id_param");
			$in['id_sms'] = $this->input->post("id_param");
			$in['id_skpd'] = $this->session->userdata("id_skpd");
			$in['tanggapan'] = $this->input->post("tanggapan");
			$tanggapansms = $this->input->post("tanggapan");
			$no_hp = $this->input->post("no_hp");
			$this->db->insert("sam_tanggapan_sms",$in);
			$isi_pesan=$this->input->post("tanggapan");
			// hitung berapa jumlah sms dengan membaginya dengan 160
			$jml_karakter=strlen($isi_pesan);
			if($jml_karakter <= 160) {
				$data0 = array(
   					'DestinationNumber' => $no_hp,
   					'TextDecoded' => $isi_pesan,
   					'CreatorID' => 'Gammu'
  				);
  				$this->db->insert('outbox', $data0);
			}
			else{
				$jml_potongan_SMS = ceil(strlen($isi_pesan)/153);
				$potongan_SMS = str_split($isi_pesan, 153);
				$masuk = "SHOW TABLE STATUS LIKE 'outbox'";
				$hasil = mysql_query($masuk);
				$data  = mysql_fetch_array($hasil);
    			$newID = $data['Auto_increment'];
    			// proses penyimpanan ke tabel mysql untuk setiap pecahan
    			for ($i=1; $i<=$jml_potongan_SMS; $i++)
    			{
    				// membuat UDH untuk setiap pecahan, sesuai urutannya
    				$udh = "050003A7".sprintf("%02s", $jml_potongan_SMS).sprintf("%02s", $i);
    				$msg = $potongan_SMS[$i-1];
    				if ($i == 1)
    				{
    					$data1 = array(
   							'DestinationNumber' => $no_hp,
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'Multipart' => 'true',
   							'CreatorID' => 'Gammu'
  						);
  						$this->db->insert('outbox', $data1);
    				}
    				else
    				{
    					$data2 = array(
   							'UDH' => $udh,
   							'TextDecoded' => $msg,
   							'ID' => $newID,
   							'SequencePosition' => $i
  						);
  						$this->db->insert('outbox_multipart', $data2);
    				}
    			}
			}
			
			$this->session->set_flashdata('result', 'Terimakasih, tanggapan Anda telah masuk.');
			redirect("superadmin/tiket_terjawab_sms");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function hapus($id_param)
	{
		if($this->session->userdata("logged_in_admin")!="")
		{
			$where['ID'] = $id_param;
			$this->db->delete("tempinbox",$where);
			redirect("superadmin/tiket_baru_sms");
		}
		else
		{
			redirect("superadmin");
		}
   }

   public function get_skpd($parent_id)
	{
 		$this->load->helper('url');
 		$d['child']=$this->app_global_web_model->generate_skpd($parent_id);
 		$this->load->view('user/child',$d);
	}
}
 
/* End of file superadmin.php */
