<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class daftar extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/

	function index()
	{
		if($this->session->userdata('logged_in')=="")
		{
	
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('DAFTAR', '/');
			$d['title'] = "Daftar";
			$d['captcha'] = $this->generate_captcha();
			//$d['kecamatan'] = $this->app_global_web_model->generate_kecamatan();
			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/daftar/bg_home');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect("user/dashboard");
		}
			
	}

	function set()
	{
		$in['username'] = $this->input->post("username", TRUE);
		$in['password'] = md5($this->input->post("password", TRUE).$this->config->item("key_login"));
		$in['email'] = $this->input->post("email", TRUE);
		$in['nama'] = $this->input->post("nama", TRUE);
		$in['alamat'] = $this->input->post("alamat", TRUE);
		$in['profesi'] = $this->input->post("profesi", TRUE); 
		$in['no_telpon'] = $this->input->post("no_telpon", TRUE);	
		$expiration = time()-3600;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		
		if ($row->count == 0)
		{
			$this->session->set_flashdata('result', 'Captcha tidak valid');
			redirect("web/daftar");
		}
		else
		{
			$cek_email = $this->db->get_where("sam_user",array("email"=>$in['email']))->num_rows();
			if($cek_email>0)
			{
				$this->session->set_flashdata('result', 'Email telah terpakai');
				redirect("web/daftar");
			}
			else
			{
				$pass1 = $this->input->post("password");
				$pass2 = $this->input->post("password2");
				
				if($pass1==$pass2)
				{
					if($_SESSION['site_send_activation']=="yes")
					{
						$a = '';
						for ($i=0; $i<7; $i++)
						{
							$a .= mt_rand(0,9);
						}
						//$in['kode_aktivasi'] = md5($in['email'].time());
						$in['kode_aktivasi'] = $a;
						$in['stts'] = 0;
						$this->db->insert("sam_user",$in);
						$id = mysql_insert_id();
						$data1 = array(
   							'DestinationNumber' => $in['no_telpon'],
   							'TextDecoded' => 'ID Anda : '.$id.' Kode Aktivasi : '.$a.'',
   							'CreatorID' => 'Gammu'
  							);
  						$this->db->insert('outbox', $data1);
						// $config = array(
      // 								'protocol' => 'smtp',
  				// 					'smtp_host' => 'ssl://smtp.gmail.com',
  				// 					'smtp_port' => 465,
  				// 					'smtp_user' => 'mabescrew@gmail.com', 
  				// 					'smtp_pass' => 'mabes123456', 
  				// 					'mailtype' => 'html',
  				// 					'charset' => 'iso-8859-1',
  				// 					'wordwrap' => TRUE
      // 								);
  				// 		$this->load->library('email',$config);
 					// 	$this->email->set_newline("\r\n");
						
						// $this->email->from($_SESSION['site_email_server'], $_SESSION['site_title']);
						// $this->email->to($in['email']);
						// $this->email->set_mailtype('html');
						// $this->email->subject('Link Aktivasi - '.$_SESSION['site_title']);
						// $this->email->message(base_url().'web/daftar/aktif/'.$id.'/'.$in['kode_aktivasi']);
						//$this->email->send();
						
						$this->session->set_flashdata('result', 'Kode verifikasi telah dikirim ke nomor Anda');
						redirect("web/daftar/aktivasi/");
					}
					else
					{
						$in['stts'] = 1;
						$this->db->insert("sam_user",$in);
						$this->session->set_flashdata('result', 'Pendaftaran sukses, silahkan login dengan akun anda');
						redirect("web/login");
					}
				}
				else
				{
						$this->session->set_flashdata('result', 'password tidak sama');
						redirect("web/daftar");
				}
				
			}
		}
	}

	function aktivasi()
	{
		if($this->session->userdata('logged_in')=="")
		{
	
			$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
			$this->breadcrumb->append_crumb('AKTIVASI', '/');
			$d['title'] = "Aktivasi";
			//$d['captcha'] = $this->generate_captcha();
			//$d['kecamatan'] = $this->app_global_web_model->generate_kecamatan();
			$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
			$this->load->view($_SESSION['site_theme'].'/daftar/bg_aktivasi');
			$this->load->view($_SESSION['site_theme'].'/bg_footer');
		}
		else
		{
			redirect("user/dashboard");
		}
	}

	function setaktivasi()
	{
			$id['id_user'] = $this->input->post("id");
			$where['kode_aktivasi'] = $this->input->post("kode_aktivasi");
			$where['id_user'] = $this->input->post("id");
			$cek = $this->db->get_where("sam_user",$where)->num_rows();
			if($cek>0)
		{
			$up['stts'] = 1;
			$this->db->update("sam_user",$up,$id);
			$this->session->set_flashdata('result', 'Akun berhasil diaktifkan, silakan login dengan akun Anda');
			redirect("web/login");
		}
		else
		{
			$this->session->set_flashdata('result', 'Kode tidak valid');
			redirect("web/daftar/aktivasi/'.$id.'");
		}
	}

	function aktif($id_param,$kode)
	{
		$id['id_user'] = $id_param;
		$where['kode_aktivasi'] = $kode;
		$where['id_user'] = $id_param;
		$cek = $this->db->get_where("sam_user",$where)->num_rows();
		if($cek>0)
		{
			$up['stts'] = 1;
			$this->db->update("sam_user",$up,$id);
			$this->session->set_flashdata('result', 'Akun berhasil diaktifkan, silakan login dengan akun Anda');
			redirect("web/login");
		}
		else
		{
			$this->session->set_flashdata('result', 'Kode tidak valid');
			redirect("web/daftar");
		}
	}
	
	function generate_captcha()
	{
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/font8.ttf',
			'img_width' => '180',
			'img_height' => 60
			);
		$cap = create_captcha($vals);
		$datamasuk = array(
			'captcha_time' => $cap['time'],
			//'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
			);
		$expiration = time()-3600;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$query = $this->db->insert_string('captcha', $datamasuk);
		$this->db->query($query);
		return $cap['image'];
	}

	function get_kelurahan($parent_id)
	{
 		$this->load->helper('url');
 		$d['kelurahan']=$this->app_global_web_model->generate_kelurahan($parent_id);
 		$this->load->view($_SESSION['site_theme'].'/sign_up/kelurahan',$d);
	}
}
