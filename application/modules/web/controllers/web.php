<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class web extends CI_Controller {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/

	function Api($jenis){
		
		if ($jenis == "web") {
			if (empty($_GET['limit']) or empty($_GET['offset'])) {
			 	$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL")->result();
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL")->result();	
			}else{
				$limit = $_GET['limit'];
				$offset = $_GET['offset'];

				$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL",null,$limit,$offset)->result();
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL",null,$limit,$offset)->result();	
			} 	

		}elseif ($jenis == "sms") {
			if (empty($_GET['limit']) or empty($_GET['offset'])) {
				$data['terjawab'] = $this->app_user_web_model->getAllSMSTiket("NOT NULL")->result();
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllSMSTiket("NULL")->result();	
			}else{
				$limit = $_GET['limit'];
				$offset = $_GET['offset'];
				
				$data['terjawab'] = $this->app_user_web_model->getAllSMSTiket("NOT NULL",$limit,$offset)->result();
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllSMSTiket("NULL",$limit,$offset)->result();
			}
		}elseif ($jenis == "kategori"){

			$id = $_GET['id_kategori'];	
			$kategori = $this->app_user_web_model->getKategori($id)->row_array();

			if (empty($_GET['limit']) or empty($_GET['offset'])) {	
				$data['id'] = $id;
				$data['kategori'] = $kategori['kategori'];
				$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL",$id)->result();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL",$id)->result();	
			}else{
				$limit = $_GET['limit'];
				$offset = $_GET['offset'];

				$data['id'] = $id;
				$data['kategori'] = $kategori['kategori'];
				$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL",$id,$limit,$offset)->result();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL",$id,$limit,$offset)->result();	
			}
		}elseif ($jenis == "bulan") {
			$bulan = $_GET['bulan'];	
			$tahun = $_GET['tahun'];	

			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			if (empty($_GET['limit']) or empty($_GET['offset'])) {
				$data['terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NOT NULL",$bulan,$tahun)->result();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NULL",$bulan,$tahun)->result();
			}else{
				$limit = $_GET['limit'];
				$offset = $_GET['offset'];

				$data['terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NOT NULL",$bulan,$tahun,$limit,$offset)->result();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NULL",$bulan,$tahun,$limit,$offset)->result();
			}	
		}else{
			$status = "400 Bad Request";
		}

		if (empty($data)) {
			$status = "400 Bad Request";
			echo $status;
		}else{
			echo json_encode($data);
		}
		
	}

	function ApiKategori(){
		echo json_encode($this->app_global_superadmin_model->getKategori()->result());
	}

	function ApiGrafik($jenis){
		
		if ($jenis == "web") {
			$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL")->num_rows();
			$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL")->num_rows();	
	

		}elseif ($jenis == "sms") {
			
				$data['terjawab'] = $this->app_user_web_model->getAllSMSTiket("NOT NULL")->num_rows();
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllSMSTiket("NULL")->num_rows();	

		}elseif ($jenis == "kategori"){

			$id = $_GET['id_kategori'];	
			$kategori = $this->app_user_web_model->getKategori($id)->row_array();

			
				$data['id'] = $id;
				$data['kategori'] = $kategori['kategori'];
				$data['terjawab'] = $this->app_user_web_model->getAllWebTiket("NOT NULL",$id)->num_rows();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getAllWebTiket("NULL",$id)->num_rows();	
			
		}elseif ($jenis == "bulan") {
			
				$bulan = $_GET['bulan'];	
				$tahun = $_GET['tahun'];	

				$data['bulan'] = $bulan;
				$data['tahun'] = $tahun;
				$data['terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NOT NULL",$bulan,$tahun)->num_rows();	
				$data['tidak_terjawab'] = $this->app_user_web_model->getWebTiketPerBulan("NULL",$bulan,$tahun)->num_rows();
			
		}else{
			$status = "400 Bad Request";
		}

		if (empty($data)) {
			$status = "400 Bad Request";
			echo $status;
		}else{
			echo json_encode($data);
		}
		
	}

	function sambatApiTiket(){
		//echo "Hello World";
		if(isset($_GET['v'])){

			$token = $_GET['v'];	

			if ($token == "eyJhbGciOiAiSFMyNTYiLCJ0eXAiOiAiSldUIn0=.eyJjb3VudHJ5IjogIk1hbGFuZyIsIm5hbWUiOiAiRWx6YSBGYWRsaSIsImVtYWlsIjogImVsemFmYWRsaUBnbWFpbC5jb20ifQ==.OAf6fHCZrbTxCmDiQF9Rk9nU/yMAleFn2rn2jJ9yW0Q="){

				$data['tiket'] = $this->app_user_web_model->getTiket()->result();

				echo json_encode($data);
			}else{
				echo "400 Bad Request";
			}
		}
			else{
				echo "400 Bad Request";
			}
		//json_encode($tiketSMS);
		}

	function sambatApiSMS(){
		//echo "Hello World";
	if(isset($_GET['v'])){
		$token = $_GET['v'];	

		if ($token == "eyJhbGciOiAiSFMyNTYiLCJ0eXAiOiAiSldUIn0=.eyJjb3VudHJ5IjogIk1hbGFuZyIsIm5hbWUiOiAiRWx6YSBGYWRsaSIsImVtYWlsIjogImVsemFmYWRsaUBnbWFpbC5jb20ifQ==.OAf6fHCZrbTxCmDiQF9Rk9nU/yMAleFn2rn2jJ9yW0Q="){
			$tiket = $this->app_user_web_model->getTiketSMS()->result();
			$i = 0;
			foreach ($tiket as $item) {
					$tiketSMS[$i]['text'] = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '',$item->TextDecoded);
					$tiketSMS[$i]['pengirim'] = $item->SenderNumber;
					$tiketSMS[$i]['tanggal'] = $item->ReceivingDateTime;
			$i++;
				}
			$data['tiketSMS'] = $tiketSMS;
			echo json_encode($data);

		}else{
			echo "400 Bad Request";
		}
	}
		else{
			echo "400 Bad Request";
		}

		//j
		//var_dump($data);
		//json_encode($tiketSMS);
	}

	function sambatAPIGrafik(){
		if(isset($_GET['v'])){
			$token = $_GET['v'];	
			
			if ($token == "eyJhbGciOiAiSFMyNTYiLCJ0eXAiOiAiSldUIn0=.eyJjb3VudHJ5IjogIk1hbGFuZyIsIm5hbWUiOiAiRWx6YSBGYWRsaSIsImVtYWlsIjogImVsemFmYWRsaUBnbWFpbC5jb20ifQ==.OAf6fHCZrbTxCmDiQF9Rk9nU/yMAleFn2rn2jJ9yW0Q="){
		//echo "Hello World";
				$data['kategori'] = $this->app_user_web_model->getKategoriTiket()->result();
				$data['webBulan'] =  $this->app_user_web_model->getBulanTiketWeb()->result();
				$data['smsBulan'] = $this->app_user_web_model->getBulanTiketSMS()->result();

				echo json_encode($data);
			}else{
				echo "400 Bad Request";
			}
		}
			else{
				echo "400 Bad Request";
			}
		//json_encode($tiketSMS);
		}

	function index()
	{	
 		$this->load->helper('text');
 		$d['title'] = "Home";
 		$d['captcha'] = $this->generate_captcha();
 		$d['tiket_gambar'] = $this->app_global_web_model->generate_tiket_gambar();
 		$d['tiket_teks'] = $this->app_global_web_model->generate_tiket_teks();
 		$d['tiket_sms'] = $this->app_global_web_model->generate_tiket_sms();
 		
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_home');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function faq()
	{	
 		$d['title'] = "FAQ";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('FAQ', '/');
 		$d['captcha'] = $this->generate_captcha();
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_faq');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}	

	function alur()
	{	
 		$d['title'] = "Alur";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('ALUR', '/');
 		$d['captcha'] = $this->generate_captcha();
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_alur');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function tiket_selengkapnya($uri=0)
	{	
 		$this->load->helper('text');
 		$d['title'] = "Tiket Selengkapnya";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('TIKET VIA WEBSITE', '/');
		$d['captcha'] = $this->generate_captcha();
 		$d['tiket_selengkapnya'] = $this->app_global_web_model->generate_tiket_selengkapnya(10,$uri);
 		//$d['tiket_teks'] = $this->app_global_web_model->generate_tiket_teks();
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_selengkapnya');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function sms_selengkapnya($uri=0)
	{	
 		$this->load->helper('text');
 		$d['title'] = "SMS Selengkapnya";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('TIKET VIA SMS', '/');
		$d['captcha'] = $this->generate_captcha();
 		$d['sms_selengkapnya'] = $this->app_global_web_model->generate_sms_selengkapnya(10,$uri);
 		//$d['tiket_teks'] = $this->app_global_web_model->generate_tiket_teks();
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_sms_selengkapnya');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function detailtiket($id_param)
	{	
 		$d['title'] = "Detail Tiket #$id_param";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('DETAIL TIKET', '/');
 		$d['captcha'] = $this->generate_captcha();
 		$d['detail_tiket'] = $this->app_global_web_model->generate_tiket_detail($id_param);
 		$d['tanggapan_tiket'] = $this->app_global_web_model->generate_tiket_tanggapan($id_param);
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_detail');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function detailtiketsms($id_param)
	{	
 		$d['title'] = "Detail SMS #$id_param";
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('DETAIL TIKET SMS', '/');
 		$d['captcha'] = $this->generate_captcha();
 		$d['detail_tiket_sms'] = $this->app_global_web_model->generate_tiket_detail_sms($id_param);
 		$d['tanggapan_tiket_sms'] = $this->app_global_web_model->generate_tiket_tanggapan_sms($id_param);
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_detail_sms');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function lacaktiket()
	{	
 
 		$this->breadcrumb->append_crumb('<span class="icon mif-home"></span>', base_url());
		$this->breadcrumb->append_crumb('LACAK TIKET', '/');
 		$d['captcha'] = $this->generate_captcha();

 		$id_tiket = $this->input->post("id_tiket");
 		$d['title'] = "Lacak Tiket #$id_tiket";
 		$d['lacak_tiket'] = $this->app_global_web_model->generate_tiket_lacak($id_tiket);
 		$this->load->view($_SESSION['site_theme'].'/bg_header',$d);
 		$this->load->view($_SESSION['site_theme'].'/bg_lacak');
 		$this->load->view($_SESSION['site_theme'].'/bg_footer');
	}

	function generate_captcha()
	{
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/font8.ttf',
			'img_width' => '180',
			'img_height' => 60
			);
		$cap = create_captcha($vals);
		$datamasuk = array(
			'captcha_time' => $cap['time'],
			//'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
			);
		$expiration = time()-3600;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$query = $this->db->insert_string('captcha', $datamasuk);
		$this->db->query($query);
		return $cap['image'];
	}
}
