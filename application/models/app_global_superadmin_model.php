<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class app_global_superadmin_model extends CI_Model {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	public function checkNotif(){
		$query = $this->db->query("SELECT c.id_tiket,c.id_skpd,a.id_admin,c.tanggal_pengiriman as tanggal FROM sam_tiket_cc as c left join sam_tanggapan as t on t.id_tiket = c.id_tiket join sam_skpd as d on c.id_skpd = d.id_skpd join sam_admin as a on c.id_skpd =a.id_skpd where id_tanggapan is NULL and id_tanggapan is NULL and c.notifikasi = 1 and a.view = 1 and date_add(tanggal_pengiriman, INTERVAL 3 day ) < Now() ORDER by c.id_tiket desc");

		return $query;
		
		$query->free_result();
	}

	public function getKategori(){
		$query = $this->db->query("SELECT * FROM `sam_kategori`");

		return $query;
		
		$query->free_result();
	}

	public function getDateKategori(){
		$query = $this->db->query("SELECT FROM_UNIXTIME(tanggal, '%Y') as tahun FROM sam_tiket GROUP BY FROM_UNIXTIME(tanggal, '%Y')");

		return $query;
		
		$query->free_result();
	}



	public function getDetailKategori($bulan,$tahun){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah, k.kategori,FROM_UNIXTIME(t.tanggal , '%m') as bulan, FROM_UNIXTIME(t.tanggal , '%Y') as tahun FROM `sam_tiket` as t join sam_kategori as k on k.id_kategori = t.id_kategori where FROM_UNIXTIME(t.tanggal , '%Y') = $tahun and FROM_UNIXTIME(t.tanggal , '%m') = $bulan GROUP by k.id_kategori,FROM_UNIXTIME(t.tanggal , '%Y'),FROM_UNIXTIME(t.tanggal , '%m')");

		return $query;
		
		$query->free_result();
	}

	public function getDetailKategoriSMS($bulan,$tahun){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah, k.kategori,FROM_UNIXTIME(t.tanggal , '%m') as bulan, FROM_UNIXTIME(t.tanggal , '%Y') as tahun FROM `sam_tiket` as t join sam_kategori as k on k.id_kategori = t.id_kategori where FROM_UNIXTIME(t.tanggal , '%Y') = $tahun and FROM_UNIXTIME(t.tanggal , '%m') = $bulan GROUP by k.id_kategori,FROM_UNIXTIME(t.tanggal , '%Y'),FROM_UNIXTIME(t.tanggal , '%m')");

		return $query;
		
		$query->free_result();
	}


	public function checkNotifSMS($id){
		$query = $this->db->query("SELECT c.id_sms, c.id_skpd, a.id_admin,tanggal_pengiriman as tanggal FROM sam_tiket_cc_sms as c left join sam_tanggapan as t on t.id_tiket = c.id_sms_cc join sam_skpd as d on c.id_skpd = d.id_skpd join sam_admin as a on a.id_skpd = d.id_skpd where c.id_skpd=$id and id_tanggapan is NULL and id_tanggapan is NULL and c.checkTanggal = 1 and date_add(tanggal_pengiriman, INTERVAL 3 day ) < Now()");

		return $query;
		
		$query->free_result();
	}

	public function getPesan($id){
		$query = $this->db->query("SELECT * FROM notifikasi_skpd as s join sam_admin as a on s.id_sender=a.id_admin and s.view = 1 where s.id_admin=$id order by id_notifikasi desc");

		return $query;
		
		$query->free_result();
	}

	public function updatePesanR(){
		$this->db->set('readStatusA', 0);
		$this->db->update('notifikasi_skpd');
	}

	public function deletePesan($data,$id=null){
		return $this->db->set($data)->where('id_notifikasi',$id)->update('notifikasi_skpd');
	}

	public function notifSkpd(){
		$query = $this->db->query("SELECT * FROM notifikasi_skpd where send=1");

		return $query;
		
		$query->free_result();
	}

	public function getSkpd($id){
		$query = $this->db->query("SELECT nama_skpd FROM sam_skpd where id_skpd = $id");

		return $query;
		
		$query->free_result();
	}

	public function getUser($id){
		$query = $this->db->query("SELECT * FROM sam_tiket where id_tiket=$id");

		return $query;
		
		$query->free_result();
	}

	public function getEmail($data){
		$query = $this->db->query("SELECT * FROM `sam_user` where email='$data'");

		return $query;
		
		$query->free_result();
	}

	public function getIDMasyarakat($id){
		$query = $this->db->query("SELECT * FROM sam_user where id_user=$id");

		return $query;
		
		$query->free_result();
	}


	public function getDetailUser($id){
		$query = $this->db->query("SELECT * FROM sam_user where id_user=$id");

		return $query;
		
		$query->free_result();
	}

	public function getDetailSender($id){
		$query = $this->db->query("SELECT * FROM `tempinbox` where ID=$id");

		return $query;
		
		$query->free_result();
	}

	public function getSMSGroup(){
		$query = $this->db->query("SELECT * FROM pbk_groups");

		return $query;
		
		$query->free_result();
	}

	public function getAdmin($id){
		$query = $this->db->query("SELECT * FROM sam_admin where id_admin=$id");

		return $query;
		
		$query->free_result();
	}

	public function getIdAdmin($id){
		$query = $this->db->query("SELECT * FROM `sam_admin` WHERE id_skpd = $id");

		return $query;
		
		$query->free_result();
	}

	public function getSuperAdmin(){
		$query = $this->db->query("SELECT * FROM sam_admin where id_skpd=0 and view=1");

		return $query;
		
		$query->free_result();
	}

	public function getIDSuperAdmin($id){
		$query = $this->db->query("SELECT * FROM sam_admin where id_skpd=0 and id_admin=$id");

		return $query;
		
		$query->free_result();
	}

	public function updateSend($notif){
		$this->db->set('send', $notif);
		$this->db->update('notifikasi_skpd');
	}


	public function countNotif($id,$notif){

		$query = $this->db->query("SELECT * FROM notifikasi_skpd where id_skpd=$id and $notif");

		return $query;
		
		$query->free_result();
	}


	public function countNotifAdmin($id){

		$query = $this->db->query("SELECT * FROM notifikasi_skpd where id_admin=$id and readStatus=1");

		return $query;
		
		$query->free_result();
	}

	public function updateNotifAdmin($id){

		$this->db->set('readStatus', 0);
		$this->db->where('id_admin', $id);
		$this->db->update('notifikasi_skpd');
		
		//$query->free_result();
	}

	public function updateNewNotif($notif,$id_tiket){
		$this->db->where('id_tiket', $id_tiket);
		$this->db->set('notifikasi', $notif);
		$this->db->update('sam_tiket_cc');
	}

	public function updateNewNotifSMS($notif,$id_sms){
		$this->db->where('id_sms', $id_sms);
		$this->db->set('checkTanggal', $notif);
		$this->db->update('sam_tiket_cc_sms');
	}

	public function updateNotif($notif){
		$this->db->set('notifikasi', $notif);
		$this->db->update('sam_tiket_cc');
	}

	public function updateNotifSMS($notif){
		$this->db->set('checkTanggal', $notif);
		$this->db->update('sam_tiket_cc_sms');
	}

	public function countTikets($tanggal1,$tanggal2){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah, FROM_UNIXTIME(tanggal , '%m') as bulan, FROM_UNIXTIME(tanggal , '%Y') as tahun FROM sam_tiket where tanggal between $tanggal1 and $tanggal2 GROUP by FROM_UNIXTIME(tanggal , '%m'), FROM_UNIXTIME(tanggal , '%Y') ORDER By tahun,bulan");

		return $query->result();
		
		$query->free_result();
	}
	public function countTiketsSMS($tanggal1,$tanggal2){
		$query = $this->db->query("SELECT mOnth(ReceivingDateTime) as bulan, year(ReceivingDateTime) as tahun , COUNT(id) AS jumlah FROM tempinbox where ReceivingDateTime between '$tanggal1' and '$tanggal2' GROUP by month(ReceivingDateTime),year(ReceivingDateTime) ORDER BY tahun,bulan ASC");

		return $query->result();
		
		$query->free_result();
	}

	public function countTiket(){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah, FROM_UNIXTIME(tanggal , '%m') as bulan FROM sam_tiket where FROM_UNIXTIME(tanggal , '%Y') = YEAR(CURDATE()) GROUP by FROM_UNIXTIME(tanggal , '%m')");

		return $query->result();
		
		$query->free_result();
	}

	public function countTiketSkpd($id){
		$query = $this->db->query("SELECT COUNT(t.id_tiket) as jumlah, FROM_UNIXTIME(tanggal , '%m') as bulan FROM sam_tiket as t JOIN sam_tiket_cc as c on t.id_tiket = c.id_tiket where FROM_UNIXTIME(tanggal , '%Y') = YEAR(CURDATE()) and c.id_skpd=$id GROUP by FROM_UNIXTIME(tanggal , '%m')");

		return $query->result();
		
		$query->free_result();
	}

	public function countTiketSMSSkpd($id){
		$query = $this->db->query("SELECT mOnth(ReceivingDateTime) as bulan , COUNT(id) AS jumlah FROM tempinbox as t JOIN sam_tiket_cc_sms as c on t.ID = c.id_sms where Year(ReceivingDateTime) = YEAR(CURDATE()) and c.id_skpd=$id GROUP by month(ReceivingDateTime)");

		return $query->result();
		
		$query->free_result();
	}

	public function countTiketSMS(){
		$query = $this->db->query("SELECT mOnth(ReceivingDateTime) as bulan , COUNT(id) AS jumlah FROM tempinbox  where Year(ReceivingDateTime) = YEAR(CURDATE()) GROUP by month(ReceivingDateTime)");

		return $query->result();
		
		$query->free_result();
	}

	public function countSkpd(){
		$query = $this->db->query("SELECT COUNT(id_tiket_cc) as jumlah, s.nama_skpd as nama FROM `sam_tiket_cc` as t LEFT JOIN sam_skpd as s on t.id_skpd = s.id_skpd  GROUP by t.id_skpd order by jumlah desc limit 5 ");
 //HAVING COUNT(id_tiket_cc) > 5
		return $query->result();
		
		$query->free_result();
	}

	public function countSkpds($id){
		$query = $this->db->query("SELECT COUNT(id_tiket_cc) as jumlah, s.nama_skpd as nama FROM `sam_tiket_cc` as t LEFT JOIN sam_skpd as s on t.id_skpd = s.id_skpd where s.id_skpd=$id GROUP by t.id_skpd");
		return $query;
		
		$query->free_result();
	}

	public function countSkpdSMS(){
		$query = $this->db->query("SELECT COUNT(id_sms_cc) as jumlah, s.nama_skpd as nama FROM `sam_tiket_cc_sms` as t LEFT JOIN sam_skpd as s on t.id_skpd = s.id_skpd GROUP by t.id_skpd ORDER BY `jumlah` DESC limit 5 ");
 //HAVING COUNT(id_tiket_cc) > 5
		return $query->result();
		
		$query->free_result();
	}

	public function countSkpdsSMS($id){
		$query = $this->db->query("SELECT COUNT(id_sms_cc) as jumlah, s.nama_skpd as nama FROM `sam_tiket_cc_sms` as t LEFT JOIN sam_skpd as s on t.id_skpd = s.id_skpd where s.id_skpd=$id GROUP by t.id_skpd");
		return $query;
		
		$query->free_result();
	}

	public function countAllTiket($id){
		$query = $this->db->query("SELECT * FROM sam_tiket_cc where id_skpd = $id");
		//$query = $this->db->get('sam_tiket');

		return $query;
		
		$query->free_result();
	}

	public function countAllTiketCC($id){
		$query = $this->db->query("SELECT DISTINCT id_tiket FROM sam_tanggapan where id_skpd=$id");

		return $query;
		
		$query->free_result();
	}

	public function getListOPD(){
		$query = $this->db->query("SELECT DISTINCT id_tiket FROM sam_tanggapan where id_skpd=$id");

		return $query;
		
		$query->free_result();
	}


	public function countAllTiketSMS($id){
		$query = $this->db->query("SELECT * FROM sam_tiket_cc_sms where id_skpd=$id");
		//$query = $this->db->get('tempinbox');

		return $query;
		
		$query->free_result();
	}



	public function countAllTiketSMSCC($id){
		$query = $this->db->query("SELECT DISTINCT id_sms FROM sam_tanggapan_sms where id_skpd=$id");

		return $query;
		
		$query->free_result();
	}

	public function generate_index_user($limit,$offset,$filter=array())
	{
		$hasil="";
		$tot_hal = $this->db->get("sam_admin");

		$config['base_url'] = base_url() . 'superadmin/admin_dinas/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$s = $this->db->query("SELECT * FROM sam_admin as a LEFT JOIN sam_skpd as b ON a.id_skpd=b.id_skpd LEFT JOIN pbk_groups c ON a.GroupID=c.ID");
		// $s = $this->db
		// 			->get("kel_admin",$limit,$offset);
		$w = $this->db->like('nama',$filter['nama_user'])->get("sam_admin",$limit,$offset);
		
		$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='false'>
		<thead>
			<tr>
				<td class='sortable-column sort-asc' style='width: 60px'>No.</td>
				<td class='sortable-column'>Nama Admin</td>
				<td class='sortable-column'>Username</td>
				<td class='sortable-column'>Email</td>
				<td class='sortable-column'>No HP</td>
				<td class='sortable-column'>Group SMS</td>
				<td class='sortable-column'>OPD</td>
				<td style='width: 155px'><a href='".base_url()."superadmin/user/tambah' class='image-button primary small-button'>Tambah User<span class='icon mif-user-plus bg-darkCobalt'></span></a></td>
			</tr>
		</thead>";
		$i = $offset+1;
		if ($filter['nama_user']!='') { 
			foreach($w->result() as $h)
			{
				$hasil .= "<tr>
				<td>".$i."</td>
				<td>".$h->nama."</td>
				<td>".$h->username."</td>
				<td>".$h->email."</td>
				<td>".$h->no_hp."</td>
				<td>".$h->GroupID."</td>
				<td>";
					$hasil .= "<a href='".base_url()."superadmin/user/edit/".$h->id_admin."' class='button warning'><span class='mif-pencil'></span></a> ";
					$hasil .= "<a href='".base_url()."superadmin/user/hapus/".$h->id_admin."' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
				</tr>";
				$i++;
			}
		} else {
			foreach($s->result() as $j)
			{
				$hasil .= "<tr>
				<td>".$i."</td>
				<td>".$j->nama."</td>
				<td>".$j->username."</td>
				<td>".$j->email."</td>
				<td>".$j->no_hp."</td>";
				if ($j->GroupID==-1){
					$hasil .= "<td>Belum ada Group</td>";
				} else {
					$hasil .= "<td>".$j->Name."</td>";
				}
				if ($j->id_skpd==0) {
					$hasil .= "<td><i class='icon-heart-2'></i> superadmin</td>";
				} else {
					$hasil .="<td>".$j->nama_skpd."</td>";
				}
				$hasil .="<td>";
				$hasil .= "<a href='".base_url()."superadmin/user/edit/".$j->id_admin."' class='button warning'><span class='mif-pencil'></span></a> ";
				$hasil .= "<a href='".base_url()."superadmin/user/hapus/".$j->id_admin."' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
			</tr>";
			$i++;
		}
	}
	$hasil .= '</table>';
	return $hasil;
}


function clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	public function generate_index_user_masyarakat()
	{
		// $tot_hal = $this->db->get("sam_admin");

		// $config['base_url'] = base_url() . 'superadmin/admin_dinas/index/';
		// $config['total_rows'] = $tot_hal->num_rows();
		// $config['per_page'] = $limit;
		// $config['uri_segment'] = 4;
		// $config['first_link'] = 'First';
		// $config['last_link'] = 'Last';
		// $config['next_link'] = 'Next';
		// $config['prev_link'] = 'Prev';
		// $this->pagination->initialize($config);

		$s = $this->db->query("SELECT * FROM sam_user");
		// $s = $this->db
		// 			->get("kel_admin",$limit,$offset);
		//$w = $this->db->like('nama',$filter['nama_user'])->get("sam_admin",$limit,$offset);
		
		return $s;
	}
	
	public function generate_index_sistem($limit,$offset)
	{
		$hasil="";
		$tot_hal = $this->db->get("sam_setting");

		$config['base_url'] = base_url() . 'superadmin/sistem/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->get("sam_setting",$limit,$offset);
		
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th>Setting System</th>
				<th>Type</th>
				<th style='width: 70px'>Aksi</th>
			</tr>
		</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
			<td>".$i."</td>
			<td>".$h->title."</td>
			<td>".$h->tipe."</td>
			<td>";
				$hasil .= "<a href='".base_url()."superadmin/sistem/edit/".$h->id_setting."' class='button warning'><span class='mif-pencil'></span></a></td>
			</tr>";
			$i++;
		}
		$hasil .= '</table>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_indexs_skpd($limit,$offset)
	{
		$w = $this->db->order_by("id_skpd","DESC")->get("sam_skpd",$limit,$offset);
		$hasil ="";
		$tot_hal = $this->db->get("sam_skpd");

		$config['base_url'] = base_url() . 'superadmin/skpd/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th class='sortable-column sort-asc'>Nama OPD</th>
				<th style='width: 170px'><a href='".base_url()."superadmin/skpd/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah OPD</a></th>
			</tr>
		</thead>";
		$no=$offset+1;
		foreach($w->result() as $h)
		{	
			$hasil .= "<tr>";
			$hasil .= "<td>".$no."</td>";
			$hasil .= "<td>".$h->nama_skpd."</td>";
			$hasil .= "<td><a href='".base_url()."superadmin/skpd/edit/".$h->id_skpd."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/skpd/hapus/".$h->id_skpd."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>";
			$hasil .= "</tr>";
			$no++;
		}
		$hasil .= "</table>";
		$hasil .= '<div class="cleaner_h20"></div>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_indexs_group($limit,$offset)
	{
		$w = $this->db->order_by("ID","DESC")->get("pbk_groups",$limit,$offset);
		$hasil ="";
		$tot_hal = $this->db->get("pbk_groups");

		$config['base_url'] = base_url() . 'superadmin/group/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th class='sortable-column sort-asc'>Nama Group</th>
				<th style='width: 170px'><a href='".base_url()."superadmin/group/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah Group</a></th>
			</tr>
		</thead>";
		$no=$offset+1;
		foreach($w->result() as $h)
		{	
			$hasil .= "<tr>";
			$hasil .= "<td>".$no."</td>";
			$hasil .= "<td>".$h->Name."</td>";
			$hasil .= "<td><a href='".base_url()."superadmin/group/edit/".$h->ID."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/group/hapus/".$h->ID."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>";
			$hasil .= "</tr>";
			$no++;
		}
		$hasil .= "</table>";
		$hasil .= '<div class="cleaner_h20"></div>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_indexs_phonebook($limit,$offset)
	{
		$w = $this->db->query("SELECT a.ID,a.Name as NameP, a.Number, a.GroupID, b.Name, b.ID as IDG FROM pbk a left join pbk_groups b on a.GroupID=b.ID order by a.ID DESC LIMIT ".$offset.",".$limit."");
		$hasil ="";
		$tot_hal = $this->db->get("pbk");

		$config['base_url'] = base_url() . 'superadmin/phonebook/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th class='sortable-column sort-asc'>Nama</th>
				<th class='sortable-column sort-asc'>Nomor HP</th>
				<th class='sortable-column sort-asc'>Group</th>
				<th style='width: 170px'><a href='".base_url()."superadmin/phonebook/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah HP</a></th>
			</tr>
		</thead>";
		$no=$offset+1;
		foreach($w->result() as $h)
		{	
			$hasil .= "<tr>";
			$hasil .= "<td>".$no."</td>";
			$hasil .= "<td>".$h->NameP."</td>";
			$hasil .= "<td>".$h->Number."</td>";
			$hasil .= "<td>".$h->Name."</td>";
			$hasil .= "<td><a href='".base_url()."superadmin/phonebook/edit/".$h->ID."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/phonebook/hapus/".$h->ID."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>";
			$hasil .= "</tr>";
			$no++;
		}
		$hasil .= "</table>";
		$hasil .= '<div class="cleaner_h20"></div>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_index_jenis($limit,$offset)
	{
		$hasil="";
		$tot_hal = $this->db->get("sam_jenis");

		$config['base_url'] = base_url() . 'superadmin/jenis/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->get("sam_jenis",$limit,$offset);
		
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th>Jenis Tiket</th>
				<th style='width: 170px'><a href='".base_url()."superadmin/jenis/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah Jenis</a></th>
			</tr>
		</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
			<td>".$i."</td>
			<td>".$h->jenis."</td>
			<td>";
				$hasil .= "<a href='".base_url()."superadmin/jenis/edit/".$h->id_jenis."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/jenis/hapus/".$h->id_jenis."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>
			</tr>";
			$i++;
		}
		$hasil .= '</table>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_index_kategori($limit,$offset)
	{
		$hasil="";
		$tot_hal = $this->db->get("sam_jenis");

		$config['base_url'] = base_url() . 'superadmin/kategori/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->get("sam_kategori",$limit,$offset);
		
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th>No.</th>
				<th>Kategori Tiket</th>
				<th style='width: 180px'><a href='".base_url()."superadmin/kategori/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah Kategori</a></th>
			</tr>
		</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
			<td>".$i."</td>
			<td>".$h->kategori."</td>
			<td>";
				$hasil .= "<a href='".base_url()."superadmin/kategori/edit/".$h->id_kategori."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/kategori/hapus/".$h->id_kategori."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>
			</tr>";
			$i++;
		}
		$hasil .= '</table>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_index_tiket_baru($limit,$offset)
	{
		$hasil="";
		$where['st'] = '0';
		//$tot_hal = $this->db->get_where("sam_tiket",$where);
		$tot_hal = $this->db->get('sam_tiket');

		$config['base_url'] = base_url() . 'superadmin/tiket_baru/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->query("SELECT DISTINCT(a.id_tiket), b.jenis, c.kategori,a.tiket, d.tanggal_pengiriman FROM sam_tiket a left join sam_jenis b on a.id_jenis=b.id_jenis left join sam_kategori c on a.id_kategori=c.id_kategori left join sam_tiket_cc as d on a.id_tiket = d.id_tiket where a.st='0' order by a.id_tiket DESC LIMIT ".$offset.",".$limit."");
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th class='sortable-column'>ID Tiket</th>
				<th class='sortable-column'>Jenis</th>
				<th class='sortable-column'>Kategori</th>
				<th class='sortable-column'>Isi Tiket</th>
				<th class='sortable-column'>Tanggal Pengiriman</th>
				<th>OPD</th>
				<th style='width: 70px'>Aksi</th>
			</tr>
		</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			if($h->tanggal_pengiriman == ""){
				$kirim = "tiket belum terkirim ke OPD";
			}else{
				$kirim = $h->tanggal_pengiriman;
			}	

			$hasil .= "<tr>
			<td>#".$h->id_tiket."</td>
			<td>".$h->jenis."</td>
			<td>".$h->kategori."</td>
			<td>".$h->tiket."</td><td>".$kirim."</td><td>";
			$cc = $this->db->query("SELECT * FROM sam_tiket_cc a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_tiket=".$h->id_tiket."");
			foreach($cc->result() as $c)
			{
				$hasil .= "<p><span class='tag info'>".$c->nama_skpd."</span></p>";
			}		
			$hasil .= "</td><td>";
			$hasil .= "<a href='".base_url()."superadmin/tiket_baru/cc/".$h->id_tiket."' class='button warning'><span class='mif-creative-commons'></span></a><a href='".base_url()."superadmin/tiket_baru/reply/".$h->id_tiket."' class='button warning'><span class='mif-bubbles'></span></a><a href='".base_url()."superadmin/tiket_baru/hapus/".$h->id_tiket."' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
		</tr>";
		$i++;
	}
	$hasil .= '</table>';
	$hasil .= $this->pagination->create_links();
	return $hasil;
}

public function generate_index_tiket_baru_sms($limit,$offset)
{
	$hasil="";
	$where['st'] = '0';
	$tot_hal = $this->db->query("SELECT * FROM tempinbox as t left join sam_tiket_cc_sms as c on id = id_sms where Class=-1");

	$config['base_url'] = base_url() . 'superadmin/tiket_baru_sms/index/';
	$config['total_rows'] = $tot_hal->num_rows();
	$config['per_page'] = $limit;
	$config['uri_segment'] = 4;
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$this->pagination->initialize($config);

	$w = $this->db->query("SELECT * FROM tempinbox as t left join sam_tiket_cc_sms as c on id = id_sms where Class=-1 order by ID DESC LIMIT ".$offset.",".$limit."");

	$hasil .= $this->pagination->create_links();
	$hasil .= "<table class='table border bordered sortable-markers-on-left'>
	<thead>
		<tr>
			<th class='sortable-column'>ID SMS</th>
			<th class='sortable-column'>Tgl Masuk</th>
			<th class='sortable-column'>Pengirim</th>
			<th class='sortable-column'>Isi Tiket</th>
			<th class='sortable-column'>Tanggal Pengiriman</th>
			<th class='sortable-column'>OPD</th>
			<th style='width: 70px'>Aksi</th>
		</tr>
	</thead>";
	$i = $offset+1;
	foreach($w->result() as $h)
	{
		if($h->tanggal_pengiriman == ""){
			$data = "Tiket belum terkirim ke OPD";
		}else{
			$data = $h->tanggal_pengiriman;
		} 
		$hasil .= "<tr>
		<td>#".$h->ID."</td>
		<td>".$h->ReceivingDateTime."</td>
		<td>".$h->SenderNumber."</td>
		<td>".$h->TextDecoded."</td>
		<td>".$data."</td><td>";
		$cc = $this->db->query("SELECT * FROM sam_tiket_cc_sms a left join tempinbox b on a.id_sms=b.ID left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_sms=".$h->ID."");

		foreach($cc->result() as $c)
		{
			$hasil .= "<p><span class='tag info'>".$c->nama_skpd."</span></p>";
		}		
		$hasil .= "</td><td>";
		$hasil .= "<a href='".base_url()."superadmin/tiket_baru_sms/ccsms/".$h->ID."' class='button warning' data-role='hint' data-hint-background='bg-orange' data-hint-color='fg-white' data-hint-mode='2' data-hint-position='left' data-hint='CC SMS|Klik untuk cc SMS ke SKPD'><span class='mif-creative-commons'></span></a><a href='".base_url()."superadmin/tiket_baru_sms/reply/".$h->ID."' class='button warning'><span class='mif-bubbles'></span></a><a href='".base_url()."superadmin/tiket_baru_sms/editsms/".$h->ID."' class='button bg-emerald fg-white' data-role='hint' data-hint-background='bg-emerald' data-hint-color='fg-white' data-hint-mode='2' data-hint-position='left' data-hint='EDIT SMS|Klik untuk edit SMS'><span class='mif-spell-check'></span></a><a href='".base_url()."superadmin/tiket_baru_sms/hapus/".$h->ID."' onClick=\"return confirm('Anda Yakin Ingin Menghapus SMS?');\" class='button alert' data-role='hint' data-hint-background='bg-red' data-hint-color='fg-white' data-hint-mode='2' data-hint-position='left' data-hint='HAPUS SMS|Klik untuk menghapus SMS'><span class='mif-bin'></span></a></td>
	</tr>";
	$i++;
}
$hasil .= '</table>';
$hasil .= $this->pagination->create_links();
return $hasil;
}

public function generate_index_tiket_terjawab()
{
	$hasil="";
		// $where['st'] = '0';
		// $tot_hal = $this->db->get("sam_tiket");

		// $config['base_url'] = base_url() . 'superadmin/tiket_terjawab/index/';
		// $config['total_rows'] = $tot_hal->num_rows();
		// $config['per_page'] = $limit;
		// $config['uri_segment'] = 4;
		// $config['first_link'] = 'First';
		// $config['last_link'] = 'Last';
		// $config['next_link'] = 'Next';
		// $config['prev_link'] = 'Prev';
		// $this->pagination->initialize($config);

	$w = $this->db->query("SELECT * from sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_jenis c on b.id_jenis=c.id_jenis left join sam_kategori d on b.id_kategori=d.id_kategori where b.st='0'order by a.id_tiket  DESC");
	$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='true'>
	<thead>
		<tr>
			<th>No</th>
			<th>ID Tiket</th>
			<th>Waktu Pengaduan</th>
			<th>Jenis</th>
			<th>Kategori</th>
			<th>Isi Tiket</th>
			<th>Tanggapan OPD</th>
			<th style='width: 70px'>Aksi</th>
		</tr>
	</thead>";
	$i = 1;
	foreach($w->result() as $h)
	{
		$hasil .= "<tr>
		<td>".$i."</td>
		<td>#".$h->id_tiket."</td>
		<td>".generate_tanggal($h->tanggal)."</td>
		<td>".$h->jenis."</td>
		<td>".$h->kategori."</td>
		<td>".$h->tiket."</td><td>";
		$cc = $this->db->query("SELECT * FROM sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_tiket=".$h->id_tiket."");
		foreach($cc->result() as $c)
		{
			$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
		}		
		$hasil .= "</td><td>";
		$hasil .= "<span data-role='hint' data-hint-background='bg-orange' data-hint-color='fg-white' data-hint-mode='2' data-hint='Info|Klik untuk tutup tiket' data-hint-position='left'><a href='".base_url()."superadmin/tiket_terjawab/closed/".$h->id_tiket."' class='button warning'><span class='mif-lock'></span></a></span></td>
	</tr>";
	$i++;
}
$hasil .= '</table>';
		// $hasil .= $this->pagination->create_links();
return $hasil;
}

public function generate_index_tiket_terjawab_sms()
{
	$hasil="";
		// $where['st'] = '0';
		// $tot_hal = $this->db->get("sam_tiket");

		// $config['base_url'] = base_url() . 'superadmin/tiket_terjawab/index/';
		// $config['total_rows'] = $tot_hal->num_rows();
		// $config['per_page'] = $limit;
		// $config['uri_segment'] = 4;
		// $config['first_link'] = 'First';
		// $config['last_link'] = 'Last';
		// $config['next_link'] = 'Next';
		// $config['prev_link'] = 'Prev';
		// $this->pagination->initialize($config);

	$w = $this->db->query("SELECT * from sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID where b.Class=-1 order by a.id_sms  DESC ");
	$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='true'>
	<thead>
		<tr>
			<th>No</th>
			<th>ID SMS</th>
			<th>Waktu SMS</th>
			<th>Isi SMS</th>
			<th>Tanggapan OPD</th>
			<th style='width: 70px'>Aksi</th>
		</tr>
	</thead>";
	$i = 1;
	foreach($w->result() as $h)
	{

		$hasil .= "<tr>
		<td>".$i."</td>
		<td>#".$h->id_sms."</td>
		<td>".$h->ReceivingDateTime."</td>
		<td>".$h->TextDecoded."</td><td>";
		$cc = $this->db->query("SELECT * FROM sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_sms=".$h->id_sms." and  b.Class=-1");
		foreach($cc->result() as $c)
		{
			if ($c->id_skpd==0){
				$hasil .= "<p><span class='tag info'>(Jawab cepat (superadmin)) : ".$c->tanggapan."</span></p>";	
			} else {
				$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
			}
		}		
		$hasil .= "</td><td>";
		$hasil .= "<span data-role='hint' data-hint-background='bg-orange' data-hint-color='fg-white' data-hint-mode='2' data-hint='Info|Klik untuk tutup tiket' data-hint-position='left'><a href='".base_url()."superadmin/tiket_terjawab_sms/closed/".$h->id_sms."' class='button warning'><span class='mif-lock'></span></a></span></td>
	</tr>";
	$i++;
}
$hasil .= '</table>';
		// $hasil .= $this->pagination->create_links();
return $hasil;
}


public function generate_index_tiket_arsip($tanggal1,$tanggal2,$limit,$offset,$search,$skpd,$kategori)
{
		//echo $search;
	$hasil="";
	$tot_hal = $this->db->query("SELECT * from sam_tiket a left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori where a.tanggal between ".$tanggal1." and ".$tanggal2);
	$config['base_url'] = base_url() . 'superadmin/arsip/index/';
	$config['total_rows'] = $tot_hal->num_rows();
	$config['per_page'] = $limit;
	$config['uri_segment'] = 4;
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$this->pagination->initialize($config);

	if($kategori != "" and $skpd != ""){
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tanggal between ".$tanggal1." and ".$tanggal2." and g.id_skpd = '$skpd' and d.id_kategori = '$kategori'  order by a.id_tiket  DESC");
	}elseif($kategori == "" and $skpd == ""){
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tanggal between ".$tanggal1." and ".$tanggal2." order by a.id_tiket  DESC");
	}else{
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tanggal between ".$tanggal1." and ".$tanggal2." and (g.id_skpd = '$skpd' or d.id_kategori = '$kategori')  order by a.id_tiket  DESC");
	}
	$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='true'>
	<thead>
		<tr>
			<th>No</th>
			<th>ID Tiket</th>
			<th>Waktu Pengaduan</th>
			<th>Diteruskan ke :</th>
			<th>Pada Tanggal  :</th>
			<th>Nama</th>
			<th>Email</th>
			<th>Alamat</th>
			<th>Jenis</th>
			<th>Kategori</th>
			<th>Isi Tiket</th>
			<th>Tanggapan OPD</th>
		</tr>
	</thead>";
	$i = 1;
	foreach($w->result() as $h)
	{
		if($h->tanggal_pengiriman == ""){
			$data = "belum terkirim ke OPD";
		}else{
			$data = $h->tanggal_pengiriman;
		}
		$hasil .= "<tr>
		<td>".$i."</td>
		<td>#".$h->tiket_masuk."</td>
		<td>".generate_tanggal($h->tanggal)."</td>
		<td>".$h->nama_skpd."</td>
		<td>".$data."</td>
		<td>".$h->nama."</td>
		<td>".$h->email."</td>
		<td>".$h->alamat."</td>
		<td>".$h->jenis."</td>
		<td>".$h->kategori."</td>
		<td>".$h->tiket."</td><td>";
		if ($h->tiket_tanggapan==''){
			$hasil .= "<p>Belum ada tanggapan OPD</p>";	
		}
		else {
			$cc = $this->db->query("SELECT * FROM sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_tiket = ".$h->tiket_masuk."");
			foreach($cc->result() as $c)
			{
				$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
			}
		}		
		$hasil .= "</td></tr>";
		$i++;
	}
	$hasil .= '</table>';
		//$hasil .= "";
	return $hasil;
}

public function generate_index_tiket_arsip_sms($tanggal1,$tanggal2,$limit,$offset,$skpd)
{
	$hasil="";

	if($skpd == ""){
	$w = $this->db->query("SELECT * from tempinbox a left join sam_tiket_cc_sms as c on a.id = c.id_sms left join sam_skpd as d on d.id_skpd = c.id_skpd left join sam_tanggapan_sms b on a.ID=b.id_sms where a.ReceivingDateTime between '".$tanggal1."' and '".$tanggal2."' order by a.ID  DESC");
	}else{
	$w = $this->db->query("SELECT * from tempinbox a left join sam_tiket_cc_sms as c on a.id = c.id_sms left join sam_skpd as d on d.id_skpd = c.id_skpd left join sam_tanggapan_sms b on a.ID=b.id_sms where d.id_skpd=$skpd and a.ReceivingDateTime between '".$tanggal1."' and '".$tanggal2."' order by a.ID  DESC");
	}


	$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='true'>
	<thead>
		<tr>
			<th>No</th>
			<th>ID SMS</th>
			<th>Pengirim</th>
			<th>Waktu Kirim</th>
			<th>Diteruskan ke :</th>
			<th>Pada Tanggal  :</th>
			<th>Isi SMS</th>
			<th>Tanggapan SMS</th>
		</tr>
	</thead>";
	$i = 1;
	foreach($w->result() as $h)
	{
		if($h->tanggal_pengiriman == ""){
			$tglp = "Belum terkirim ke OPD";
		}else{
			$tglp = $h->tanggal_pengiriman;
		}
		$hasil .= "<tr>
		<td>".$i."</td>
		<td>#".$h->ID."</td>
		<td>".substr_replace($h->SenderNumber,'xxx',-3)."</td>
		<td>".$h->ReceivingDateTime."</td>
		<td>".$h->nama_skpd."</td>
		<td>".$tglp."</td>
		<td>".$h->TextDecoded."</td><td>";
		$cc = $this->db->query("SELECT * FROM sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_sms=".$h->ID."");
		foreach($cc->result() as $c)
		{
			if ($c->id_skpd==0){
				$hasil .= "<p><span class='tag info'>(Jawab cepat (superadmin)) : ".$c->tanggapan."</span></p>";	
			} else {
				$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
			}
		}		
		$hasil .= "</td></tr>";
		$i++;
	}
	$hasil .= '</table>';

	return $hasil;
}

public function generate_index_tiket_arsip_xls($tanggal1,$tanggal2,$search,$skpd,$kategori)
{
	$hasil="";

	if($kategori != "" and $skpd != ""){
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tiket like '%$search%' and a.tanggal between ".$tanggal1." and ".$tanggal2." and g.id_skpd = '$skpd' and d.id_kategori = '$kategori'  order by a.id_tiket  DESC");
	}elseif($kategori == "" and $skpd == ""){
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tiket like '%$search%' and  a.tanggal between ".$tanggal1." and ".$tanggal2." order by a.id_tiket  DESC");
	}else{
		$w = $this->db->query("SELECT a.id_tiket as tiket_masuk,f.tanggal_pengiriman, a.tanggal, a.tiket, a.id_jenis, a.id_kategori, a.id_user, b.id_tanggapan, b.tanggapan, b.id_tiket as tiket_tanggapan, b.id_skpd, c.id_jenis, c.jenis, d.id_kategori, d.kategori, e.id_user, e.nama, e.email, e.alamat, g.nama_skpd from sam_tiket a left join sam_tiket_cc f on a.id_tiket = f.id_tiket left join sam_skpd as g on g.id_skpd=f.id_skpd left join sam_tanggapan b on a.id_tiket=b.id_tiket left join sam_jenis c on a.id_jenis=c.id_jenis left join sam_kategori d on a.id_kategori=d.id_kategori left join sam_user e on a.id_user=e.id_user where a.tiket like '%$search%' and a.tanggal between ".$tanggal1." and ".$tanggal2." and (g.id_skpd = '$skpd' or d.id_kategori = '$kategori')  order by a.id_tiket  DESC");
	}
	$hasil .= "<table>
	<tr>
		<td></td>
		<td align='center' style='font-size:150%;color:#128023'>Laporan Arsip Tiket Tanggal ".generate_tanggal_query($tanggal1)." - ".generate_tanggal_query($tanggal2)."</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<table cellpadding='8' style='border-collapse:collapse;' border='1'>
				<thead>
					<tr height='40' style='background-color:#128023; color:#fff;'>
						<td>No</td>
						<td>ID Tiket</td>
						<td>Waktu Pengaduan</td>

						<th>Diteruskan ke :</th>
						<th>Pada Tanggal  :</th>
						<td>Nama</td>
						<td>Email</td>
						<td>Alamat</td>
						<td>Jenis</td>
						<td>Kategori</td>
						<td>Isi Tiket</td>
						<td>Petugas</td>
						<td>Tanggapan OPD</td>
					</tr>
				</thead>";
				$i = 1;
				foreach($w->result() as $h)
				{
					if($h->tanggal_pengiriman == ""){
						$data = "belum terkirim ke OPD";
					}else{
						$data = $h->tanggal_pengiriman;
					}
					$hasil .= "<tr height='35'>
					<td>".$i."</td>
					<td>#".$h->tiket_masuk."</td>
					<td>".generate_tanggal($h->tanggal)."</td>
					<td>".$h->nama_skpd."</td>
					<td>".$data."</td>
					<td>".$h->nama."</td>
					<td>".$h->email."</td>
					<td>".$h->alamat."</td>
					<td>".$h->jenis."</td>
					<td>".$h->kategori."</td>
					<td>".$h->tiket."</td>
					<td>1. Drs. Hamzah Setiono<br>2. Laode Muh. Arif M.</td><td>";
					if ($h->tiket_tanggapan==''){
						$hasil .= "<p>Belum ada tanggapan OPD</p>";	
					}
					else {
						$cc = $this->db->query("SELECT * FROM sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_tiket=".$h->tiket_masuk."");
						foreach($cc->result() as $c)
						{
							$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
						}
					}		
					$hasil .= "</td></tr>";
					$i++;
				}
				$hasil .= '</table></td>
			</tr>
		</table>';
		return $hasil;
	}

	public function generate_index_tiket_arsip_sms_xls($tanggal1,$tanggal2,$search,$skpd)
	{
		$hasil="";
		$tanggal11 = urldecode($tanggal1);
		$tanggal22 = urldecode($tanggal2);

		if($skpd == ""){
			$w = $this->db->query("SELECT * from tempinbox a left join sam_tiket_cc_sms as c on a.id = c.id_sms left join sam_skpd as d on d.id_skpd = c.id_skpd left join sam_tanggapan_sms b on a.ID=b.id_sms where a.TextDecoded like '%$search%' and a.ReceivingDateTime between '".$tanggal11."' and '".$tanggal22."' order by a.ID  DESC");
		}else{
			$w = $this->db->query("SELECT * from tempinbox a left join sam_tiket_cc_sms as c on a.id = c.id_sms left join sam_skpd as d on d.id_skpd = c.id_skpd left join sam_tanggapan_sms b on a.ID=b.id_sms where d.id_skpd = $skpd and a.TextDecoded like '%$search%' and a.ReceivingDateTime between '".$tanggal11."' and '".$tanggal22."' order by a.ID  DESC");
		}
		$hasil .= "<table>
		<tr>
			<td></td>
			<td align='center' style='font-size:150%;color:#128023'>Laporan Arsip Tiket SMS Tanggal ".$tanggal11." - ".$tanggal22."</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<table cellpadding='8' style='border-collapse:collapse;' border='1'>
					<thead>
						<tr height='40' style='background-color:#128023; color:#fff;'>
							<td>No</td>
							<td>ID SMS</td>
							<td>Pengirim</td>
							<td>Waktu Kirim</td>
							<td>Diteruskan ke :</td>
							<td>Pada Tanggal  :</td>
							<td>Isi SMS</td>
							<td>Petugas</td>
							<td>Tanggapan SMS</td>
						</tr>
					</thead>";
					$i = 1;
					foreach($w->result() as $h)
					{
						$hasil .= "<tr height='35'>
						<td>".$i."</td>
						<td>#".$h->ID."</td>
						<td>".substr_replace($h->SenderNumber,'xxx',-3)."</td>
						<td>".$h->ReceivingDateTime."</td>
						<td>".$h->nama_skpd."</td>
						<td>".$h->tanggal_pengiriman."</td>
						<td>".$h->TextDecoded."</td>
						<td>1. Drs. Hamzah Setiono<br>2. Laode Muh. Arif M.</td><td>";
						$cc = $this->db->query("SELECT * FROM sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID left join sam_skpd c on a.id_skpd=c.id_skpd where a.id_sms=".$h->ID."");
						foreach($cc->result() as $c)
						{
							
							if ($c->id_skpd==0){
								$hasil .= "<p><span class='tag info'>(Jawab cepat (superadmin)) : ".$c->tanggapan."</span></p>";	
							} else {
								$hasil .= "<p><span class='tag info'>(".$c->nama_skpd.") : ".$c->tanggapan."</span></p>";
							}
						}		
						$hasil .= "</td></tr>";
						$i++;
					}
					$hasil .= '</table></td>
				</tr>
			</table>';
			return $hasil;
		}

		public function generate_index_tiket_baru_skpd($id_skpd,$limit,$offset)
		{
			$hasil="";
			$tot_hal = $this->db->query("SELECT * from sam_tiket_cc a left join sam_tiket b on a.id_tiket=b.id_tiket join sam_user as u on b.id_user=u.id_user left join sam_jenis c on b.id_jenis=c.id_jenis left join sam_kategori d on b.id_kategori=d.id_kategori where a.id_skpd=$id_skpd and a.id_tiket NOT IN (SELECT id_tiket FROM sam_tanggapan)");

			$config['base_url'] = base_url() . 'superadmin/tiket_baru/index/';
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 4;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Prev';
			$this->pagination->initialize($config);

			$w = $this->db->query("SELECT * from sam_tiket_cc a left join sam_tiket b on a.id_tiket=b.id_tiket join sam_user as u on b.id_user=u.id_user left join sam_jenis c on b.id_jenis=c.id_jenis left join sam_kategori d on b.id_kategori=d.id_kategori where a.id_skpd=$id_skpd and a.id_tiket NOT IN (SELECT id_tiket FROM sam_tanggapan) order by a.id_tiket DESC LIMIT ".$offset.",".$limit."");
			$hasil .= "<table class='table border bordered sortable-markers-on-left'>
			<thead>
				<tr>
					<th class='sortable-column'>ID Tiket</th>
					<th class='sortable-column'>Pengirim</th>
					<th class='sortable-column'>Tanggal/Waktu</th>
					<th class='sortable-column'>Email</th>
					<th class='sortable-column'>No.Telp</th>
					<th class='sortable-column'>Jenis</th>
					<th class='sortable-column'>Kategori</th>
					<th class='sortable-column'>Isi Tiket</th>
					<th style='width: 70px'>Aksi</th>
				</tr>
			</thead>";
			$i = $offset+1;
			foreach($w->result() as $h)
			{
				$hasil .= "<tr>
				<td>#".$h->id_tiket."</td>
				<td>".$h->nama."</td>
				<td>".generate_tanggal($h->tanggal)."</td>
				<td>".$h->email."</td>
				<td>".$h->no_telpon."</td>
				<td>".$h->jenis."</td>
				<td>".$h->kategori."</td>
				<td>".$h->tiket."</td>";
				$hasil .= "<td><span data-role='hint' data-hint-background='bg-orange' data-hint-color='fg-white' data-hint-mode='2' data-hint='Info|Klik untuk menjawab' data-hint-position='left'><a href='".base_url()."superadmin/tiket_baru/jawab/".$h->id_tiket."' class='button warning'><span class='mif-bubbles'></span></a></span></td>
			</tr>";
			$i++;
		}
		$hasil .= '</table>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_index_tiket_baru_sms_skpd($id_skpd,$limit,$offset)
	{
		$hasil="";
		$tot_hal = $this->db->query("SELECT * FROM sam_tiket_cc_sms a left join tempinbox b on a.id_sms=b.ID where a.id_skpd=".$id_skpd."");

		$config['base_url'] = base_url() . 'superadmin/tiket_baru_sms/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->query("SELECT * from sam_tiket_cc_sms a left join tempinbox b on a.id_sms=b.ID where a.id_skpd=".$id_skpd." and a.id_sms NOT IN (SELECT id_sms FROM sam_tanggapan_sms) order by a.id_sms DESC LIMIT ".$offset.",".$limit."");
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
		<thead>
			<tr>
				<th class='sortable-column'>ID SMS</th>
				<th class='sortable-column'>Pengirim</th>
				<th class='sortable-column'>Tanggal/Waktu</th>
				<th class='sortable-column'>Isi SMS</th>
				<th style='width: 70px'>Aksi</th>
			</tr>
		</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
			<td>#".$h->ID."</td>		
			<td>#".$h->SenderNumber."</td>
			<td>".$h->ReceivingDateTime."</td>
			<td>".$h->TextDecoded."</td>";
			$hasil .= "<td><span data-role='hint' data-hint-background='bg-orange' data-hint-color='fg-white' data-hint-mode='2' data-hint='Info|Klik untuk menjawab' data-hint-position='left'><a href='".base_url()."superadmin/tiket_baru_sms/jawab/".$h->id_sms."' class='button warning'><span class='mif-bubbles'></span></a></span></td>
		</tr>";
		$i++;
	}
	$hasil .= '</table>';
	$hasil .= $this->pagination->create_links();
	return $hasil;
}

public function generate_index_tiket_terjawab_skpd($id_skpd,$limit,$offset)
{
	$hasil="";
	$tot_hal = $this->db->query("SELECT * FROM sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket where a.id_skpd=".$id_skpd."");

	$config['base_url'] = base_url() . 'superadmin/tiket_terjawab/index/';
	$config['total_rows'] = $tot_hal->num_rows();
	$config['per_page'] = $limit;
	$config['uri_segment'] = 4;
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$this->pagination->initialize($config);

	$w = $this->db->query("SELECT * from sam_tanggapan a left join sam_tiket b on a.id_tiket=b.id_tiket left join sam_jenis c on b.id_jenis=c.id_jenis left join sam_kategori d on b.id_kategori=d.id_kategori where a.id_skpd=".$id_skpd." order by a.id_tiket DESC LIMIT ".$offset.",".$limit."");
	$hasil .= "<table class='table border bordered sortable-markers-on-left'>
	<thead>
		<tr>
			<th class='sortable-column'>ID Tiket</th>
			<th class='sortable-column'>Waktu Pengaduan</th>
			<th class='sortable-column'>Jenis</th>
			<th class='sortable-column'>Kategori</th>
			<th class='sortable-column'>Isi Tiket</th>
			<th style='width: 70px'>Tanggapan</th>
		</tr>
	</thead>";
	$i = $offset+1;
	foreach($w->result() as $h)
	{
		$hasil .= "<tr>
		<td>#".$h->id_tiket."</td>
		<td>".generate_tanggal($h->tanggal)."</td>
		<td>".$h->jenis."</td>
		<td>".$h->kategori."</td>
		<td>".$h->tiket."</td>
		<td>".$h->tanggapan."</td>
	</tr>";
	$i++;
}
$hasil .= '</table>';
$hasil .= $this->pagination->create_links();
return $hasil;
}

public function generate_index_tiket_terjawab_skpd_sms($id_skpd,$limit,$offset)
{
	$hasil="";
	$tot_hal = $this->db->query("SELECT * FROM sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID where a.id_skpd=".$id_skpd."");

	$config['base_url'] = base_url() . 'superadmin/tiket_terjawab_sms/index/';
	$config['total_rows'] = $tot_hal->num_rows();
	$config['per_page'] = $limit;
	$config['uri_segment'] = 4;
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$this->pagination->initialize($config);

	$w = $this->db->query("SELECT * from sam_tanggapan_sms a left join tempinbox b on a.id_sms=b.ID where a.id_skpd=".$id_skpd." order by a.id_sms DESC LIMIT ".$offset.",".$limit."");
	$hasil .= "<table class='table border bordered sortable-markers-on-left'>
	<thead>
		<tr>
			<th class='sortable-column'>ID SMS</th>
			<th class='sortable-column'>Waktu SMS</th>
			<th class='sortable-column'>Isi SMS</th>
			<th style='width: 200px'>Tanggapan</th>
		</tr>
	</thead>";
	$i = $offset+1;
	foreach($w->result() as $h)
	{
		$hasil .= "<tr>
		<td>#".$h->id_sms."</td>
		<td>".$h->ReceivingDateTime."</td>
		<td>".$h->TextDecoded."</td>
		<td>".$h->tanggapan."</td>
	</tr>";
	$i++;
}
$hasil .= '</table>';
$hasil .= $this->pagination->create_links();
return $hasil;
}

public function generate_tiket_cc($id_param)
{
	$query = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN sam_jenis c ON a.id_jenis=c.id_jenis LEFT JOIN sam_kategori d ON a.id_kategori=d.id_kategori where a.id_tiket='".$id_param."' ");
	return $query->result();
}

public function generate_tiket_cc_sms($id_param)
{
	$query = $this->db->query("SELECT * FROM tempinbox where ID='".$id_param."' ");
	return $query->result();
}

public function generate_child_jenis_select($id_jenis)
{
	$hasil = "";
	$w = $this->db->query("SELECT * FROM sam_jenis WHERE id_jenis ='".$id_jenis."'");
	$x = $this->db->query("SELECT * FROM sam_jenis order by id_jenis DESC");
	$hasil .= '<div class="input-control select full-size"><select name="id_jenis">';

	foreach($w->result() as $h)
	{		
		if ($h->id_jenis == $id_jenis) {
			
			$hasil .= '<option value="'.$h->id_jenis.'" selected="selected">'.$h->jenis.'</option>';
		}
	}

	foreach($x->result() as $u)
	{
		$hasil .= '<option value="'.$u->id_jenis.'">'.$u->jenis.'</option>';
	}
	
	$hasil .= '</select></div>';
	return $hasil;
}

public function generate_child_kategori_select($id_kategori)
{
	$hasil = "";
	$w = $this->db->query("SELECT * FROM sam_kategori WHERE id_kategori ='".$id_kategori."'");
	$x = $this->db->query("SELECT * FROM sam_kategori order by id_kategori DESC");
	$hasil .= '<div class="input-control select full-size"><select name="id_kategori">';

	foreach($w->result() as $h)
	{		
		if ($h->id_kategori == $id_kategori) {
			
			$hasil .= '<option value="'.$h->id_kategori.'" selected="selected">'.$h->kategori.'</option>';
		}
	}

	foreach($x->result() as $u)
	{
		$hasil .= '<option value="'.$u->id_kategori.'">'.$u->kategori.'</option>';
	}
	
	$hasil .= '</select></div>';
	return $hasil;
}

public function generate_skpd_cc($id_tiket)
{
	$hasil = "";
	$w = $this->db->query("SELECT * FROM sam_tiket_cc a left join sam_skpd b on a.id_skpd=b.id_skpd WHERE id_tiket ='".$id_tiket."'");
	$hasil .= '<ol class="numeric-list square-marker large-bullet">';
	if(($w->num_rows())>0) {
		foreach($w->result() as $h)
		{		
			$hasil .= '<li>'.$h->nama_skpd.' <a href="'.base_url().'superadmin/tiket_baru/deleteForwardSKPD/'.$h->id_tiket_cc.'/'.$id_tiket.'"><span class="mif-cross mif-1x" style="color: red;"></span></a>
			</li>';
		}
	}
	else {
		$hasil .= '<li>Tiket belum di Forward ke OPD</li>';
	}		
	$hasil .= '</ol>';
	return $hasil;
}

public function generate_skpd_cc_sms($id_tiket)
{
	$hasil = "";
	$w = $this->db->query("SELECT * FROM sam_tiket_cc_sms a left join sam_skpd b on a.id_skpd=b.id_skpd left join sam_admin c on a.id_skpd =c.id_skpd WHERE a.id_sms ='".$id_tiket."'");
	$hasil .= '<ol class="numeric-list square-marker large-bullet">';
	if(($w->num_rows())>0) {
		foreach($w->result() as $h)
		{		
			$hasil .= '<li>'.$h->nama_skpd.' - '.$h->no_hp.' <a href="'.base_url().'superadmin/tiket_baru/deleteForwardSMSSKPD/'.$h->id_sms_cc.'/'.$h->id_sms.'"><span class="mif-cross mif-1x" style="color: red;"></span></a></li>';
		}
	}
	else {
		$hasil .= '<li>Tiket belum di Forward ke OPD</li>';
	}		
	$hasil .= '</ol>';
	return $hasil;
}

public function count_tiket_superadmin()
{

	$query = $this->db->query("SELECT * FROM sam_tiket a left join sam_jenis b on a.id_jenis=b.id_jenis left join sam_kategori c on a.id_kategori=c.id_kategori where a.st='0'");
	return $query->num_rows(); 
}

public function count_tiket_skpd($id_skpd)
{

	$query = $this->db->query("SELECT a.id_tiket FROM sam_tiket_cc a left join sam_tiket b on a.id_tiket = b.id_tiket WHERE a.id_tiket NOT IN (SELECT c.id_tiket FROM sam_tanggapan c) and a.id_skpd=".$id_skpd."");
	return $query->num_rows();
}

public function count_surat_keterangan($id_lokasi)
{

	$this->db->where('id_lokasi',$id_lokasi);
	$this->db->where('st =',0);
	$this->db->from('kel_surat_keterangan');
	$hasil = $this->db->count_all_results();
	return $hasil;
}

public function generate_indexs_surat_keterangan($limit,$offset,$fill,$id_lokasi)
{
	$hasil = "";

	$page=$offset;
	if(!$page):
		$offset = 0;
	else:
		$offset = $page;
	endif;
	
	$filter['st'] = "";
	$set_fill = "";
	if($fill==2)
	{
		$set_fill = "";
		$filter['st'] = "";
	}
	else
	{
		$set_fill = "a.st='".$fill."' and ";
		$filter['st'] = $fill;
	}
	
	$tot_hal = $this->db->query("SELECT * FROM kel_surat_keterangan a where ".$set_fill." a.id_lokasi = '".$id_lokasi."'");
	$config['base_url'] = base_url() . 'superadmin/surat_keterangan/index/';
	$config['total_rows'] = $tot_hal->num_rows();
	$config['per_page'] = $limit;
	$config['uri_segment'] = 4;
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$this->pagination->initialize($config);
	
	$w = $this->db->query("SELECT * FROM kel_surat_keterangan a left join kel_user b on a.id_user=b.id_user where ".$set_fill." a.id_lokasi = '".$id_lokasi."' order by id_surat_ket DESC LIMIT ".$offset.",".$limit."");
	$i = 0;
	foreach($w->result() as $h)
	{	
		$gbr = "no-image.png";
		if($h->gambar!="")
		{
			$gbr = $h->gambar;
		}
		if($i==0)
		{
			$hasil .= '<div class="row no-margin">';
		}
		$approve = "<i class='icon-thumbs-up bg-red'></i> Approve";
		$upd_approve = "1";
		if($h->st==1)
		{
			$approve = "<i class='icon-thumbs-down bg-red'></i> Unapprove";
			$upd_approve = "0";
		}
		
		$hasil .= '<div class="span3">
		<p>
			<a href="'.base_url().'superadmin/surat_keterangan/hapus/'.$h->id_surat_ket.'" class="image-button danger fg-white image-left" onClick=\'return confirm("Anda Yakin ingin Menghapus?");\'><i class="icon-remove bg-red"></i> Hapus</a>
			<a href="'.base_url().'superadmin/surat_keterangan/approve/'.$h->id_surat_ket.'/'.$upd_approve.'" class="image-button bg-amber fg-white image-left">'.$approve.'</a>
		</p>
		<img src="'.base_url().'asset/images/member/thumb/'.$gbr.'"  class="img" width="320" style="border: 1px #eaeaea solid">
		<p><blockquote>
			<p>Tanggal Berkas Masuk</p>
			<small><cite title="tanggal">'.generate_tanggal($h->tanggal).'</cite></small>
		</blockquote></p>
		<p><blockquote>NIK</blockquote></p><p>'.$h->nik.'</p>
		<p><blockquote>Nama</blockquote></p><p>'.$h->nama.'</p>
		<p><blockquote>Memasukkan keperluan untuk</blockquote></p><p>'.$h->keterangan.'</p>
		<p><blockquote>File Upload</blockquote></p><p><a class="image-button bg-lightBlue fg-white" href="'.base_url().'asset/file/'.$h->file.'" target="_blank">Download <i class="icon-download-2 bg-darkIndigo"></i></a></p>
		<p><blockquote>Berkas Upload tidak lengkap?</blockquote></p>
		<a href="'.base_url().'superadmin/surat_keterangan/komentar/'.$h->id_surat_ket.'" class="button">Klik Untuk Komentar</a>
	</div>';
	$i++;
	if($i>3)
	{
		$i=0;
	}
}
$hasil .= '</div><div class="row">';
$hasil .= $this->pagination->create_links();
$hasil .= '</div>';
return $hasil;
}
}
