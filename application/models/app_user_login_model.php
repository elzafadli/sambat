<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class app_user_login_model extends CI_Model {

/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	public function getEmail($data){
		$query = $this->db->query("SELECT * FROM sam_user where email = '$data'");

		return $query;
		
		$query->free_result();
	}

	public function getStatus($data){
		$query = $this->db->query("SELECT * FROM sam_user where email = '$data' and stts = 1");

		return $query;
		
		$query->free_result();
	}

	public function cekUserLogin($data)
	{
		$cek['username'] 	= mysql_real_escape_string($data['username']);
		$cek['password'] 	= $data['password'];
		
		$q_cek_login = $this->db->get_where('sam_user', $cek);
		if($q_cek_login->num_rows()>0)
		{
			foreach($q_cek_login->result() as $qad)
			{
				$sess_data['logged_in'] = 'yesGetMeLoginBaby';
				$sess_data['nama'] = $qad->nama;
				$sess_data['id_user'] = $qad->id_user;
				$sess_data['email'] = $qad->email;
				$sess_data['alamat'] = $qad->alamat;
				$sess_data['profesi'] = $qad->profesi;
				$sess_data['no_telpon'] = $qad->no_telpon;
				$sess_data['username'] = $qad->username;
				
				$this->session->set_userdata($sess_data);
			}
			redirect("user/dashboard");
		}
		else
		{
			$this->session->set_flashdata("result","Gagal Login. Username dan Password Tidak Cocok....");
			redirect("web/login");
		}
	}
	 
	public function cekAdminLogin($data)
	{
		$cek['username'] 		= mysql_real_escape_string($data['username']);
		$cek['password'] 	= $data['password'];
		
		$q_cek_login = $this->db->get_where('sam_admin', $cek);
		if($q_cek_login->num_rows()>0)
		{
			foreach($q_cek_login->result() as $qad)
			{
				$sess_data['logged_in_admin'] = 'yesGetMeLoginBaby';
				$sess_data['nama'] = $qad->nama;
				$sess_data['id_admin'] = $qad->id_admin;
				$sess_data['username'] = $qad->username;
				$sess_data['id_skpd'] = $qad->id_skpd;

				$this->session->set_userdata($sess_data);
			}
			redirect("superadmin/dashboard");
		}
		else
		{
			$this->session->set_flashdata("result","Gagal Login. Username dan Password Tidak Cocok....");
			redirect("superadmin");
		}
	}
}

/* End of file app_user_login_model.php */
/* Location: ./application/models/app_user_login_model.php */