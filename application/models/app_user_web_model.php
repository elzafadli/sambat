<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class app_user_web_model extends CI_Model {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/

	/*public function getPesan($id){
		$query = $this->db->query("SELECT * FROM notifikasi as n join sam_tiket as t on n.id_tiket = t.id_tiket where id_user = $id");

		return $query;
		
		$query->free_result();
	}*/

	public function getKategori($id){
		$query = $this->db->query("SELECT * FROM `sam_kategori` where id_kategori= ".$id);

		return $query;
		
		$query->free_result();
	}

	public function getEmail(){
		$query = $this->db->query("SELECT * FROM `sam_setting` where id_setting = 13");

		return $query;
		
		$query->free_result();
	}


	public function getWebTiketPerBulan($jenis,$bulan,$tahun,$limit=null,$offset=null){
		if ($limit == null && $offset == null) {
			$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where FROM_UNIXTIME(tu.tanggal,'%m') = ".$bulan." and FROM_UNIXTIME(tu.tanggal,'%Y') = ".$tahun." and id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC");
		}else{
			$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where FROM_UNIXTIME(tu.tanggal,'%m') = ".$bulan." and FROM_UNIXTIME(tu.tanggal,'%Y') = ".$tahun." and id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC LIMIT ".$limit." OFFSET ".$offset);
		}

		return $query;
		
		$query->free_result();
	}

	public function getAllWebTiket($jenis,$kategori=null,$limit=null,$offset=null){

		if ($kategori != null) {

			if ($limit == null && $offset == null) {
				$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where tu.id_kategori = ".$kategori." and id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC");
			}else{
				$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC LIMIT ".$limit." OFFSET ".$offset);
			}

		}else{
			if ($limit == null && $offset == null) {
				$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC");
			}else{
				$query = $this->db->query("SELECT FROM_UNIXTIME(tu.tanggal) as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, u.nama as pengirim, tu.tiket, t.tanggapan, s.id_skpd, s.nama_skpd FROM sam_tiket_cc as c LEFT JOIN `sam_tanggapan` as t on t.id_tiket = c.id_tiket JOIN sam_skpd as s on c.id_skpd=s.id_skpd join sam_tiket as tu on c.id_tiket=tu.id_tiket join sam_user as u on tu.id_user=u.id_user where id_tanggapan IS ".$jenis." ORDER BY `id_tiket_cc` DESC LIMIT ".$limit." OFFSET ".$offset);
			}
		}

		return $query;
		
		$query->free_result();
	}

	public function getAllSMSTiket($jenis,$limit=null,$offset=null){
		if ($limit == null && $offset == null) {
			$query = $this->db->query("SELECT tb.ReceivingDateTime as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, tb.SenderNumber as pengirim, tb.TextDecoded as tiket, ts.tanggapan, s.id_skpd, s.nama_skpd  FROM `sam_tiket_cc_sms` c left join sam_tanggapan_sms as ts on c.id_sms=ts.id_sms join sam_skpd as s on c.id_skpd=s.id_skpd JOIN tempinbox as tb on tb.id=c.id_sms WHERE ts.id_tanggapan IS ".$jenis." ORDER BY `id_sms_cc` DESC");
		}else{
			$query = $this->db->query("SELECT tb.ReceivingDateTime as tanggal_pengiriman, c.tanggal_pengiriman as tanggal_forward, tb.SenderNumber as pengirim, tb.TextDecoded as tiket, ts.tanggapan, s.id_skpd, s.nama_skpd  FROM `sam_tiket_cc_sms` c left join sam_tanggapan_sms as ts on c.id_sms=ts.id_sms join sam_skpd as s on c.id_skpd=s.id_skpd JOIN tempinbox as tb on tb.id=c.id_sms WHERE ts.id_tanggapan IS ".$jenis." ORDER BY `id_sms_cc` DESC LIMIT ".$limit." OFFSET ".$offset);
		}

		return $query;
		
		$query->free_result();
	}

	public function getLastSend($limit,$id){
		$query = $this->db->query('SELECT from_unixtime(tanggal,"%m/%d/%Y %h:%i:%s") as tanggal, from_unixtime(tanggal,"%i") as waktu ,tiket,id_user,id_tiket FROM `sam_tiket` as t where id_user = '.$id.' ORDER BY `id_tiket` DESC limit '.$limit);

		return $query;
		
		$query->free_result();
	}

	public function deleteSpam($id){

		$this->db->where('id_tiket', $id);
		$this->db->delete('sam_tiket');
	}

	public function getTiket(){
		$query = $this->db->query("SELECT tiket, nama as pengirim, FROM_UNIXTIME(tanggal), k.id_kategori, k.kategori FROM `sam_tiket` as t join sam_kategori as k on t.id_kategori = k.id_kategori join sam_user as u on t.id_user = u.id_user");

		return $query;
		
		$query->free_result();
	}

	public function getTiketSMS(){
		$query = $this->db->query("SELECT TextDecoded, SenderNumber, ReceivingDateTime FROM `tempinbox` order by id desc");

		return $query;
		
		$query->free_result();
	}

	public function getKategoriTiket(){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah, k.kategori FROM `sam_tiket` as t JOIN sam_kategori as k on t.id_kategori=k.id_kategori GROUP by k.id_kategori,k.kategori");

		return $query;
		
		$query->free_result();
	}

	public function getBulanTiketSMS(){
		$query = $this->db->query("SELECT COUNT(id) as jumlah,month(ReceivingDateTime) as bulan, YEAR(ReceivingDateTime) as tahun FROM `tempinbox` GROUP by YEAR(ReceivingDateTime),month(ReceivingDateTime)");

		return $query;
		
		$query->free_result();
	}

	public function getBulanTiketWeb(){
		$query = $this->db->query("SELECT COUNT(id_tiket) as jumlah,FROM_UNIXTIME(tanggal , '%m') as bulan, FROM_UNIXTIME(tanggal , '%Y') as tahun FROM sam_tiket GROUP by FROM_UNIXTIME(tanggal , '%Y'),FROM_UNIXTIME(tanggal , '%m')");

		return $query;
		
		$query->free_result();
	}


	public function generate_captcha()
	{
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '150',
			'img_height' => 40
			);
		$cap = create_captcha($vals);
		$datamasuk = array(
			'captcha_time' => $cap['time'],
			//'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
			);
		$expiration = time()-3600;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$query = $this->db->insert_string('captcha', $datamasuk);
		$this->db->query($query);
		return $cap['image'];
	}
	
	public function generate_detail_member($id_param,$offset)
	{
		$hasil = "";
		$where['id_user'] = $id_param;
		$w = $this->db->get_where("sam_user",$where);
		foreach($w->result() as $h)
		{
			$hasil .= '<div class="span9 bg-white padding10">
							<div class="panel" data-role="panel">
    							<div class="panel-header bg-amber fg-white">
       								Informasi '.$h->nama.' 
    							</div>
    							<div class="panel-content">
    								<div class="row">
    									<div class="span5">
       										<p>Nama : '.$h->nama.'</p>
       										<p>Email : '.$h->email.'</p>
       										<p>Alamat : '.$h->alamat.'</p>
       										<p>No Telpon / HP : '.$h->no_telpon.'</p>
       									</div>
       								</div>
    							</div>
							</div>
							<div class="row" style="border: 1px #eaeaea solid">
							<h2 class="bg-lightBlue fg-white no-margin padding10">Silakan pilih pengurusan yang diinginkan :</h2>
							<div class="padding10">
							<a href="'.base_url().'user/surat_keterangan"><div class="tile selected" data-hint-mode="2" data-hint-position="right" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk">Surat Keterangan / Umum</div></a>
							<a href="'.base_url().'user/surat_pindah"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk 4.KK dan KTP Pengikut bila ada 5.Foto 4x6 berwarna">Surat Pindah</div></a>
							<a href="'.base_url().'user/surat_nikah"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk 4.Akte Kelahiran/Surat Kelahiran 5.Ijazah Terakhir 6.KK Orangtua (apabila beda KK dengan Orangtua) 7.Fotokopi KTP calon suami/istri 8.Fotokopi KK calon suami/istri 9.Foto 3x4 berwarna 10.Foto calon suami/istri 3x4 berwarna">Surat Nikah</div></a>
							<a href="'.base_url().'user/surat_kelahiran"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga Orangtua 3.Kartu Tanda Penduduk Kedua Orangtua 4.Fotokopi Surat Kelahiran dari RSB/Bidan 5.Fotokopi Surat Nikah Orangtua">Surat <br>Kelahiran</div></a>
							<a href="'.base_url().'user/surat_kematian"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga yang meninggal 3.Kartu Tanda ahaPenduduk yang meninggal 4.Surat Kematian dari RS jika meninggal di RS 5.KTP Pelapor atau Ahli waris">Surat <br>Kematian</div></a>
							<a href="'.base_url().'user/surat_kkb"><div class="tile selected" data-hint-mode="2" data-hint-position="left" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk">Surat Keterangan Kelakuan Baik</div></a>
							<a href="'.base_url().'user/surat_keterangan_usaha"><div class="tile selected" data-hint-mode="2" data-hint-position="right" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk">Surat Keterangan Usaha</div></a>
							<a href="'.base_url().'user/ijin_keramaian"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk">Ijin Keramaian</div></a>
							<a href="'.base_url().'user/surat_bepergian"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga 3.Kartu Tanda Penduduk 4.KK atau KTP Pengikut bila ada 5.Foto 4x6 berwarna">Surat Keterangan Bepergian</div></a>
							<a href="'.base_url().'user/surat_kuasa"><div class="tile selected" data-hint-mode="2" data-hint-position="top" data-hint="Syarat|1.Pengantar RT/RW 2.Kartu Keluarga Pemberi Kuasa 3.Kartu Tanda Penduduk Pemberi Kuasa 4.KK Penerima Kuasa 5.KTP Penerima Kuasa">Surat Keterangan Kuasa</div></a>
							</div>
							</div>
							</div>
					</div>';
		}
		$hasil .= '<div class="cleaner_h20"></div>';
		return $hasil;
	}

	public function generate_index_tiket_user($limit,$offset)
	{
		$hasil="";
		$tot_hal = $this->db->get("sam_jenis");

		$config['base_url'] = base_url() . 'user/cektiket/index/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->get("sam_tiket",$limit,$offset);
		
		$hasil .= "<table class='table border bordered sortable-markers-on-left'>
					<thead>
					<tr>
					<th>No.</th>
					<th>Kategori Tiket</th>
					<th style='width: 180px'><a href='".base_url()."superadmin/kategori/tambah' class='image-button primary small-button'><span class='icon mif-folder-plus bg-darkCobalt'></span> Tambah Kategori</a></th>
					</tr>
					</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
					<td>".$i."</td>
					<td>".$h->kategori."</td>
					<td>";
			$hasil .= "<a href='".base_url()."superadmin/kategori/edit/".$h->id_kategori."' class='button warning'><span class='mif-pencil'></span></a> <a href='".base_url()."superadmin/kategori/hapus/".$h->id_kategori."' class='button danger' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\"><span class='mif-bin'></span></a></td>
					</tr>";
			$i++;
		}
		$hasil .= '</table>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_detail_surat_keterangan($id_param)
	{
		$hasil = "";
		$w = $this->db->query("SELECT a.id_surat_ket, a.id_lokasi, a.id_user, a.keterangan, a.tanggal, a.st, a.file, b.id_user, b.nama, b.nik, c.id_lokasi, c.lokasi FROM kel_surat_keterangan a LEFT JOIN kel_user b ON a.id_user=b.id_user LEFT JOIN kel_lokasi c ON a.id_lokasi=c.id_lokasi WHERE a.id_user='".$id_param."' order by a.id_surat_ket DESC");
		foreach($w->result() as $h)
		{	
			$hasil .= '<div class="span9 bg-white padding10">
							<div class="panel" data-role="panel">
    							<div class="panel-header bg-amber fg-white">
       								DETAIL SURAT KETERANGAN '.$h->nama.' 
    							</div>
    							<div class="panel-content">
    								<p>Terimakasih sudah menggunakan aplikasi kami, berikut detail informasi Anda :
    								<p>Waktu Berkas Masuk : '.generate_tanggal($h->tanggal).'</p>
       								<p>NIK : '.$h->nik.'</p>
       								<p>Nama : '.$h->nama.'</p>
       								<p>Kelurahan : '.$h->lokasi.'</p>
       								<p>Keperluan Pengurusan : '.$h->keterangan.'</p>
       								<p>File Upload Anda : <a class="image-button bg-lightBlue fg-white" href="'.base_url().'asset/file/'.$h->file.'" target="_blank">Download <i class="icon-download-2 bg-darkIndigo"></i></a></p>
       								<p>Periksa kembali isian Anda ? </p>
       								<p><a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/surat_keterangan/edit/'.$h->id_surat_ket.'"><i class="icon-thumbs-down bg-darkIndigo"></i>Edit</a> <a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/dashboard"><i class="icon-thumbs-up bg-darkIndigo"></i>Tidak</a>  
    							</div>
							</div>
						</div>';
		}
		return $hasil;
	}

}

/* End of file app_user_web_model.php */