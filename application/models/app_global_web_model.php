<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class app_global_web_model extends CI_Model {

	/**
	 * @author : Wewaits
	 * @twitter : @wewaits
	 **/
	function getAPI($limit=null){
		if($limit != null){
			$query = $this->db->query("SELECT a.tiket, u.nama, a.tanggal, s.nama_skpd, t.tanggapan FROM sam_tanggapan as t join sam_tiket as a on t.id_tiket=a.id_tiket join sam_user as u on a.id_user=u.id_user join sam_skpd as s on t.id_skpd = s.id_skpd ORDER by a.id_tiket desc LIMIT $limit");
		}else{
			$query = $this->db->query("SELECT a.tiket, u.nama, a.tanggal, s.nama_skpd, t.tanggapan FROM sam_tanggapan as t join sam_tiket as a on t.id_tiket=a.id_tiket join sam_user as u on a.id_user=u.id_user join sam_skpd as s on t.id_skpd = s.id_skpd ORDER by a.id_tiket desc");
		}
		

		return $query->result();

		$query->free_result();
	}
	public function get_pesan($where){
		if($where)
			$this->db->where($where);

		$this->db->select('*');
		$this->db->from('notifikasi as n');
		$this->db->join('sam_admin as a',"n.id_admin = a.id_admin");
		$this->db->order_by('id_notifikasi', 'DESC');
		$query = $this->db->get();

		return $query;
		
		$query->free_result();
	}

	public function update_pesan($where){
		if($where)
			$this->db->where($where);

		$this->db->set('readStatus', '0');
		$this->db->update('notifikasi');
	}

	public function generate_captcha()
	{
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url().'captcha/',
			'font_path' => './system/fonts/impact.ttf',
			'img_width' => '150',
			'img_height' => 40
			);
		$cap = create_captcha($vals);
		$datamasuk = array(
			'captcha_time' => $cap['time'],
			//'ip_address' => $this->input->ip_address(),
			'word' => $cap['word']
			);
		$expiration = time()-3600;
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		$query = $this->db->insert_string('captcha', $datamasuk);
		$this->db->query($query);
		return $cap['image'];
	}


	public function generate_parent()
	{

        $query = $this->db->query("SELECT * FROM sam_skpd WHERE id_parent='0' order by id_skpd DESC");
        return $query->result();

	}

	public function generate_skpd($parent_id='')
	{
		$this->db->select('sam_skpd.*');
        $this->db->where('id_parent', $parent_id);
        $query = $this->db->get('sam_skpd');
        return $query->result();
	}

	public function generate_group()
	{
		$this->db->select('pbk_groups.*');
        $query = $this->db->get('pbk_groups');
        return $query->result();
	}

	public function generate_phonebook()
	{
		$this->db->select('pbk.*');
        $query = $this->db->get('pbk');
        return $query->result();
	}

	public function generate_child_skpd()
	{
		$hasil = "";
		$w = $this->db->query("SELECT * FROM sam_skpd WHERE id_parent !='0' order by id_skpd DESC");
		$hasil .= '<select class="input-control select" name="id_skpd">';
		$hasil .= '<option value="" selected="selected">Pilih SKPD</option>';
		$hasil .= '<option value="0">superadmin</option>';
		foreach($w->result() as $h)
		{		
			$hasil .= '<option value="'.$h->id_skpd.'">'.$h->nama_skpd.'</option>';		
		}
		$hasil .= '</select>';
		return $hasil;
	}

	public function generate_child_skpd_select($id_skpd)
	{
		$hasil = "";
		$w = $this->db->query("SELECT * FROM sam_skpd WHERE id_skpd ='".$id_skpd."'");
		$hasil .= '<div class="input-control select full-size"><select name="id_skpd">';
		if(($w->num_rows())>0) {
		foreach($w->result() as $h)
		{		
			if ($h->id_skpd == $id_skpd) {
				
				$hasil .= '<option value="'.$h->id_skpd.'" selected="selected">'.$h->nama_skpd.'</option>';
				$k = $this->db->query("SELECT * FROM sam_skpd WHERE id_parent !='0' order by id_skpd DESC");
				foreach($k->result() as $j)
				{
					$hasil .= '<option value="'.$j->id_skpd.'">'.$j->nama_skpd.'</option>';
				}
			}		
		}
		}
		
			else {
				$hasil .= '<option value="0" selected="selected">superadmin</option>';
				$k = $this->db->query("SELECT * FROM sam_skpd WHERE id_parent !='0' order by id_skpd DESC");
				foreach($k->result() as $j)
				{
					$hasil .= '<option value="'.$j->id_skpd.'">'.$j->nama_skpd.'</option>';
				}
			}		
		$hasil .= '</select></div></div></div><div class="row flex-just-sb">
                                <div class="cell colspan5">
                                </div><div class="cell colspan5">
                                </div></div>';
		return $hasil;
	}

	public function generate_jenis_tiket()
	{

        $query = $this->db->query("SELECT * FROM sam_jenis order by id_jenis DESC");
        return $query->result();

	}

	public function generate_kategori_tiket()
	{

        $query = $this->db->query("SELECT * FROM sam_kategori order by id_kategori DESC");
        return $query->result();

	}

	public function generate_index_tiket_user($id_param,$limit,$offset)
	{
		$hasil="";
		$where['id_user'] = $id_param;
		$tot_hal = $this->db->get_where("sam_tiket",$where);

		$config['base_url'] = base_url() . 'user/ceksurat/index/'.$id_param.'/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);

		$w = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_kategori b ON a.id_kategori=b.id_kategori LEFT JOIN sam_jenis c ON a.id_jenis=c.id_jenis where a.id_user='".$id_param."' order by a.id_tiket DESC");
		
		$hasil .= "<table class='dataTable border bordered' data-role='datatable' data-auto-width='false'>
					<thead>
					<tr>
					<th>ID</th>
					<th>Tanggal - Waktu</th>
					<th>Jenis</th>
					<th>Kategori</th>
					<th>Isi Tiket</th>
					<th>Status</th>
					<th>Keterangan</th>
					</tr>
					</thead>";
		$i = $offset+1;
		foreach($w->result() as $h)
		{
			$hasil .= "<tr>
					<td>#".$h->id_tiket."</td>
					<td>".generate_tanggal($h->tanggal)."</td>
					<td>".$h->jenis."</td>
					<td>".$h->kategori."</td>
					<td>".$h->tiket."</td>";
					if ($h->st==0) {
			$hasil .= "<td>Open</td>
						<td>Belum ada tanggapan</td>";			
					} else {
			$hasil .= "<td>Closed</td>
						<td><a href='".base_url()."web/detailtiket/".$h->id_tiket."' class='button success'>Lihat Tanggapan</a></td>";		
					}
			$hasil .= "</tr>";
			$i++;
		}
		$hasil .= '</table>';
		return $hasil;
	}

	public function generate_tiket_gambar()
	{

        $query = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN `sam_tanggapan` as t on a.id_tiket = t.id_tiket join sam_skpd as s on t.id_skpd = s.id_skpd where a.file != 0 order by a.id_tiket DESC LIMIT 3");
        return $query->result();

	}

	public function generate_tiket_teks()
	{

        $query = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN `sam_tanggapan` as t on a.id_tiket = t.id_tiket join sam_skpd as s on t.id_skpd = s.id_skpd where a.file =0 order by a.id_tiket DESC LIMIT 6");
        return $query->result();

	}

	public function generate_tiket_sms()
	{

        $query = $this->db->query("SELECT ID, ReceivingDateTime, SenderNumber, SUBSTRING(TextDecoded,8) as TextDecoded, ts.*,s.nama_skpd FROM tempinbox as t LEFT JOIN sam_tanggapan_sms as ts on t.ID = ts.id_sms join sam_skpd as s on ts.id_skpd = s.id_skpd where TextDecoded REGEXP 'sambat' order by ID DESC LIMIT 6");
        return $query->result();

	}

	public function generate_tiket_selengkapnya($limit,$offset)
	{
		$hasil="";
		$page=$offset;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;
        $tot_hal = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN `sam_tanggapan` as t on a.id_tiket = t.id_tiket join sam_skpd as s on t.id_skpd = s.id_skpd where a.file =0");
        $config['base_url'] = base_url() . 'web/tiket_selengkapnya/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);


        $w = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN `sam_tanggapan` as t on a.id_tiket = t.id_tiket join sam_skpd as s on t.id_skpd = s.id_skpd where a.file =0 order by a.id_tiket DESC LIMIT ".$offset.",".$limit."");
        $hasil .= '<div class="content padding10">';
        $i = $offset+1;
        foreach($w->result() as $s)
        {
        $hasil .= '<blockquote>
        			Isi Tiket :
                        <hr>
        			<a href="'.base_url().'web/detailtiket/'.$s->id_tiket.'">
        				<cite title="Klik untuk detail">ID Sambat : '.$s->id_tiket.'</cite>
        				<p>'. character_limiter($s->tiket, 340).'</p>
                        <small>Pengirim : '.$s->nama.' <cite title="Source Title">'.generate_tanggal($s->tanggal).'</cite></small>
                    </a>
                    <br><br>
                        Tanggapan :
                         <hr>
                        <a href="'.base_url().'web/detailtiketsms/'.$s->id_tiket.'">
                        <p>'.character_limiter($s->tanggapan, 340).'</p>
                        <small>Dari : '.$s->nama_skpd.'</small>
                        </a>
                    </blockquote>';
        $i++;                           
        }
        $hasil .= '</div>';
        $hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_sms_selengkapnya($limit,$offset)
	{
		$hasil="";
		$page=$offset;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;
        $tot_hal = $this->db->query("SELECT ID, ReceivingDateTime, SenderNumber, SUBSTRING(TextDecoded,8) as TextDecoded, ts.*,s.nama_skpd FROM tempinbox as t LEFT JOIN sam_tanggapan_sms as ts on t.ID = ts.id_sms join sam_skpd as s on ts.id_skpd = s.id_skpd where TextDecoded REGEXP 'sambat'");
        $config['base_url'] = base_url() . 'web/sms_selengkapnya/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);


        $w = $this->db->query("SELECT ID, ReceivingDateTime, SenderNumber, SUBSTRING(TextDecoded,8) as TextDecoded, ts.*,s.nama_skpd FROM tempinbox as t LEFT JOIN sam_tanggapan_sms as ts on t.ID = ts.id_sms join sam_skpd as s on ts.id_skpd = s.id_skpd where TextDecoded REGEXP 'sambat' order by ID DESC LIMIT ".$offset.",".$limit."");
        $hasil .= '<div class="content padding10">';
        $i = $offset+1;
        foreach($w->result() as $s)
        {
        $hasil .= '<blockquote>
       				 	Isi Tiket :
                     	<hr>
        			<a href="'.base_url().'web/detailtiketsms/'.$s->ID.'">
        				<cite title="Klik untuk detail">ID Sambat : '.$s->ID.'</cite>
        				<p>'. character_limiter($s->TextDecoded, 340).'</p>
                        <small>Pengirim : '.substr_replace($s->SenderNumber,'xxx',-3).' <cite title="Source Title">'.$s->ReceivingDateTime.'</cite></small>
                    </a>
                    <br><br>
                        Tanggapan :
                        <hr>
        			<a href="'.base_url().'web/detailtiketsms/'.$s->ID.'">
        				<p>'. $s->tanggapan.'</p>
                        <small>Pengirim : '.$s->nama_skpd.'</small>
                    </a></blockquote>';
        $i++;                           
        }
        $hasil .= '</div>';
        $hasil .= $this->pagination->create_links();
		return $hasil;
	}

	public function generate_tiket_detail($id_param)
	{
		$query = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN sam_jenis c ON a.id_jenis=c.id_jenis LEFT JOIN sam_kategori d ON a.id_kategori=d.id_kategori where a.id_tiket='".$id_param."' ");
        return $query->result();
	}

	public function generate_tiket_detail_sms($id_param)
	{
		$query = $this->db->query("SELECT ID, ReceivingDateTime, SenderNumber, SUBSTRING(TextDecoded,8) as TextDecoded FROM tempinbox where TextDecoded REGEXP 'sambat' and ID='".$id_param."' ");
        return $query->result();
	}

	public function generate_tiket_tanggapan($id_param)
	{
		$query = $this->db->query("SELECT * FROM sam_tanggapan a LEFT JOIN sam_tiket b ON a.id_tiket = b.id_tiket LEFT JOIN sam_skpd c ON a.id_skpd=c.id_skpd where a.id_tiket='".$id_param."' ");
        return $query->result();
	}

	public function generate_tiket_tanggapan_sms($id_param)
	{
		$query = $this->db->query("SELECT * FROM sam_tanggapan_sms a LEFT JOIN tempinbox b ON a.id_sms = b.ID LEFT JOIN sam_skpd c ON a.id_skpd=c.id_skpd where a.id_sms='".$id_param."' ");
        return $query->result();
	}

	public function generate_tiket_lacak($id_tiket)
	{
		$query = $this->db->query("SELECT * FROM sam_tiket a LEFT JOIN sam_user b ON a.id_user = b.id_user LEFT JOIN sam_jenis c ON a.id_jenis=c.id_jenis LEFT JOIN sam_kategori d ON a.id_kategori=d.id_kategori where a.id_tiket='".$id_tiket."' ");
        return $query->result();
	}

	public function generate_indexs_surat_keterangan_edit($id_param,$limit,$offset)
	{
		$hasil = "";

		$page=$offset;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;
		
		$where['id_user'] = $id_param;
		$tot_hal = $this->db->get_where("kel_surat_keterangan",$where);
		$config['base_url'] = base_url() . 'user/surat_keterangan/index/'.$id_param.'/';
		$config['total_rows'] = $tot_hal->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$this->pagination->initialize($config);
		
		$w = $this->db->query("SELECT * FROM kel_surat_keterangan a LEFT JOIN kel_lokasi b ON a.id_lokasi=b.id_lokasi where a.id_user='".$id_param."' LIMIT ".$offset.",".$limit."");
		$i = 0;
		$hasil .= '<div class="accordion with-marker" data-role="accordion" data-closeany="true">
						<div class="accordion-frame">
                            <a class="heading bg-amber fg-white" href="#">Surat Keterangan/Umum</a>
                            	<div class="content">
    									<table class="table">
    										 <thead>
                       	 						<tr>
                            						<th class="text-left">Waktu Masuk</th>
                            						<th class="text-left">Lokasi</th>
                            						<th class="text-left">Status</th>
                            						<th class="text-left">Aksi</th>
                            						<th class="text-left">Komentar Admin</th>
                        						</tr>
                        					</thead>
                        					<tbody>';
		foreach($w->result() as $h)
		{	
			$status = "Masih Proses";
			if($h->st!="0")
			{
				$status = "Berkas Selesai";
			}
			if($i==0)
			{

				$hasil .= '';
			}
			
                        					if($h->st!="0") { 
                        					$hasil .='<tr class="success">
                        							<td>'.generate_tanggal($h->tanggal).'</td>
                        							<td class="right">'.$h->lokasi.'</td>
                        							<td class="right">'.$status.'</td>
                        							<td class="right">Bawa berkas upload dalam bentuk fisik (hardcopy)</td>
                        							<td class="right">'.$h->komentar.'</td>
                        						</tr>';} else { 
                        						if ($h->komentar=="") {
                        					$hasil .='<tr class="error">
                        							<td>'.generate_tanggal($h->tanggal).'</td>
                        							<td class="right">'.$h->lokasi.'</td>
                        							<td class="right">'.$status.'</td>
                        							<td class="right"><a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/surat_keterangan/edit/'.$h->id_surat_ket.'"><i class="icon-pencil bg-darkIndigo"></i>Edit</a> <a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/surat_keterangan/hapus/'.$h->id_surat_ket.'"><i class="icon-cancel-2 bg-darkIndigo"></i>Hapus</a></td>
                        							<td class="right">Tidak Ada</td>
                        						</tr>';
                        						} else {
                        						$hasil .='<tr class="warning">
                        							<td>'.generate_tanggal($h->tanggal).'</td>
                        							<td class="right">'.$h->lokasi.'</td>
                        							<td class="right">'.$status.'</td>
                        							<td class="right"><a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/surat_keterangan/edit/'.$h->id_surat_ket.'"><i class="icon-pencil bg-darkIndigo"></i>Edit</a> <a class="image-button bg-lightBlue fg-white image-left" href="'.base_url().'user/surat_keterangan/hapus/'.$h->id_surat_ket.'"><i class="icon-cancel-2 bg-darkIndigo"></i>Hapus</a></td>
                        							<td class="right">'.$h->komentar.'</td>';	
                        						}
                        						}
                        					
			$i++;
			if($i>3)
			{
				$i=0;
			}
		}
		$hasil .='</tbody>
    									</table>
    					</div>
					</div>
				</div>';
		$hasil .= $this->pagination->create_links();
		return $hasil;
	} 
}

/* End of file app_global_web_model.php */