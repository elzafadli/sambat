<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?> - <?php echo $_SESSION['site_title'].' - '.$_SESSION['site_quotes']; ?></title>
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Pengaduan Pemerintah Kota Malang (sambat online)">
    <meta name="keywords" content="pengaduan, online, pengaduan online, pemerintah kota malang, sambat, sambat online">
    <meta name="author" content="Bidang Informasi Publik Pemerintah Kota Malang">
	<!-- Bagian CSS -->
	<link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-icons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-schemes.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/docs.css" rel="stylesheet">
	<!-- Bagian JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/metro.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/prettify/run_prettify.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery.dataTables.min.js"></script>

 </head>
<body>
<!-- Bagian menu fixed -->
<header class="app-bar fixed-top navy" data-role="appbar">
    <div class="container">
        <ul class="app-bar-menu">
            <li>
                <a class="app-bar-element branding dropdown-toggle"><img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/logo-malangkota.png" style="height: 50px; display: inline-block; margin-right: 10px;"> Pemerintah Kota Malang</a>
                <ul class="d-menu" data-role="dropdown">
                    <li><a href="http://malangkota.go.id/">Portal Kota Malang</a></li>
                    <li><a href="http://mediacenter.malangkota.go.id/">Mediacenter Kendedes</a></li>
                    <li><a href="http://kominfo.malangkota.go.id/">Dinas Komunikasi <br>dan Informatika</a></li>
                    <li class="divider"></li>
                    <li><a href="" class="dropdown-toggle">Jaringan Informasi</a>
                        <ul class="d-menu" data-role="dropdown">
                            <li><a href="http://www.facebook.com/malangkota.go.id">Facebook</a></li>
                            <li><a href="http://www.twitter.com/PemkotMalang">Twitter</a></li>
                            <li><a href="http://www.youtube.com/mediacentermalang">Youtube Channel</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="app-bar-menu">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li data-flexorder="2"><a href="<?php echo base_url(); ?>web/faq">FAQ</a></li>
            <li data-flexorder="1"><a href="<?php echo base_url(); ?>web/alur">Alur Pengaduan</a></li>
        </ul>
        <div class="app-bar-pullbutton automatic"></div>
        <ul class="app-bar-menu place-right" data-flexdirection="reverse">
        	<?php  if($this->session->userdata('logged_in')=="")
            { ?>
            <li><a href="<?php echo base_url(); ?>web/daftar"><span class="mif-user-plus icon"></span> Daftar</a></li>
            <li><a class="dropdown-toggle fg-white"><span class="mif-enter"></span> Login</a>
                <div class="app-bar-drop-container bg-white fg-dark place-right"
                data-role="dropdown" data-no-close="true">
                    <div class="padding20">
                        <?php echo form_open("web/login/set",'class="clearfix"'); ?>
                            <h4 class="text-light">Login User</h4>
                            <div class="input-control text">
                                <span class="mif-user prepend-icon"></span>
                                <input type="text" placeholder="username" name="username" required >
                            </div>
                            <div class="input-control text">
                                <span class="mif-lock prepend-icon"></span>
                                <input type="password" placeholder="password" name="password" required>
                            </div>
                            <p><?php echo $captcha; ?></p>
                            <div class="input-control text">
                                <span class="mif-security prepend-icon"></span>
                                <input type="text" placeholder="captcha" name="captcha" required>
                            </div>
                            <div class="form-actions">
                                <button class="button">Login</button>
                                <a href="<?php echo base_url();?>web/login/pemulihan/" class="button link text-small">Lupa Password</a>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </li>
            <?php } else { ?>
            <li><a href="<?php echo base_url(); ?>user/pesan"><span class="mif-envelop icon <?php if($this->session->userdata('notifUser') != 0){ ?>mif-ani-bounce <?php } ?>"></span> Pesan <span class="badge" style="background-color: #2ecc71;font-size: 14px;border-radius: 10px;"> &nbsp;&nbsp;<?php echo $this->session->userdata('notifUser'); ?> &nbsp;</span></a></li>
            <li><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-user-check icon"></span> Dashboard</a></li>
            <li><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span> Logout</a></li>
            <?php } ?>
        </ul>
    </div>
</header>
<!-- Bagian menu fixed -->
<!-- Bagian konten atas-->
<div class="page-content">
    <div class="fg-lightBlue">
        <div class="container">
            <div class="no-overflow" style="padding-top: 10px">
                <h1 style="font-size: 4.5rem; line-height: 1" class="metro-title align-center"><?php echo $_SESSION['site_title']; ?>
                </h1>
                <div class="sub-leader text-light align-center" style="padding-bottom: 20px">
                    <?php echo $_SESSION['site_quotes']; ?><br>Pemerintah Kota Malang
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Bagian konten atas-->
       