<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    ​<blockquote>
                       <h3>Silakan masukkan kode aktivasi</h3>
                    </blockquote>
                    <?php if($this->session->flashdata("result")){ ?>
                    <h4>
                    <span class="tag info"><?php echo $this->session->flashdata("result"); ?></span>
                    </h4>
                    <?php } ?>
                    <?php echo form_open("web/daftar/setaktivasi",'class="clearfix"'); ?>
                        <div class="input-control modern text" data-role="input">
                            <input type="text" name="id" required>
                            <span class="label">ID Anda</span>
                            <span class="informer">Masukkan ID Anda</span>
                            <span class="placeholder">Masukkan ID Anda</span>
                        </div>
                        <div class="input-control modern text" data-role="input">
                            <input type="text" name="kode_aktivasi" required>
                            <span class="label">Kode Aktivasi</span>
                            <span class="informer">Masukkan kode Aktivasi</span>
                            <span class="placeholder">Masukkan kode Aktivasi</span>
                        </div>
                        <button class="button loading-pulse lighten primary">Aktivasi Akun</button>
                    <?php echo form_close(); ?>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->