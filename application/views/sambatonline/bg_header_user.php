<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title><?php echo $_SESSION['site_title'].' - '.$_SESSION['site_quotes']; ?></title>
	<meta name="description" content="">
	<meta name="author" content="cuongv">
	<meta name="robots" content="index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- CSS styles -->
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-bootstrap.css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/metro-bootstrap-responsive.css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/iconFont.css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/css/docs.css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/prettify/prettify.css'>
	
    <!-- Load JavaScript Libraries -->
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery/jquery.widget.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/jquery/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/prettify/prettify.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/docs.js"></script>

    <!-- Metro UI CSS JavaScript plugins -->
    <script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/metro.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/js/chosen.jquery.js"></script>
	 <link rel="stylesheet" href="http://fierytech.appspot.com/css/nice-nav.css">
    <link rel="stylesheet" href="http://fierytech.appspot.com/css/glyphicons-social.css">
	</head>
	<body class="metro" style="background-color: #efeae3">
    <div class="">
        <div style="background: url(<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/b2.jpg) top left no-repeat; background-size: cover; height: 150px;">
            <div class="container" style="padding: 15px 10px">
                <h1 class="fg-white"><?php echo $_SESSION['site_title']; ?></h1>
                <h2 class="fg-white">
                    <?php echo $_SESSION['site_quotes']; ?>
                </h2>
            </div>
        </div>

       