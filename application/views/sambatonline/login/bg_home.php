<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    ​<blockquote>
                       <h3>Silakan Login dengan akun Anda</h3>
                    </blockquote>
                    <?php if($this->session->flashdata("result")){ ?>
                    <h4>
                    <span class="tag info"><?php echo $this->session->flashdata("result"); ?></span>
                    </h4>
                    <?php } ?>
                    <?php echo form_open("web/login/set",'class="clearfix"'); ?>
                        <div class="input-control modern text" data-role="input">
                            <input type="text" name="username" required>
                            <span class="label">Username Anda</span>
                            <span class="informer">Masukkan username yang akan digunakan</span>
                            <span class="placeholder">Username Anda</span>
                        </div>
                        <div class="input-control modern text" data-role="input">
                            <input type="password" name="password" required>
                            <span class="label">Password Anda</span>
                            <span class="informer">Masukkan Password yang akan digunakan</span>
                            <span class="placeholder">Password Anda</span>
                        </div>
                        <p><?php echo $captcha; ?></p>
                        <div class="input-control modern text" data-role="input">
                            <input type="text" name="captcha" required>
                            <span class="label">Captcha</span>
                            <span class="informer">Masukkan Captcha</span>
                            <span class="placeholder">Captcha</span>
                        </div>
                        <h5><span class="tag warning">Lupa Password Anda ? </span><a href="<?php echo base_url();?>web/login/pemulihan/"> klik disini</a></h5>
                        <button class="button loading-pulse lighten primary">Login</button>
                    <?php echo form_close(); ?>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->