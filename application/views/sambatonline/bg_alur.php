<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    ​<ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    <blockquote>
                       <h3>Alur Aplikasi </h3>
                    </blockquote>
                    <!-- panel -->
                    <div class="panel">
                    <div class="heading">
                        <span class="title">Alur Tiket Web</span>
                    </div>
                    <div class="content padding10">
                    <div class="wizard2" data-role="wizard2" data-button-labels='{"help": "?", "prev": "<span class=\"mif-arrow-left\"></span>", "next": "<span class=\"mif-arrow-right\"></span>", "finish": "<span class=\"mif-checkmark\"></span>"}'>
                        <div class="step">
                            <div class="step-content">
                                <p class="text-small lowercase no-margin">tahap 1</p>
                                <h1 class="no-margin-top">Daftar User</h1>
                                <div class="text-small padding20 bg-grayLighter">
                                    Sebelum menggunakan aplikasi ini, Anda diharuskan memiliki akun. Pendaftaran hanya dilakukan satu kali, dan selanjutnya dapat digunakan untuk membuat pengaduan atau membalas komentar.
                                </div>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step-content">
                                <p class="text-small lowercase no-margin">tahap 2</p>
                                <h1 class="no-margin-top">Login User</h1>
                                <div class="text-small padding20 bg-grayLighter">
                                    Setelah melakukan pendaftaran, Anda dapat login dengan menggunakan akun yang telah Anda buat sebelumnya.
                                </div>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step-content">
                                <p class="text-small lowercase no-margin">tahap 3</p>
                                <h1 class="no-margin-top">Masukkan Sambatan (pengaduan)</h1>
                                <div class="text-small padding20 bg-grayLighter">
                                    Pada aplikasi ini Anda dapat mengirimkan saran, kritik, pengaduan dan pertanyaan. Silakan pilih yang sesuai dengan kebutuhan Anda. Sambatan (pengaduan) akan kami moderasi sesuai dengan peraturan dan ketentuan yang berlaku.
                                 </div>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step-content">
                                <p class="text-small lowercase no-margin">tahap 4</p>
                                <h1 class="no-margin-top">Tunggu Sambatan (pengaduan)</h1>
                                <div class="text-small padding20 bg-grayLighter">
                                   Setelah memasukkan sambatan (pengaduan), silakan tunggu balasan dari kami. Untuk sambatan (pengaduan) yang tidak sesuai dengan peraturan dan ketentuan, kami berhak untuk tidak menayangkan atau tidak membalas. Terimakasih.
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end panel -->
                    <div class="panel">
                    <div class="heading">
                        <span class="title">Alur SMS</span>
                    </div>
                    <div class="content padding10">
                        <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/4.jpg">
                    </div>
                    </div>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <?php echo form_open("web/lacaktiket"); ?>
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" name="id_tiket" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                           <?php echo form_close(); ?>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body