<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    <?php foreach($detail_tiket as $t): ?>
                    ​<blockquote>
                       <h3>Detail Tiket #<?php echo $t->id_tiket; ?></h3>
                       <small>Pengirim : <?php echo $t->nama; ?> <cite title="<?php echo generate_tanggal($t->tanggal); ?>"><?php echo generate_tanggal($t->tanggal); ?></cite></small>
                       <p><span class="tag info"><?php echo $t->jenis; ?></span> <span class="tag info"><?php echo $t->kategori; ?></span></p>
                    </blockquote>
                    <div class="cell">
                        <dl>
                            <dt>Isi Tiket:</dt>
                            <?php if ($t->file !='0') { ?>
                            <div class="row cell align-center" style="margin:40px 0" >
                                <div class="image-container bordered handing ani image-format-hd">
                                    <div class="frame">
                                        <img src="<?php echo base_url(); ?>asset/images/member/thumb/<?php echo $t->file; ?>" style="height: 214px">
                                    </div>
                                </div>
                             </div>
                             <dd><?php echo $t->tiket; ?></dd>
                            <?php } else { ?>
                            <dd><?php echo $t->tiket; ?></dd>
                            <?php } ?>
                        </dl>
                    </div>
                    <?php endforeach; ?>
                     <div class="cell">
                        <dl>
                            <dt>Tanggapan:</dt>
                            <?php if( !empty($tanggapan_tiket) ) { ?>
                            <dd>
                                <ol class="numeric-list square-marker large-bullet fg-lightBlue">
                                    <?php foreach($tanggapan_tiket as $p): ?>
                                    <li><?php echo $p->tanggapan; ?> (Ditanggapi oleh : <strong><?php echo $p->nama_skpd; ?></strong>)</li>
                                <?php endforeach; ?>
                                </ol>
                            </dd>
                            <?php } else { ?>
                            <dd><strong>​​<span class="mif-warning mif-ani-flash"></span> MAAF TIKET INI BELUM ADA TANGGAPAN, MASIH DALAM PROSES.</strong></dd>
                            <?php } ?>
                        </dl>
                    </div>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <?php echo form_open("web/lacaktiket"); ?>
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" name="id_tiket" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                           <?php echo form_close(); ?>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->