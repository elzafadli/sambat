<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    ​<blockquote>
                       <h3>Tiket Sambatan Via SMS</h3>
                    </blockquote>
                    <div class="cell">
                        <div class="panel" data-role="panel">
                            <div class="heading">
                                <span class="icon mif-organization"></span>
                                <span class="title">Sambatan Terbaru</span>
                            </div>
                            <?php echo $sms_selengkapnya;?>
                        </div>
                    </div>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <?php echo form_open("web/lacaktiket"); ?>
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" name="id_tiket" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                           <?php echo form_close(); ?>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->