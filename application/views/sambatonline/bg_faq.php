<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    ​<blockquote>
                       <h3>Frequently Asked Questions</h3>
                    </blockquote>
                    <div class="cell">
                        <div class="panel" data-role="panel">
                            <div class="heading">
                                <span class="icon mif-info"></span>
                                <span class="title">Apa itu Sambat Online?</span>
                            </div>
                            <div class="content padding10">
                                Sambat Online adalah Sistem Aplikasi Masyarakat Bertanya Terpadu online Dinas Komunikasi dan Informatika Pemerintah Kota Malang. Melalui aplikasi ini, Anda dapat mengirimkan saran, kritik, pertanyaan atau pengaduan seputar Pemerintah Kota Malang. 
                            </div>
                        </div>
                        <br>
                        <div class="panel" data-role="panel">
                            <div class="heading">
                                <span class="icon mif-question"></span>
                                <span class="title">Bagaimana cara menggunakan aplikasi ini?</span>
                            </div>
                            <div class="content padding10">
                                Sebelum menggunakan aplikasi ini, Anda diharuskan mendaftar terlebih dahulu. Setelah mendaftar, Anda dapat masuk (login) dengan menggunakan akun yang telah dibuat sebelumnya.
                                <br>Pendaftaran hanya dilakukan satu kali saja, untuk selanjutnya Anda dapat menggunakan akun yang sama untuk menggunakan aplikasi ini kembali. Anda diwajibkan mengisi data dengan sebenar-benarnya sesuai dengan identitas Anda. 
                            </div>
                        </div>
                        <br>
                        <div class="panel" data-role="panel">
                            <div class="heading">
                                <span class="icon mif-clipboard"></span>
                                <span class="title">Bagaimana cara mendaftar (register) dan masuk (login)?</span>
                            </div>
                            <div class="content padding10">
                                Klik menu <a href="">daftar</a> untuk memulai pendaftaran user. Harap mengisi semua isian dengan sebenar-benarnya, kesalahan pengisian merupakan tanggung jawab user.<br>Setelah melakukan pendaftaran, Anda akan mendapatkan surel (email) berisi link aktivasi user. Anda tidak dapat masuk (login) sebelum mengaktifkan akun Anda. Login dengan menggunakan akun Anda dan silakan memulai menggunakan aplikasi ini.
                            </div>
                        </div>
                        <br>
                        <div class="panel" data-role="panel">
                            <div class="heading">
                                <span class="icon mif-lock"></span>
                                <span class="title">Apa yang dilakukan jika tidak bisa masuk (login)?</span>
                            </div>
                            <div class="content padding10">
                                Pastikan username, password dan captcha yang Anda masukkan sudah benar. Jika Anda tidak dapat masuk (login) kemungkinan username dan password Anda salah. Klik <a href="">lupa password</a> agar aplikasi ini mengirimkan kembali username dan password Anda melalui surel (email).
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell colspan4">
                    <div class="panel">
                        <div class="heading">
                            <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                            <span class="title">Lacak Sambatan Anda</span>
                        </div>
                        <div class="content padding20">
                            <?php echo form_open("web/lacaktiket"); ?>
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" name="id_tiket" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                           <?php echo form_close(); ?>
                            <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                            </p>
                            <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                            <div class="toolbar">
                                <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                    <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                    <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                    <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                </div>                               
                            </div>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->