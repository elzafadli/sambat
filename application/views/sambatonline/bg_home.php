 <!-- Bagian konten atas-->
<div class="page-content">
    <div class="fg-lightBlue">
        <div class="container">
            <div class="flex-grid">
                <div class="row">
                    <div class="cell colspan8" style="border: 4px #1ba1e2 solid; ">
                        <div class="carousel square-bullets" data-height="295" data-role="carousel" data-direction="left" data-controls="false">
                            <div class="slide"><img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/1.jpg" data-role="fitImage" data-format="fill"></div>
                            <div class="slide"><img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/2.jpg" data-role="fitImage" data-format="fill"></div>
                            <div class="slide"><img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/3.jpg" data-role="fitImage" data-format="fill"></div>
                            <div class="slide"><img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/4.jpg" data-role="fitImage" data-format="fill"></div>
                        </div>
                    </div>
                    <div class="cell colspan4">
                        <div class="panel">
                            <div class="heading">
                                <img class="icon" src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/location.png">
                                <span class="title">Lacak Sambatan Anda</span>
                            </div>
                            <div class="content padding20">
                               <?php echo form_open("web/lacaktiket"); ?>
                            <div class="input-control text full-size" data-role="input">
                                <input type="text" name="id_tiket" style="padding-right: 54px;">
                                <button class="button"><span class="mif-search"></span></button>
                            </div>
                           <?php echo form_close(); ?>
                                <p class="" style="margin-top: 10px;">
                                Disini anda dapat mencari sambatan (pengaduan) yang sudah dikirimkan ke center kami dengan menggunakan ID yang sudah di berikan.
                                </p>
                                <h4 class="fg-lightBlue">Temukan kami disini juga..</h4>
                                <div class="toolbar">
                                    <div class="toolbar-section" data-role="group" data-group-type="multi-state">
                                        <button class="toolbar-button"><span class="mif-facebook"></span></button>
                                        <button class="toolbar-button"><span class="mif-twitter"></span></button>
                                        <button class="toolbar-button"><span class="mif-youtube"></span></button>
                                    </div>                               
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
<!-- /Bagian konten atas-->
<!-- bagian konten body -->
<div class="page-content-body">
    <div class="bg-lightBlue fg-white">
        <div class="container">
            <div class="grid no-margin-top">
                <!-- pengaduan gambar -->
                <div class="row cells3">
                    <?php foreach($tiket_gambar as $t): ?>
                    <div class="cell">
                        <a class="fg-white" href="<?php echo base_url(); ?>web/detailtiket/<?php echo $t->id_tiket; ?>">
                        <h5>ID Sambat : <?php echo $t->id_tiket; ?></h5>
                        <div class="image-container element-selected">
                            <div class="frame">
                                <img src="<?php echo base_url(); ?>asset/images/member/thumb/<?php echo $t->file; ?>" style="height: 214px">
                            </div>
                            <div class="image-overlay">
                                <p><?php echo character_limiter($t->tiket, 240);?></p>
                            </div>
                        </div>
                        <blockquote>
                            Tiket
                            <hr>
                            <p><?php echo character_limiter($t->tiket, 340);?></p>
                            <p>Pengirim : <?php echo $t->nama; ?></p>
                            <small class="fg-white"><?php echo generate_tanggal($t->tanggal); ?></small><br><br>
                            Tanggapan
                            <hr>
                            <p>Dari : <?php echo $t->nama_skpd; ?></p>
                            <small class="fg-white"><?php echo $t->tanggapan; ?></small>
                        </blockquote>
                        </a>
                    </div>    
                    <?php endforeach; ?>
                </div>
                <!-- /pengaduan gambar -->
            </div>
        </div>
    </div>
    <!-- pengaduan teks -->
    <div class="container">
        <div class="row">
            <div class="panel" data-role="panel">
                <div class="heading">
                    <span class="icon mif-organization"></span>
                    <span class="title">Sambatan Terbaru via Web</span>
                </div>
                <div class="content padding10">
                    <?php foreach($tiket_teks as $s): ?>
                    <blockquote>            
                        Isi Tiket :
                        <hr>
                        <a href="<?php echo base_url(); ?>web/detailtiket/<?php echo $s->id_tiket; ?>">
                        <cite title="Klik untuk detail">ID Sambat : <?php echo $s->id_tiket; ?></cite>
                        <p><?php echo character_limiter($s->tiket, 340);?></p>
                        <small>Pengirim : <?php echo $s->nama; ?>, <cite title="Source Title"><?php echo generate_tanggal($s->tanggal); ?></cite></small>
                        </a>
                        <br><br>
                        Tanggapan :
                        <hr>
                        <a href="#">
                        <p><?php echo character_limiter($s->tanggapan, 340);?></p>
                        <small>Dari : <?php echo $s->nama_skpd; ?></small>
                        </a>
                    </blockquote>
                    <?php endforeach; ?>
                    <hr class="thin">
                    <div class="align-right">
                        <a href="<?php echo base_url(); ?>web/tiket_selengkapnya" class="button loading-pulse lighten primary">  Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /pengaduan teks -->
    <br>
    <!-- pengaduan sms -->
    <div class="container">
        <div class="row">
            <div class="panel" data-role="panel">
                <div class="heading">
                    <span class="icon mif-mobile"></span>
                    <span class="title">Sambatan Terbaru via SMS</span>
                </div>
                <div class="content padding10">
                    <?php foreach($tiket_sms as $m): ?>
                    <blockquote>
                        Isi Tiket :
                        <hr>
                        <a href="<?php echo base_url(); ?>web/detailtiketsms/<?php echo $m->ID; ?>">
                        <cite title="Klik untuk detail">ID Sambat : <?php echo $m->ID; ?></cite>
                        <p><?php echo character_limiter($m->TextDecoded, 340);?></p>
                        <small>Pengirim : <?php echo substr_replace($m->SenderNumber,'xxx',-3); ?> , <cite title="Source Title"><?php echo $m->ReceivingDateTime; ?></cite></small>
                        </a>
                        <br><br>
                        Tanggapan :
                        <hr>
                        <a href="<?php echo base_url(); ?>web/detailtiketsms/<?php echo $m->ID; ?>">
                        <p><?php echo character_limiter($m->tanggapan, 340);?></p>
                        <small>Dari : <?php echo $s->nama_skpd; ?></small> <cite title="Source Title"><?php //echo $m->ReceivingDateTime; ?></cite></small>
                        </a>
                    </blockquote>
                    <?php endforeach; ?>
                    <hr class="thin">
                    <div class="align-right">
                        <a href="<?php echo base_url(); ?>web/sms_selengkapnya" class="button loading-pulse lighten primary">  Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- /pengaduan sms -->
</div>
<div class="page-content-body no-tablet-portrait no-phone">
    <div class="container">
        <div class="presenter bg-darkCyan" data-role="presenter" data-height="220" data-easing="swing">
            <div class="scene">
                <div class="act fg-white">
                    <h1 class="actor" data-position="20,200" >Sampaikan sambatan Anda sekarang...</h1>
                    <h1 class="actor" data-position="70,140" >Kami akan melayani Anda dengan senang hati.</h1>
                </div>
                <div class="act fg-white">
                    <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/banner-1.png" class="actor" data-position="10,10" style="height: 200px">
                    <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/banner-2.png" class="actor" data-position="10,280" style="height: 200px">
                    <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/banner-3.png" class="actor" data-position="10,550" style="height: 200px">
                    <img src="<?php echo base_url(); ?>asset/theme/<?php echo $_SESSION['site_theme']; ?>/images/banner-4.png" class="actor" data-position="10,820" style="height: 200px">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /bagian konten body -->
