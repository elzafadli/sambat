<!-- bagian konten body -->
<div class="page-content-body">
    <!-- form -->
    <div class="container">
        <div class="grid">
            <div class="row cells12 fg-lightBlue">
                <div class="cell colspan8">
                    <ul class="breadcrumbs2">
                        <?php echo $this->breadcrumb->output(); ?>
                    </ul>
                    ​<blockquote>
                       <h3>Silakan Masukkan Email Anda</h3>
                    </blockquote>
                    <?php if($this->session->flashdata("result")){ ?>
                    <h4>
                    <span class="tag info"><?php echo $this->session->flashdata("result"); ?></span>
                    </h4>
                    <?php } ?>
                    <?php echo form_open("web/login/email",'class="clearfix"'); ?>
                        <div class="input-control modern text" data-role="input">
                            <input type="email" name="email" required>
                            <span class="label">Email Anda</span>
                            <span class="informer">Masukkan Email Anda digunakan</span>
                            <span class="placeholder">Email Anda</span>
                        </div>
                        <p><?php echo $captcha; ?></p>
                        <div class="input-control modern text" data-role="input">
                            <input type="text" name="captcha" required>
                            <span class="label">Captcha</span>
                            <span class="informer">Masukkan Captcha</span>
                            <span class="placeholder">Captcha</span>
                        </div>
                        <br>
                        <hr></hr>
                        <button class="button loading-pulse lighten primary">Login</button>
                    <?php echo form_close(); ?>
                </div>
        </div>
        </div>
    </div>
    <!-- /form -->
</div>
<!-- /bagian konten body -->