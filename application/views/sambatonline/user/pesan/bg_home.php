
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#tampil").click(function(){
        $("#tampil").hide();
        $("tr").show();
    });
});
</script>

<div class="container">
    <div class="grid fluid">
        <!-- bagian konten body -->
        <div class="page-content-body">
            <div class="container">
                <div class="grid responsive">
                    <div class="row cells12 fg-lightBlue">
                        <!-- Sidebar kiri -->
                        <div class="cell colspan3">
                            <ul class="sidebar2 ">
                                <li class="title">Menu Anda</li>
                                <li><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-home icon"></span>Dashboard</a></li>
                                <li><a href="<?php echo base_url(); ?>user/tiket"><span class="mif-mail icon"></span>Buat Tiket</a></li>
                                <li><a href="<?php echo base_url(); ?>user/cektiket"><span class="mif-mail-read icon"></span>Tiket Anda</a></li>
                                <li><a href="<?php echo base_url(); ?>user/profile"><span class="mif-user icon"></span>Profile</a></li>
                                <li><a href="<?php echo base_url(); ?>user/password"><span class="mif-key icon"></span>Password</a></li>
                                <li class=""><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span>Logout</a></li>
                            </ul>
                        </div>
                        <!-- /Sidebar kiri -->
                        <!-- konten kanan -->
                        <div class="cell colspan9">
                            <ul class="breadcrumbs2">
                                <?php echo $this->breadcrumb->output(); ?>
                            </ul>
                            <hr class="thin bg-grayLighter">
                            <?php if($this->session->flashdata("result")){ ?>
                            <div class="notice marker-on-bottom fg-blue">
                              <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                          </div>
                          <?php } ?>
                          <div class="table-responsive">          
                              <table class="table">
                                <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Pesan</th>
                                      <th>Dari</th>
                                      <th>Hapus</th>
                                  </tr>
                              </thead>
                              <tbody>

                                <?php
                                $i=1; 
                                foreach ($pesan as $item) {
                                if ($i <= 10) {
                                
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $item->pesan; ?></td>
                                        <td><?php echo $item->nama; ?></td>
                                        <td><a href='<?php echo base_url(); ?>user/pesan/delete/<?php echo $item->id_notifikasi; ?>' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
                                    </tr>
                                <?php }else{ ?>
                                    <tr id="pesan" style="display: none">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $item->pesan; ?></td>
                                        <td><?php echo $item->nama; ?></td>
                                        <td><a href='<?php echo base_url(); ?>user/pesan/delete/<?php echo $item->id_notifikasi; ?>' onClick=\"return confirm('Anda Yakin Ingin Menghapus?');\" class='button alert'><span class='mif-bin'></span></a></td>
                                    </tr>
                                <?php } ?>
                                    <?php $i++;}  ?>
                                </tbody>
                            </table>
                            <button id="tampil">View More</button>
                        </div>
                        <?php //echo $data_retrieve; ?>
                    </div>
                    <!-- /konten kanan -->
                </div>
            </div>
        </div>
    </div>
    <!-- /bagian konten body -->
</div>
</div>