<div class="container">
    <div class="grid fluid">
    <!-- bagian konten body -->
    <div class="page-content-body">
        <div class="container">
            <div class="grid responsive">
                <div class="row cells12 fg-lightBlue">
                    <!-- Sidebar kiri -->
                    <div class="cell colspan3">
                        <ul class="sidebar2 ">
                            <li class="title">Menu Anda</li>
                            <li><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-home icon"></span>Dashboard</a></li>
                            <li><a href="<?php echo base_url(); ?>user/tiket"><span class="mif-mail icon"></span>Buat Tiket</a></li>
                            <li><a href="<?php echo base_url(); ?>user/cektiket"><span class="mif-mail-read icon"></span>Tiket Anda</a></li>
                            <li class="active stick bg-darkBlue"><a href="<?php echo base_url(); ?>user/profile"><span class="mif-user icon"></span>Profile</a></li>
                            <li><a href="<?php echo base_url(); ?>user/password"><span class="mif-key icon"></span>Password</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span>Logout</a></li>
                        </ul>
                    </div>
                    <!-- /Sidebar kiri -->
                    <!-- konten kanan -->
                    <div class="cell colspan9">
                        <ul class="breadcrumbs2">
                            <?php echo $this->breadcrumb->output(); ?>
                        </ul>
                        <h1 class="text-light">Edit Profile Anda</h1>
                        <hr class="thin bg-grayLighter">
                        <?php if($this->session->flashdata("result")){ ?>
                        <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                        </div>
                        <?php } ?>
                        <br>
                        <?php echo form_open("user/profile/set",'class="clearfix"'); ?>
                        <div class="flex-grid example" data-text="Edit Profile">
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                    <label>Nama</label>
                                    <div class="input-control text full-size">
                                        <input type="text" id="nama" name="nama" required value="<?php echo $this->session->userdata("nama"); ?>">
                                    </div>
                                </div>
                                <div class="cell colspan6">
                                    <label>Email</label>
                                    <div class="input-control text full-size">
                                        <input type="email" id="email" name="email" required value="<?php echo $this->session->userdata("email"); ?>">
                                        <input type="hidden" id="email_temp" name="email_temp" value="<?php echo $this->session->userdata("email"); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                    <label>Profesi</label>
                                    <div class="input-control text full-size">
                                        <input type="text" id="profesi" name="profesi" required value="<?php echo $this->session->userdata("profesi"); ?>">
                                    </div>
                                </div>
                                <div class="cell colspan6">
                                    <label>No HP (contoh: 08xxxxxxxx)</label>
                                    <div class="input-control text full-size">
                                        <input type="text" id="no_telpon" name="no_telpon" required value="<?php echo $this->session->userdata("no_telpon"); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan12">
                                    <label>Alamat</label>
                                    <div class="input-control textarea full-size">
                                        <textarea id="alamat" name="alamat" required><?php echo $this->session->userdata("alamat"); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <button class="button loading-pulse lighten primary">Simpan Data</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- /konten kanan -->
                </div>
            </div>
        </div>
    </div>
<!-- /bagian konten body -->
</div>
</div>