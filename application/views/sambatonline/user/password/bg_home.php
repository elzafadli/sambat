<div class="container">
    <div class="grid fluid">
    <!-- bagian konten body -->
    <div class="page-content-body">
        <div class="container">
            <div class="grid responsive">
                <div class="row cells12 fg-lightBlue">
                    <!-- Sidebar kiri -->
                    <div class="cell colspan3">
                        <ul class="sidebar2 ">
                            <li class="title">Menu Anda</li>
                            <li><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-home icon"></span>Dashboard</a></li>
                            <li><a href="<?php echo base_url(); ?>user/tiket"><span class="mif-mail icon"></span>Buat Tiket</a></li>
                            <li><a href="<?php echo base_url(); ?>user/cektiket"><span class="mif-mail-read icon"></span>Tiket Anda</a></li>
                            <li><a href="<?php echo base_url(); ?>user/profile"><span class="mif-user icon"></span>Profile</a></li>
                            <li class="active stick bg-darkBlue"><a href="<?php echo base_url(); ?>user/password"><span class="mif-key icon"></span>Password</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span>Logout</a></li>
                        </ul>
                    </div>
                    <!-- /Sidebar kiri -->
                    <!-- konten kanan -->
                    <div class="cell colspan9">
                        <ul class="breadcrumbs2">
                            <?php echo $this->breadcrumb->output(); ?>
                        </ul>
                        <h1 class="text-light">Edit Password Anda</h1><br>

                        <hr class="thin bg-grayLighter">
                        <?php if($this->session->flashdata("result")){ ?>
                        <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                        </div>
                        <?php } ?>
                        <br>
                        <?php echo form_open("user/password/set",'class="clearfix"'); ?>
                        <div class="flex-grid example" data-text="Edit Password">
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                    <label>Password Lama</label>
                                    <div class="input-control password full-size">
                                    <?php if($this->session->userdata('password')){  ?>
                                        <input type="password" id="pass_lama" name="pass_lama" placeholder="Password Lama" disabled value="123456789">
                                    <?php }else{ ?>
                                        <input type="password" id="pass_lama" name="pass_lama" placeholder="Password Lama" required>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="cell colspan6">
                                    <label>Password Baru</label>
                                    <div class="input-control password full-size">
                                        <input type="password" id="pass_baru" name="pass_baru" placeholder="Password Baru" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                </div>
                                <div class="cell colspan6">
                                    <label>Ulangi Password Baru</label>
                                    <div class="input-control password full-size">
                                        <input type="password" id="ulangi_pass" name="ulangi_pass" placeholder="Ulangi Password Baru" required>
                                    </div>
                                </div>
                            </div>
                            <button class="button loading-pulse lighten primary">Simpan Data</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- /konten kanan -->
                </div>
            </div>
        </div>
    </div>
<!-- /bagian konten body -->
</div>
</div>