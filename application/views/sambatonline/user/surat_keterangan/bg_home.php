<div class="container">
	<div class="grid fluid">
	<div class="row">
			<nav class="breadcrumbs margin-bootom10">
			<?php echo $this->breadcrumb->output(); ?>
			</nav>
		</div>
	</div>
	<div class="grid fluid bg-white">
		<div class="row">
			<div class="span3 padding10">
				<div class="no-tablet-portrait no-phone">
                <div class="panel">
                    <div class="panel-header bg-lightBlue fg-white">
                        Waktu sekarang
                    </div>
                    <div class="panel-content bg-white">
                        <div class="times" data-role="times"></div>
                        <p>Kami hanya memproses berkas pada hari dan jam kerja.</p>
                    </div>
                </div>
            	</div>
                <nav class="row sidebar light">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>user/dashboard"><i class="icon-home"></i>Dashboard</a></li>
                        <li><a href="<?php echo base_url(); ?>user/profile"><i class="icon-user-3"></i>Profile</a></li>
                        <li><a href="<?php echo base_url(); ?>user/password"><i class="icon-unlocked"></i>Password</a></li>
                        <li><a href="<?php echo base_url(); ?>user/berkas"><i class="icon-file"></i>Cek Berkas</a></li>
                        <li>
                            <a class="dropdown-toggle" href="#"><i class="icon-tree-view"></i>Surat</a>
                                <ul class="dropdown-menu" data-role="dropdown">
                                    <li class="stick bg-red active"><a href="<?php echo base_url(); ?>user/surat_keterangan">Surat Keterangan / Umum</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_pindah">Surat Pindah</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_nikah">Surat Nikah</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kelahiran">Surat Kelahiran</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kematian">Surat Kematian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kkb">Surat Keterangan Kelakuan Baik</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_keterangan_usaha">Surat Keterangan Usaha</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/ijin_keramaian">Ijin Keramaian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_bepergian">Surat Keterangan Bepergian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kuasa">Surat Keterangan Kuasa</a></li>
                                </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>web/sign_in/logout"><i class="icon-accessibility"></i>Sign Out</a></li>
                    </ul>
                </nav>
            </div>
            <?php //echo $dt_retrieve; ?>
        <div class="span9 bg-white padding10">
        <?php if($this->session->flashdata("result")){ ?>
                <div class="notice marker-on-bottom"><?php echo $this->session->flashdata("result"); ?></div>
            <?php } ?>
            <?php echo form_open_multipart("user/surat_keterangan/set",'class="clearfix"'); ?>
            <div class="wizard" id="wizard">
                <div class="steps">
                    <div class="step">
                        <blockquote>
                            <p>SYARAT SURAT KETERANGAN/UMUM</p>
                            <small>Pengantar RT/RW</small><br>
                            <small>Kartu Keluarga (KK)</small><br>
                            <small>Kartu Tanda Penduduk (KTP)</small><br>
                        </blockquote>
                        <small><cite title="Source Title">* Mohon membawa semua berkas yang telah diupload nantinya</cite></small>
                        <p class="text-alert">Klik tombol lanjut untuk proses selanjutnya</p>
                    </div>
                    <div class="step">
                        <blockquote><p>Memasukkan Keperluan untuk :</p></blockquote>
                        <textarea id="keterangan" name="keterangan" data-transform="input-control" placeholder="Isikan Keterangan" required></textarea>
                    <p class="text-alert">Klik tombol lanjut untuk proses selanjutnya</p>
                    </div>
                    <div class="step">
                        <blockquote><p>File yang harus diunggah</p>
                        <small>Pengantar RT/RW</small><br>
                        <small>Kartu Keluarga</small><br>
                        <small>Kartu Tanda Penduduk</small>
                        </blockquote>
                        <small><cite title="Source Title">* Semua file dijadikan satu dalam bentuk zip, pdf atau rar</cite></small>
                        <div class="input-control file info-state" data-role="input-control">
                            <input type="file" name="file" required>
                            <button class="btn-file"></button>
                        </div>
                        <p class="text-alert">ekstensi yang diperbolehkan *.zip, *.pdf atau *.rar</p>
                        <div style="margin-top:20px;"><button type="submit" class="image-button bg-lightBlue fg-white image-left large"><i class="icon-upload-3 bg-cobalt large"></i>Simpan Data</button></div>
                        <small><cite title="Source Title">* Mohon membawa semua file yang telah diupload pada waktu pengambilan berkas dalam bentuk <em>hardcopy</em></cite></small>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <script>
                $(function(){
                    $('#wizard').wizard({
                        locale: 'id',
                        onCancel: function(){
                            $.Dialog({
                                title: 'Surat Keterangan',
                                content: 'Dengan meng-klik YA, semua isian Anda akan hilang <br>Anda Yakin ingin membatalkan? <br><a class="button danger" href="<?php echo base_url(); ?>user/dashboard">YA</a>',
                                shadow: true,
                                padding: 10,
                                width:300
                            });
                        },
                        onHelp: function(){
                            $.Dialog({
                                title: 'Surat Keterangan',
                                content: 'Help button clicked',
                                shadow: true,
                                padding: 10
                            });
                        },
                        onFinish: function(){
                            $.Dialog({
                                title: 'Surat Keterangan',
                                content: 'Periksa kembali isian Anda...klik tombol Simpan Data untuk memproses berkas.<br><small><cite title="Source Title">* Mohon membawa semua berkas yang telah diupload nantinya</cite></small>',
                                shadow: true,
                                padding: 10
                            });
                        }
                    });
                });
            </script>
        </div>
        </div>
	</div>
</div>