<div class="container">
	<div class="grid fluid">
	<div class="row">
			<nav class="breadcrumbs margin-bootom10">
			<?php echo $this->breadcrumb->output(); ?>
			</nav>
		</div>
	</div>
	<div class="grid fluid bg-white">
		<div class="row">
			<div class="span3 padding10">
				<div class="no-tablet-portrait no-phone">
                <div class="panel">
                    <div class="panel-header bg-lightBlue fg-white">
                        Waktu sekarang
                    </div>
                    <div class="panel-content bg-white">
                        <div class="times" data-role="times"></div>
                        <p>Kami hanya memproses berkas pada hari dan jam kerja.</p>
                    </div>
                </div>
            	</div>
                <nav class="row sidebar light">
    				<ul>
        				<li><a href="<?php echo base_url(); ?>user/dashboard"><i class="icon-home"></i>Dashboard</a></li>
        				<li><a href="<?php echo base_url(); ?>user/profile"><i class="icon-user-3"></i>Profile</a></li>
        				<li><a href="<?php echo base_url(); ?>user/password"><i class="icon-unlocked"></i>Password</a></li>
        				<li class="stick bg-red active"><a href="<?php echo base_url(); ?>user/berkas"><i class="icon-file"></i>Cek Berkas</a></li>
        				<li>
            				<a class="dropdown-toggle" href="#"><i class="icon-tree-view"></i>Surat</a>
            					<ul class="dropdown-menu" data-role="dropdown">
                					<li><a href="<?php echo base_url(); ?>user/surat_keterangan">Surat Keterangan / Umum</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_pindah">Surat Pindah</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_nikah">Surat Nikah</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kelahiran">Surat Kelahiran</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kematian">Surat Kematian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kkb">Surat Keterangan Kelakuan Baik</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_keterangan_usaha">Surat Keterangan Usaha</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/ijin_keramaian">Ijin Keramaian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_bepergian">Surat Keterangan Bepergian</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/surat_kuasa">Surat Keterangan Kuasa</a></li>
            					</ul>
        				</li>
        				<li><a href="<?php echo base_url(); ?>web/sign_in/logout"><i class="icon-accessibility"></i>Sign Out</a></li>
    				</ul>
				</nav>
            </div>
            <div class="span9 bg-white padding10">
            <blockquote>
                <p>Keterangan</p>
            </blockquote>
            <ul>
                <li class="fg-red">Merah : Berkas Anda masih dalam proses di Kelurahan.</li>
                <li class="fg-green">Hijau : Berkas Anda telah selesai di proses.</li>
                <li class="fg-orange">Kuning : Berkas Anda tidak lengkap, silakan baca komentar admin dan edit kembali.</li>
            </ul>
            <p><?php echo $surat_keterangan_indexs; ?></p>
            <p><?php echo $surat_pindah_indexs; ?></p>
            <p><?php echo $surat_nikah_indexs; ?></p>
            <p><?php echo $surat_kelahiran_indexs; ?></p>
            <p><?php echo $surat_kematian_indexs; ?></p>
            <p><?php echo $surat_kkb_indexs; ?></p>
            <p><?php echo $surat_keterangan_usaha_indexs; ?></p>
            <p><?php echo $ijin_keramaian_indexs; ?></p>
            <p><?php echo $surat_bepergian_indexs; ?></p>
            <p><?php echo $surat_kuasa_indexs; ?></p>
            </div>
        </div>
	</div>
</div>