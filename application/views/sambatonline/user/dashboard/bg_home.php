<div class="container">
	<div class="grid fluid">
    <!-- bagian konten body -->
    <div class="page-content-body">
        <div class="container">
            <div class="grid responsive">
                <div class="row cells12 fg-lightBlue">
                    <!-- Sidebar kiri -->
                    <div class="cell colspan3">
                        <ul class="sidebar2 ">
                            <li class="title">Menu Anda</li>
                            <li class="active stick bg-darkBlue"><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-home icon"></span>Dashboard</a></li>
                            <li><a href="<?php echo base_url(); ?>user/tiket"><span class="mif-mail icon"></span>Buat Tiket</a></li>
                            <li><a href="<?php echo base_url(); ?>user/cektiket"><span class="mif-mail-read icon"></span>Tiket Anda</a></li>
                            <li><a href="<?php echo base_url(); ?>user/profile"><span class="mif-user icon"></span>Profile</a></li>
                            <li><a href="<?php echo base_url(); ?>user/password"><span class="mif-key icon"></span>Password</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span>Logout</a></li>
                        </ul>
                    </div>
                    <!-- /Sidebar kiri -->
                    <!-- konten kanan -->
                    <div class="cell colspan9">
                        <ul class="breadcrumbs2">
                            <?php echo $this->breadcrumb->output(); ?>
                        </ul>
                        <h1>Selamat datang, <small>apa yang ingin Anda lakukan hari ini?</small></h1>
                        <hr class="thin bg-grayLighter">
                    </div>
                    <!-- /konten kanan -->
                </div>
            </div>
        </div>
    </div>
<!-- /bagian konten body -->
</div>
</div>