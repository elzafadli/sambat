<div class="container">
    <div class="grid fluid">
    <!-- bagian konten body -->
    <div class="page-content-body">
        <div class="container">
            <div class="grid responsive">
                <div class="row cells12 fg-lightBlue">
                    <!-- Sidebar kiri -->
                    <div class="cell colspan3">
                        <ul class="sidebar2 ">
                            <li class="title">Menu Anda</li>
                            <li><a href="<?php echo base_url(); ?>user/dashboard"><span class="mif-home icon"></span>Dashboard</a></li>
                            <li class="active stick bg-darkBlue"><a href="<?php echo base_url(); ?>user/tiket"><span class="mif-mail icon"></span>Buat Tiket</a></li>
                            <li><a href="<?php echo base_url(); ?>user/cektiket"><span class="mif-mail-read icon"></span>Tiket Anda</a></li>
                            <li><a href="<?php echo base_url(); ?>user/profile"><span class="mif-user icon"></span>Profile</a></li>
                            <li><a href="<?php echo base_url(); ?>user/password"><span class="mif-key icon"></span>Password</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>web/login/logout"><span class="mif-exit icon"></span>Logout</a></li>
                        </ul>
                    </div>
                    <!-- /Sidebar kiri -->
                    <!-- konten kanan -->
                    <div class="cell colspan9">
                        <ul class="breadcrumbs2">
                            <?php echo $this->breadcrumb->output(); ?>
                        </ul>
                        <h1 class="text-light">Tiket Baru</h1>
                        <hr class="thin bg-grayLighter">
                        <?php if($this->session->flashdata("result")){ ?>
                        <div class="notice marker-on-bottom fg-blue">
                          <span class="mif-warning mif-ani-flash"></span> <?php echo $this->session->flashdata("result"); ?>
                        </div>
                        <?php } ?>
                        <br>
                        <?php echo form_open_multipart("user/tiket/set",'class="clearfix"'); ?>
                        <div class="flex-grid example" data-text="Tiket Sambatan">
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                    <label>Pilih Jenis Tiket</label>
                                    <div class="input-control select full-size">
                                        <select name="id_jenis" id="id_jenis">
                                            <option value="0" selected="selected" >Klik untuk Pilih</option>
                                            <?php foreach($jenis as $j): ?> 
                                                <option value="<?php echo $j->id_jenis; ?>"><?php echo $j->jenis; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="cell colspan6">
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan12">
                                    <label>Pilih Kategori Tiket</label>
                                    <div class="input-control select multiple full-size" style="height: 100px">
                                        <select multiple="" name="id_kategori" id="id_kategori">
                                            <option value="0" selected="selected" >Klik untuk Pilih Kategori:</option>
                                            <?php $no=1; foreach( $kategori as $k):?> 
                                                <option value="<?php echo $k->id_kategori; ?>"><?php echo $no.". ".$k->kategori; ?></option>
                                            <?php $no++; endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan12">
                                    <label>Isi Tiket (Sisa : <span id="sisa">500</span> Karakter)</label>
                                    <div class="input-control textarea full-size">
                                        <textarea id="tiket" name="tiket" required maxlength="500" onkeyup="countChar(this)"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-just-sb">
                                <div class="cell colspan6">
                                    <label>Upload Gambar (jika ada)</label>
                                    <div class="input-control file full-size" data-role="input">
                                        <input type="file" id="foto" name="userfile" tabindex="-1" style="z-index: 0;">
                                        <button class="button" type="button"><span class="mif-folder"></span></button>
                                    </div>
                                </div>
                                <div class="cell colspan6">
                                </div>
                            </div>
                            <button class="button loading-pulse lighten primary">Simpan Data</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- /konten kanan -->
                </div>
            </div>
        </div>
    </div>
<!-- /bagian konten body -->
</div>
</div>

<script>
 function countChar(val) {
    var myLength = $("#tiket").val().length;
    
    var word = 500 - myLength;
    $("#sisa").text(word);

  };
</script>